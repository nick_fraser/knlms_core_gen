
import sbt._
import Keys._

object BuildSettings extends Build {
    lazy val buildSettings = Defaults.defaultSettings ++ Seq(
        parallelExecution in Test := false
        //scalaVersion in remez in Global := "2.10.4"
        //javaOptions += "-Xmx4G",
        //javaOptions += "-Xms4G"
    )

    lazy val root = Project("root", file("."), settings=buildSettings)
    //lazy val root = Project("root", file("."), settings=buildSettings) dependsOn(RootProject(remez))

    //lazy val remez = uri("git://github.com/nickfraser/remez-scala.git")

}

