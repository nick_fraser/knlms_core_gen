
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

// n is any positive int
// doReg is of length log2Up(n) + 1

class PipelinedTree[T <: Data](dtype: T, doReg: Array[Boolean], n: Int, f: (T, T) => T, val pf: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){dtype.clone.asInput}
        val res = dtype.clone.asOutput
    }

    //Create input registers if required.
    val reg_list = new ArrayBuffer[Rege[T]]()
    for (i <- 0 until n) {
        val reg = Module(new Rege(dtype, doReg(0)))
        reg.io.din := io.x(i)
        reg_list += reg
    }

    /*
    //Create tree structure.
    var regwidth : Int = n/2
    for (j <- 1 until doReg.size) {
        for (i <- 0 until regwidth) {
            val nreg = Module(new Rege(dtype, doReg(j)))
            val lreg = reg_list.head
            //reg_list.drop(1)
            reg_list -= lreg
            val rreg = reg_list.head
            //reg_list.drop(1)
            reg_list -= rreg
            nreg.io.din := f(lreg.io.dout,rreg.io.dout)
            reg_list += nreg
        }
        regwidth = regwidth/2
    }
    */

    val treeIn = reg_list.map(x => x.io.dout)
    io.res := ReduceTree(treeIn, f, doReg.slice(1, doReg.length), pf)

    val latency: Int = log2Up(n)*pf + CountReg.nreg(doReg)
}

//n is any positive int.
//doReg is any size.

//class VarShiftRegister[T <: Data](dtype: T, doReg: Array[Boolean], n: Int, init: T = None) extends Module {
class VarShiftRegister[T <: Data](dtype: T, doReg: Array[Boolean], n: Int, init: T = null.asInstanceOf[T]) extends Module {
    val io = new Bundle {
        val vin = Vec.fill(n){dtype.clone.asInput}
        val vout = Vec.fill(n){dtype.clone.asOutput}
    }
    val latency: Int = CountReg.nreg(doReg)

    if (latency == 0) {
        //connect inputs directly to output.
        for (i <- 0 until n) {
            io.vout(i) := io.vin(i)
        }
    }
    else {
        //Connect shift register to inputs and SR network.
        val reg_list = new ArrayBuffer[Rege[T]]()
        for (i <- 0 until doReg.size) {
            for (j <- 0 until n) {
                if (i == 0) {
                    val reg = Module(new Rege(dtype, doReg(i), init))
                    reg.io.din := io.vin(j)
                    reg_list += reg
                }
                else {
                    val nreg = Module(new Rege(dtype, doReg(i), init))
                    val preg = reg_list.head
                    reg_list -= preg
                    nreg.io.din := preg.io.dout
                    reg_list += nreg
                }
            }
        }

        //Connect shift register to output.
        for (i <- 0 until n) {
            val reg = reg_list.head
            reg_list -= reg
            io.vout(i) := reg.io.dout
        }
    }
}

