
package PKAF

import Chisel._
import util.Random
import scala.collection.mutable.ArrayBuffer

class KNLMSTimeSeriesWrapper[T <: Bits](dtype: T, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double, val doReg: Array[Boolean], val n: Int, val m: Int,
                                        mul: (T, T) => T, div: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, exp: T => T, gt: (T, T) => Bool, DtoT: Double => T,
                                        pmul: Int = 0, pdiv: Int = 0, padd: Int = 0, psub: Int = 0, pexp: Int = 0, pgt: Int = 0,
                                        val singleDelay: Boolean = true, val addMask: Boolean = false, val correctionTerms: Int = 0) extends Module {
    val io = new Bundle {
        val y = dtype.clone.asInput
        val ybar = dtype.clone.asOutput
        val valid_in = Bool(INPUT)
        val valid_out = Bool(OUTPUT)
    }

    //Create a KNLMS module.
    val knlms = Module(new KNLMS[T](dtype, neg_gamma, thresh, epsilon, eta, doReg, n, m, mul, div, add, sub, exp, gt, DtoT, pmul, pdiv, padd, psub, pexp, pgt, singleDelay, addMask, correctionTerms));

    //Create inputs.
    val x = Vec.fill(m){ RegInit(DtoT(0.0)) } //Create and x vector using a shift register.
    x(0) := io.y
    for (i <- 1 until m) {
        x(i) := x(i-1)
    }

    //Create & connect valid signal.
    knlms.io.valid_in := io.valid_in
    io.valid_out := knlms.io.valid_out

    //Connect x and y.
    knlms.io.y := io.y
    for (i <- 0 until m) {
        knlms.io.x(i) := x(i)
    }
    //printf("y=%x, x(0)=%x, x(1)=%x\n", io.y.toSInt, x(0).toSInt, x(1).toSInt)

    //Connect ybar.
    io.ybar := knlms.io.ybar

    val latency = knlms.latency
    val (d1, d2) = (knlms.d1, knlms.d2)
}

