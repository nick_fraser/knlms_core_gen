
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

// n is any positive int
// doReg is of length 1 + (log2Up(n) + 1) + 1 + 1

class ExpKernel[T <: Data](dtype: T, neg_gamma: Double, fromD: Double => T, val doReg: Array[Boolean], val n : Int, mul: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, exp: T => T, val pmul: Int = 0, val padd: Int = 0, val psub: Int = 0, val pexp: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){dtype.clone.asInput}
        val y = Vec.fill(n){dtype.clone.asInput}
        val res = dtype.clone.asOutput
    }

    val dptree = Module(new DotProdTree[T](dtype, doReg.slice(1,2+log2Up(n)), n, mul, add, pmul, padd))
    for (i <- 0 until n) {
        val reg = Module(new Rege(dtype, doReg(0)))
        reg.io.din := sub(io.x(i), io.y(i))
        dptree.io.x(i) := reg.io.dout
        dptree.io.y(i) := reg.io.dout
    }

    val pre = Module(new Rege(dtype, doReg(2+log2Up(n))))
    val post = Module(new Rege(dtype, doReg(3+log2Up(n))))
    pre.io.din := mul(dptree.io.res, fromD(neg_gamma))
    post.io.din := exp(pre.io.dout)
    io.res := post.io.dout

    val latency: Int = psub + CountReg.nreg(doReg(0) +: doReg.slice(2+log2Up(n), 4+log2Up(n))) + dptree.latency + pmul + pexp
}

