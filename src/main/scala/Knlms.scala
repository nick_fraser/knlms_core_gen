
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer
import util.Random

// n is any positive int.
// m is any positive int.
// doReg is of size {(log2(m) + 4) + (2) + (log2(n) + 1) + 1 + (3)} + a*pmul + b*pdiv + c*padd + d*psub + e*pexp + f*pgt.

class KNLMS[T <: Data](dtype: T, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double, val doReg: Array[Boolean], val n: Int, val m: Int,
                        mul: (T, T) => T, div: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, exp: T => T, gt: (T, T) => Bool, DtoT: Double => T,
                        pmul: Int = 0, pdiv: Int = 0, padd: Int = 0, psub: Int = 0, pexp: Int = 0, pgt: Int = 0,
                        val singleDelay: Boolean = true, val addMask: Boolean = false, val correctionTerms: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(m){dtype.clone.asInput}
        val y = dtype.clone.asInput
        val valid_in = Bool(INPUT)
        val ybar = dtype.clone.asOutput
        val Dout = Vec.fill(m*n){dtype.clone.asOutput}
        val Aout = Vec.fill(n){dtype.clone.asOutput}
        val cout = UInt(OUTPUT, log2Up(n) + 1)
        val valid_out = Bool(OUTPUT)
    }
    val knlmsfp = Module(new KNLMSForward[T](dtype, neg_gamma, thresh, epsilon, eta, doReg, n, m, mul, div, add, sub, exp, gt, DtoT, pmul, pdiv, padd, psub, pexp, pgt, singleDelay, addMask, correctionTerms))

    //create intermediate registers to connect to the KNLMS forward path.
    val dreg = Vec.fill(n*m){ RegInit(DtoT(0.0)) }
    val areg = Vec.fill(n){ RegInit(DtoT(0.0)) }
    val creg = Reg(init = UInt(0, log2Up(n) + 1))

    //Connect c.
    creg := knlmsfp.io.cout
    knlmsfp.io.cin := creg
    io.cout := knlmsfp.io.cout
    //io.cout := creg

    //Connect y, ybar and x.
    knlmsfp.io.y := io.y
    io.ybar := knlmsfp.io.ybar
    for (i <- 0 until m)
        knlmsfp.io.x(i) := io.x(i)

    //Connect alpha.
    for (i <- 0 until n) {
        knlmsfp.io.Ain(i) := areg(i)
        areg(i) := knlmsfp.io.Aout(i)
        io.Aout(i) := knlmsfp.io.Aout(i)
        //io.Aout(i) := areg(i)
    }

    //Connect D.
    for (i <- 0 until n) {
        for (j <- 0 until m) {
            val ind: Int = i*m + j
            knlmsfp.io.Din(ind) := dreg(ind)
            dreg(ind) := knlmsfp.io.Dout(ind)
            io.Dout(ind) := knlmsfp.io.Dout(ind)
            //io.Dout(in) := dreg(in)
        }
    }

    //Connect valids.
    knlmsfp.io.valid_in := io.valid_in
    io.valid_out := knlmsfp.io.valid_out

    val latency = knlmsfp.latency
    val (d1, d2) = (knlmsfp.d1, knlmsfp.d2)
}

object KNLMS {
    def getRegLength(n: Int, m: Int): Int = 11 + log2Up(n) + log2Up(m)

    def estimateDoReg(n: Int, m: Int, p: Int, divDelay: Int, expDelay: Int): (Array[Boolean], Array[Boolean], Array[Boolean]) = {
        //val divDelay: Int = 17
        //val expDelay: Int = 5
        val regLen: Int = getRegLength(n, m) + expDelay + divDelay
        val doReg = Array.fill(regLen){ false }
        val kernelDelay: Int = 3 + log2Up(m) + expDelay
        val addToDictDelay: Int = 2
        val predictDelay: Int = 1 + log2Up(n)
        val errDelay: Int = 1
        val updateDelay: Int = 2 + divDelay
        val totalDelay: Int = kernelDelay + addToDictDelay + predictDelay + errDelay + updateDelay
        for (i <- 1 until p+1) {
            /*val idealLocation: Int = (i*totalDelay)/(p+1)
            val prevLocation: Int = ((i-1)*totalDelay)/(p+1)
            if (idealLocation <= kernelDelay) {
                val startLocation: Int = 0
                val relLocation: Int = idealLocation - startLocation 
                val regIndexStart: Int = 0
                val regIndexEnd: Int = 4 + log2Up(m)
                if (relLocation < 3 + log2Up(m)) doReg(regIndexStart + relLocation) = true
                else if (!doReg(regIndexEnd-1)) doReg(regIndexEnd-1) = true
                else doReg(regIndexEnd-2) = true
            }
            else if (idealLocation <= (kernelDelay + addToDictDelay)) {
                val startLocation: Int = kernelDelay+1
                val relLocation: Int = idealLocation - startLocation 
                val regIndexStart: Int = 4 + log2Up(m)
                val regIndexEnd: Int = 6 + log2Up(m)
                doReg(regIndexStart + relLocation) = true
            }
            else if (idealLocation <= (kernelDelay + addToDictDelay + predictDelay)) {
                val startLocation: Int = kernelDelay + addToDictDelay + 1
                val relLocation: Int = idealLocation - startLocation 
                val regIndexStart: Int = 6 + log2Up(m)
                val regIndexEnd: Int = 7 + log2Up(n) + log2Up(m)
                doReg(regIndexStart + relLocation) = true
            }
            //else if (idealLocation <= (kernelDelay + addToDictDelay + predictDelay + errDelay + updateDelay)) {
            else {
                val startLocation: Int = kernelDelay + addToDictDelay + predictDelay + 1
                val relLocation: Int = idealLocation - startLocation 
                val regIndexStart: Int = 7 + log2Up(n) + log2Up(m)
                val regIndexEnd: Int = 10 + log2Up(n) + log2Up(m)
                if (relLocation < 2) doReg(regIndexStart + relLocation) = true
                else if (relLocation >= 1 + divDelay) doReg(regIndexEnd-1) = true
            }*/

            val index = (i*regLen)/(p+1)
            doReg(index) = true
        }
        val expStart: Int = 3 + log2Up(m) //The location in the module where the exponential starts.
        val expReg = doReg.slice(expStart, expStart + expDelay)
        val divStart: Int = kernelDelay + addToDictDelay + predictDelay + errDelay + 2
        val divReg = doReg.slice(divStart, divStart + divDelay)
        val otherReg = doReg.slice(0, expStart) ++ doReg.slice(expStart + expDelay, divStart) ++ doReg.slice(divStart + divDelay, regLen)
        //println("All:    " + doReg.map(f => if(f) "1" else "0").mkString("{",", ","}"))
        //println("doReg:  " + otherReg.map(f => if(f) "1" else "0").mkString("{",", ","}"))
        //println("expReg: " + expReg.map(f => if(f) "1" else "0").mkString("{",", ","}"))
        //println("divReg: " + divReg.map(f => if(f) "1" else "0").mkString("{",", ","}"))
        (otherReg, expReg, divReg)
    }

    //def estimatePDiv(doReg: Array[Boolean], p: Int): Int = p - CountReg.nreg(doReg)

}

