
package PKAF

import Chisel._

/** A kernel adaptive filtering prediction module.
  *
  * m, n are positive ints
  * doReg.length = (log2Up(m) + 4) + (log2Up(n) + 1)
  * TODO: Allow other kernel functions (not just the Gaussian kernel).
  */
class KafPredict[T <: Data](dtype: T, doReg: Array[Boolean], n: Int, m: Int, fromD: Double => T,
                            neg_gamma: Double, mul: (T, T) => T, div: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, exp: T => T,
                            pmul: Int = 0, pdiv: Int = 0, padd: Int = 0, psub: Int = 0, pexp: Int = 0, pgt: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(m){ dtype.clone.asInput }
        val d = Vec.fill(m*n){ dtype.clone.asInput }
        val a = Vec.fill(n){ dtype.clone.asInput }
        val c = UInt(INPUT, log2Up(n) + 1)
        val y = dtype.clone.asOutput
    }

    /***** KERNEL EVALUATION *****/

    //Create kernel modules.
    val KMs = Array.fill(n){ Module(new ExpKernel[T](dtype, neg_gamma, fromD, doReg.slice(0,log2Up(m)+4), m, mul, add, sub, exp, pmul, padd, psub, pexp)) }
    val k = Vec.fill(n){ dtype.clone }

    //Connect up inputs to kernel module to Dictionary and outputs to k vector.
    for (i <- 0 until n) {
        for (j <- 0 until m) {
            KMs(i).io.x(j) := io.x(j)
            val index = i*m + j
            KMs(i).io.y(j) := io.d(index)
        }
        k(i) := KMs(i).io.res
    }

    /******* KERNEL VECTOR MASK *******/

    //Shift all csr by doReg.slice(0, log2Up(n)+4).
    val csr = Module(new VarShiftRegister[UInt](UInt(), Array.fill(KMs(0).latency){ true }, 1))
    csr.io.vin(0) := io.c

    //Apply a mask to k vector.
    val kmask = Module(new VectorMask[T](dtype, fromD(0.0), n))
    kmask.io.cin := csr.io.vout(0)
    for (i <- 0 until n) {
        kmask.io.x(i) := k(i)
    }

    /****** CALCULATE PREDICTION ******/

    //Delay alpha.
    val asrTmp = Vec.fill(n){ dtype.clone }
    val asr = Module(new VarShiftRegister[T](dtype, Array.fill(KMs(0).latency){ true }, n))
    for (i <- 0 until n) {
        asr.io.vin(i) := io.a(i)
        asrTmp(i) := asr.io.vout(i)
    }

    val y_est = Module(new DotProdTree[T](dtype, doReg.slice(log2Up(m)+4, log2Up(m)+log2Up(n)+5), n, mul, add, pmul, padd))
    for (i <- 0 until n) {
        y_est.io.x(i) := kmask.io.y(i)
        y_est.io.y(i) := asrTmp(i)
    }

    /******* Connection Output ********/
    io.y := y_est.io.res

    val latency = KMs(0).latency + y_est.latency
}

object KafPredict {
    def getRegLength(n: Int, m: Int): Int = 5 + log2Up(n) + log2Up(m)
}

