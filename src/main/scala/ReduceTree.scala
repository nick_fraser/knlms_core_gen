
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

/** ReduceTree:
  *
  * An object to create a binary tree like structure hardware with optional registers after each layer in the tree.
  * doReg.length = log2Up(it.length)
  */
object ReduceTree {
    def insertRegs[T <: Data](x: T, n: Int): T = {
        if (n <= 0) return x
        else return insertRegs(RegNext(x), n-1)
    }
    def apply[T <: Data](it: Iterable[T], f: (T, T) => T): T = {
        ReduceTree(it, f, Array.fill(log2Up(it.size)){ false })
    }
    def apply[T <: Data](it: Iterable[T], f: (T, T) => T, doReg: Array[Boolean]): T = {
        ReduceTree(it, f, doReg, 0)
    }
    def apply[T <: Data](it: Iterable[T], f: (T, T) => T, doReg: Array[Boolean], pf: Int): T = {
        val n = it.size
        if (n == 1) {
            return it.head
        }
        val next = ArrayBuffer[T]()
        val fP: (T, T) => T = if (doReg(0) == true) { (x,y) => RegNext(f(x,y)) } else f
        val iP: T => T = if (doReg(0) == true) { x => insertRegs(x, pf+1) } else { x => x }
        val pairs = it.grouped(2)
        for (pair <- pairs) {
            if (pair.size == 1) next += iP(pair.head)
            else next += pair.reduce(fP)
        }
        ReduceTree(next, f, doReg.slice(1, doReg.length))
    }
}

