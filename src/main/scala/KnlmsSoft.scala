
package PKAF
import scala.collection.mutable.ArrayBuffer

//A software version of the KNLMS algorithm - This version behaves exactly the same as the hardware version. It uses preallocated memory and has predetermined maximum dictionary size.
class KNLMSSoft(val n: Int, val m: Int, val gamma: Double, val eta: Double, val epsilon: Double, val thresh: Double) {
    var a = Array.fill(n){ 0.0 }
    var d = Array.fill(n*m){ 0.0 }
    var c: Int = 0

    def train(x: Array[Double], y: Double): Unit = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");

        //Calculate kernel.
        val k = kernel(x)

        //Determine if the dictionary needs to be updated.
        val add: Boolean = k.reduce((l: Double, r: Double) => if(l > r) l else r) < thresh && c < n
        if(add) {
            k(c) = 1.0
            d = MatHelper.updateRow(d, n, m, x, c)
            c += 1
        }

        //Calculate prediction and kdotk.
        val kdotk = k.map((f: Double) => f*f).reduce(_+_)
        val ybar: Double = (k, a).zipped.map(_*_).reduce(_+_)

        //Update alpha.
        a = (a, k.map((f: Double) => f*(y-ybar)*eta/(kdotk + epsilon))).zipped.map(_+_)
    }

    def predict(x: Array[Double]): Double = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");
        val k = kernel(x)
        val ybar: Double = (k, a).zipped.map(_*_).reduce(_+_)
        ybar
    }

    private def kernel(x: Array[Double]): Array[Double] = {
        var k = new Array[Double](n)
        for(i <- 0 until n) {
            val start: Int = i*m
            val end: Int = (i+1)*m
            k(i) = if(i < c) math.exp((x, d.slice(start, end)).zipped.map(_-_).map((f: Double) => f*f).reduce(_+_) * -gamma) else 0.0
        }
        k
    }

}

object KNLMSSoft {
    //Create a KNLMSSoft object an initialise a, d and c with given values.
    def apply(a: Array[Double], d: Array[Double], c: Int, n: Int, m: Int, gamma: Double, eta: Double, epsilon: Double, thresh: Double): KNLMSSoft = {
        val k = new KNLMSSoft(n, m, gamma, eta, epsilon, thresh)
        k.a = a
        k.d = d
        k.c = c
        k
    }
}

abstract class PipelinedKernelAdaptiveFilter {
    def train(x: Array[Double], y: Double): Unit
    def predict(x: Array[Double]): Double
    var a: Array[Double]
    var d: Array[Double]
    var c: Int
    var ybar: Double
}

//A software version of the DKNLMS algorithm - This version behaves exactly the same as the hardware version. It uses preallocated memory and has predetermined maximum dictionary size.
class DKNLMSSoft(val n: Int, val m: Int, val gamma: Double, val eta: Double, val epsilon: Double, val thresh: Double, val delay: Int) extends PipelinedKernelAdaptiveFilter {
    //These values represent the current values at the output, and therefore the values used for the next input.
    var a = Array.fill(n){ 0.0 }
    var d = Array.fill(n*m){ 0.0 }
    var c: Int = 0
    var ybar: Double = 0.0

    //These values are a FIFO containing partial internal calculations in the pipeline.
                                //x,            y,      add,        k,              ybar
    var fifo_int = ArrayBuffer[(Array[Double],  Double, Boolean,    Array[Double],  Double)]()
    for (i <- 0 until delay) {
        fifo_int += {(Array.fill(m){ 0.0 }, 0.0, false, Array.fill(n){ 0.0 }, 0.0)}
    }

    def train(x: Array[Double], y: Double): Unit = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");

        //Calculate values to add to the queue.
        //Calculate kernel.
        val k = kernel(x)

        //Determine if the dictionary needs to be updated.
        val add: Boolean = k.reduce((l: Double, r: Double) => if(l > r) l else r) < thresh
        //Calculate prediction and kdotk.
        val yest: Double = (k, a).zipped.map(_*_).reduce(_+_)

        //Put these results onto the pipeline.
        fifo_int += {(x, y, add, k, yest)}
        val (px, py, padd, pk, pybar) = fifo_int(0)
        fifo_int -= fifo_int(0)
        ybar = pybar

        if(padd && c < n) {
            pk(c) = 1.0
            d = MatHelper.updateRow(d, n, m, px, c)
            c += 1
        }
        val pkdotk = pk.map((f: Double) => f*f).reduce(_+_)

        //Calculate delta_a.
        a = (a, pk.map((f: Double) => f*(py-pybar)*eta/(pkdotk + epsilon))).zipped.map(_+_)
    }

    def predict(x: Array[Double]): Double = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");
        val k = kernel(x)
        val ybar: Double = (k, a).zipped.map(_*_).reduce(_+_)
        ybar
    }

    private def kernel(x: Array[Double]): Array[Double] = {
        var k = new Array[Double](n)
        for(i <- 0 until n) {
            val start: Int = i*m
            val end: Int = (i+1)*m
            k(i) = if(i < c) math.exp((x, d.slice(start, end)).zipped.map(_-_).map((f: Double) => f*f).reduce(_+_) * -gamma) else 0.0
        }
        k
    }

}

//A software version of the DKNLMS algorithm - This version behaves exactly the same as the hardware version. It uses preallocated memory and has predetermined maximum dictionary size.
class MultiDelayKnlms(val n: Int, val m: Int, val gamma: Double, val eta: Double, val epsilon: Double, val thresh: Double, val dictDelay: Int, val alphaDelay: Int, addMask: Boolean = false) extends PipelinedKernelAdaptiveFilter {
    //These values represent the current values at the output, and therefore the values used for the next input.
    var a = Array.fill(n){ 0.0 }
    var d = Array.fill(n*m){ 0.0 }
    var c: Int = 0
    var ybar: Double = 0.0
    var dictCount: Int = 0

                                //x,            y,      k,              add
    val dictFifo = ArrayBuffer[(Array[Double], Double, Array[Double], Boolean)]()
    for (i <- 0 until dictDelay) dictFifo += {(Array.fill(m){ 0.0 }, 0.0, Array.fill(n){ 0.0 }, false)}
                                //y,    ybar,   k
    val alphaFifo = ArrayBuffer[(Double, Double, Array[Double])]()
    for (i <- 0 until alphaDelay) alphaFifo += {(0.0, 0.0, Array.fill(n){ 0.0 })}

    def train(x: Array[Double], y: Double): Unit = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");

        //Calculate values to add to the queue.
        //Calculate kernel.
        val k = kernel(x)

        //Determine if the dictionary needs to be updated.
        val add: Boolean = (k.reduce((l: Double, r: Double) => if(l > r) l else r) < thresh) && (dictCount == 0)
        if (add && addMask) {
            dictCount = dictDelay
        } else if (dictCount != 0) {
            dictCount -= 1
        }

        //Put these results onto the pipeline.
        dictFifo += {(x, y, k, add)}
        val (pdx, pdy, pdk, pdadd) = dictFifo(0)
        dictFifo -= dictFifo(0)
        if(pdadd && c < n) {
            pdk(c) = 1.0
            d = MatHelper.updateRow(d, n, m, pdx, c)
            c += 1
        }

        //Calculate prediction and kdotk.
        val yest: Double = (pdk, a).zipped.map(_*_).reduce(_+_)

        //Put intermediate results into the pipeline.
        alphaFifo += {(pdy, yest, pdk)}
        val (pay, paybar, pak) = alphaFifo(0)
        alphaFifo -= alphaFifo(0)

        val pakdotk = pak.map((f: Double) => f*f).reduce(_+_)
        //Calculate delta_a.
        a = (a, pak.map((f: Double) => f*(pay-paybar)*eta/(pakdotk + epsilon))).zipped.map(_+_)
        ybar = paybar
    }

    def predict(x: Array[Double]): Double = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");
        val k = kernel(x)
        val ybar: Double = (k, a).zipped.map(_*_).reduce(_+_)
        ybar
    }

    private def kernel(x: Array[Double]): Array[Double] = {
        var k = new Array[Double](n)
        for(i <- 0 until n) {
            val start: Int = i*m
            val end: Int = (i+1)*m
            k(i) = if(i < c) math.exp((x, d.slice(start, end)).zipped.map(_-_).map((f: Double) => f*f).reduce(_+_) * -gamma) else 0.0
        }
        k
    }

}

/** A software version of MultiDelayKnlms with the addition of correction terms.
  *
  * This software simulated values running through the pipeline of my proposed architecture. 
  */
class MultiDelayKnlmsWithCorrections(val n: Int, val m: Int, val gamma: Double, val eta: Double, val epsilon: Double, val thresh: Double, val dictDelay: Int, val alphaDelay: Int) extends PipelinedKernelAdaptiveFilter {
    //These values represent the current values at the output, and therefore the values used for the next input.
    var a = Array.fill(n){ 0.0 }
    var d = Array.fill(n*m){ 0.0 }
    var c: Int = 0
    var ybar: Double = 0.0

                                //x,            y,      k,              kk,          valid
    val dictFifo = ArrayBuffer[(Array[Double], Double, Array[Double], Array[Double], Boolean)]()
    for (i <- 0 until dictDelay) dictFifo += {(Array.fill(m){ 0.0 }, 0.0, Array.fill(n){ 0.0 }, Array.fill(dictDelay){ 0.0 }, false)}

    // Create a FIFO to tell whether or not the previous entries were added to the dictionary.
    val addFifo = ArrayBuffer[Boolean]()
    for (i <- 0 until dictDelay) addFifo += false

                                //y,    ybar,   k,              ,vKtk.
    val alphaFifo = ArrayBuffer[(Double, Double, Array[Double], Array[Double])]()
    for (i <- 0 until alphaDelay) alphaFifo += {(0.0, 0.0, Array.fill(n){ 0.0 }, Array.fill(alphaDelay){ 0.0 })}

    val stepFifo = ArrayBuffer[Double]()
    for (i <- 0 until alphaDelay) stepFifo += 0.0

    def train(x: Array[Double], y: Double): Unit = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");

        //Calculate values to add to the queue.
        //Calculate kernel.
        val k = kernel(x)

        // Create the second kernel vector from dictFIFO.
        val kk = dictFifo.map(_._1).map(xf => Math.exp(-gamma*(xf,x).zipped.map(_-_).map(xi => xi*xi).reduce(_+_)))

        //Put these results onto the pipeline.
        dictFifo += {(x, y, k, kk.to[Array], true)}
        val (pdx, pdy, pdk, pdkk, pdvalid) = dictFifo(0)
        dictFifo -= dictFifo(0)

        //Determine if the dictionary needs to be updated.
        val delayAdd: Boolean = (pdk.reduce((l: Double, r: Double) => if(l > r) l else r) < thresh)
        val pAdd: Boolean = if (dictDelay < 1) true else !(pdkk, addFifo).zipped.map((k,add) => (k > thresh) && add).reduce(_|_)
        val add = (delayAdd & pAdd) && (c < n) && pdvalid

        // Fix the pdk vector to include the values that have been added from pdkk.
        val kadd = (pdkk, addFifo).zipped.filter((k,add) => add).zipped.map((k,add) => k)
        val numAdd = if (dictDelay < 1) 0 else addFifo.map(x => if (x) 1 else 0).reduce(_+_)
        for (i <- 0 until numAdd) pdk(c-numAdd+i) = kadd(i)

        if(add) {
            pdk(c) = 1.0
            d = MatHelper.updateRow(d, n, m, pdx, c)
            c += 1
        }

        addFifo += add
        addFifo -= addFifo(0)

        //Calculate prediction and kdotk.
        val yest: Double = (pdk, a).zipped.map(_*_).reduce(_+_)
        val vKtk = alphaFifo.map(_._3).map(kk => (pdk, kk).zipped.map(_*_).reduce(_+_))

        //Put intermediate results into the pipeline.
        alphaFifo += {(pdy, yest, pdk, vKtk.to[Array])}

        val (pay, paybar, pak, pavKtk) = alphaFifo(0)
        alphaFifo -= alphaFifo(0)

        val pakdotk = pak.map((f: Double) => f*f).reduce(_+_)

        // Calculate the step at this iteration.
        val prevStep = if (alphaDelay < 1) 0.0 else (pavKtk, stepFifo).zipped.map(_*_).reduce(_+_)
        val step = (eta/(pakdotk + epsilon))*(pay - paybar - prevStep)

        //Calculate delta_a.
        a = (a, pak.map(_*step)).zipped.map(_+_)
        //ybar = paybar
        ybar = paybar + prevStep
        stepFifo += step
        stepFifo -= stepFifo(0)
    }

    def predict(x: Array[Double]): Double = {
        if(x.length != m) throw new Exception("The length of x=" + x.length + " does not match the length of m=" + m + ".");
        val k = kernel(x)
        val ybar: Double = (k, a).zipped.map(_*_).reduce(_+_)
        ybar
    }

    private def kernel(x: Array[Double]): Array[Double] = {
        var k = new Array[Double](n)
        for(i <- 0 until n) {
            val start: Int = i*m
            val end: Int = (i+1)*m
            k(i) = if(i < c) math.exp((x, d.slice(start, end)).zipped.map(_-_).map((f: Double) => f*f).reduce(_+_) * -gamma) else 0.0
        }
        k
    }

}

