
package PKAF

import Chisel._

// n is any positive int.

class VectorMask[T <: Data](dtype: T, default: T, n: Int) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){ dtype.clone.asInput }
        val y = Vec.fill(n){ dtype.clone.asOutput }
        val cin = UInt(INPUT, log2Up(n)+1)
    }

    for (i <- 0 until n) {
        val m = Module(new Mask[T](dtype, default, i, n))
        m.io.cin := io.cin
        m.io.x := io.x(i)
        io.y(i) := m.io.y
    }
}

class Mask[T <: Data](dtype: T, default: T, n: Int, nMax: Int) extends Module {
    val io = new Bundle {
        val x = dtype.clone.asInput
        val y = dtype.clone.asOutput
        val cin = UInt(INPUT, log2Up(nMax)+1)
    }

    when (io.cin > UInt(n)) {
        io.y := io.x
    } .otherwise {
        io.y := default
    }
}

