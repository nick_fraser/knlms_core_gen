
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

//A static object which contains different ways to compute the exponential function.
object ExpEval {
    def Exp[T <: Data](x: T, exp: T => T): T = {
        return exp(x)
    }

    //Doesn't work if the datatype cannot represent the indexes of the lut.
    def ExpLUT[T <: Data](x: T, low: Double, high: Double, n: Int, mul: (T, T) => T, add: (T, T) => T, toSInt: T => SInt, toT: Double => T): T = {
        //Generate the LUT.
        val lut = new ArrayBuffer[T]()
        val b = -low
        val m = (n-1)/(high - low)
        //print(f"b = $b%e, m = $m%e\n")
        val bT = toT(b)
        val mT = toT(m)
        //println("bT.width=" + bT.width)
        //println("mT.width=" + mT.width)

        //generate the LUT.
        //print("{")
        for (i <- 0 until n) {
            //print(math.exp(low + i*((high-low)/(n-1))))
            //if (i != n-1) print(", ")
            lut += toT(math.exp(low + i*((high-low)/(n-1))))
        }
        //print("}\n")
        val lutT = Vec(lut)

        //Determine the index of the LUT. Store in luti
        val index = SInt(width = log2Up(n) + 1)
        val luti = SInt(width = log2Up(n) + 1)
        val scal_x = mul(add(bT, x), mT)
        index := toSInt(scal_x)
        when (index < SInt(0)) {
            luti := SInt(0)
        } .elsewhen (index >= SInt(n)) {
            luti := SInt(n-1)
        } .otherwise {
            //No rounding.
            luti := index
        }

        //Access the element of the LUT.
        return lutT(UInt(luti))
    }

    //Doesn't work if the datatype cannot represent the indexes of the lut.
    def ExpLUTRd[T <: Data](x: T, low: Double, high: Double, n: Int, mul: (T, T) => T, add: (T, T) => T, toSInt: T => SInt, toT: Double => T, gt: (T, T) => Bool, rem: T => T): T = {
        //Generate the LUT.
        val lut = new ArrayBuffer[T]()
        val b = -low
        val m = (n-1)/(high - low)
        val bT = toT(b)
        val mT = toT(m)

        //generate the LUT.
        for (i <- 0 until n) {
            lut += toT(math.exp(low + i*((high-low)/(n-1))))
        }
        val lutT = Vec(lut)

        //Determine the index of the LUT. Store in luti
        val index = SInt(width = log2Up(n) + 1)
        val luti = SInt(width = log2Up(n) + 1)
        val scal_x = mul(add(bT, x), mT)
        val excess = rem(scal_x)
        index := toSInt(scal_x)
        when (index < SInt(0)) {
            luti := SInt(0)
        } .elsewhen (index >= SInt(n)) {
            luti := SInt(n-1)
        } .elsewhen (gt(excess, toT(0.5))) {
            //Round up if it's closer.
            luti := index + SInt(1)
        } .otherwise {
            luti := index
        }

        //Access the element of the LUT.
        return lutT(UInt(luti))
    }

    //Doesn't work if the datatype cannot represent the indexes of the lut.
    def ExpLUTli[T <: Data](x: T, low: Double, high: Double, n: Int, mul: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, toSInt: T => SInt, DtoT: Double => T, rem: T => T): T = {
        //Generate the LUT.
        val lut = new ArrayBuffer[T]()
        val b = -low
        val m = (n-1)/(high - low)
        val bT = DtoT(b)
        val mT = DtoT(m)

        //generate the LUT.
        for (i <- 0 until n) {
            lut += DtoT(math.exp(low + i*((high-low)/(n-1))))
        }
        val lutT = Vec(lut)

        //Determine the indexes for the LUT. Store is luth lutl.
        val index = SInt(width = log2Up(n) + 1)
        val lutl = SInt(width = log2Up(n) + 1)
        val luth = SInt(width = log2Up(n) + 1)
        val scal_x = mul(add(bT, x), mT)
        index := toSInt(scal_x)
        lutl := index
        luth := index + SInt(1)
        val excess = rem(scal_x)
        val ret = x.clone
        when (index < SInt(0)) {
            ret := lutT(0)
        } .elsewhen (index >= SInt(n-1)) {
            ret := lutT(n-1)
        } .otherwise {
            //Include rounding?
            //lutl := index
            //luth := index + SInt(1)
            ret := add(mul(sub(DtoT(1.0), excess), lutT(UInt(lutl))), mul(excess, lutT(UInt(luth))))
        }

        return ret
    }

}

