
package PKAF
import scala.collection.mutable.ArrayBuffer

object MatHelper {
    def print(x: Array[Double], n: Int, m: Int): Unit = {
        for(i <- 0 until n) {
            val start: Int = i*m
            val end: Int = (i + 1)*m
            println(x.slice(start, end).map(_.toString()).mkString(" "))
        }
    }
    def updateRow(d: Array[Double], n: Int, m: Int, x: Array[Double], row: Int): Array[Double] = {
        val dn = new ArrayBuffer[Double]()
        for(i <- 0 until n) {
            val start = i*m
            val end = (i+1)*m
            if(i != row)
                dn ++= d.slice(start, end)
            else
                dn ++= x
        }
        dn.toArray
    }
}

