
package PKAF

import Chisel._
import java.io.FileWriter
import scala.io.Source
import scala.sys.process._

/** A simple suite to testing the accuracy of kernel adaptive filter hardware.
  *
  * TODO: Switch the c variable from a KNLMSForward module to a generic KAFModule.
  */
class KafHardwareSim[T <: Bits](c: () => KNLMS[T], p: () => KafPredict[T], toD: BigInt => Double, fromD: Double => T, tToD: T => Double,
                                n: Int, m: Int, xTrain: Array[Array[Double]], yTrain: Array[Double], xTest: Array[Array[Double]], yTest: Array[Double],
                                testMode: String, testName: String) {

    class KafTrainer(c: KNLMS[T], isTrace: Boolean = false) extends Tester(c, isTrace) {
        def connectInputs(x: Vec[T], y: T, valid: Boolean): Unit = {
            //Connect cin and y.
            poke(c.io.y, y.litValue())
            poke(c.io.valid_in, Bool(valid).litValue())

            //Connect x.
            for (i <- 0 until c.m) {
                poke(c.io.x(i), x(i).litValue())
            }
        }

        def fetchOutputs(): Unit = {
            //Get cout.
            cCur = peek(c.io.cout).toInt

            //Get dictionary.
            for (i <- 0 until c.n) {
                for (j <- 0 until c.m) {
                    val index: Int = i*c.m + j
                    dCur(i)(j) = toD(peek(c.io.Dout(index)).toInt)
                }
            }

            //Connect alpha.
            for (i <- 0 until c.n) {
                aCur(i) = toD(peek(c.io.Aout(i)).toInt)
            }
        }

        for(i <- 0 until nTrain) {
            connectInputs(Vec( xTrain(i).map(fromD) ), fromD(yTrain(i)), true)
            fetchOutputs()
            printModel(f"output/hwtmp/$testName%s_$i%d")
            step(1)
        }
    }

    class KafPredictor(c: KafPredict[T], i: Int, isTrace: Boolean = false) extends Tester(c, isTrace) { // Create multiple predictors.
        if (c.latency != 0) throw new Exception(f"""The predictor module requires a latency of zero, not "${c.latency}%d"""") //"// Quote mark to fix syntax highlighting.

        val d = Array.fill(n){ Array.fill(m){ 0.0 } }
        val a = Array.fill(n){ 0.0 }
        var cin: Int = 0

        // Load the model at time i.
        val dLines = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_d.csv").getLines().toArray[String]
        val aLines = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_a.csv").getLines().toArray[String]
        cin = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_c.csv").getLines().next.split(",")(0).toInt
        for(k <- 0 until dLines.length) {
            d(k) = dLines(k).split(",").map(_.toDouble)
            a(k) = aLines(k).split(",")(0).toDouble
        }

        var squareErr = 0.0
        for(j <- 0 until nTest) {
            // Connect inputs.
            poke(c.io.c, UInt(cin).litValue())
            for(k <- 0 until n) {
                for (l <- 0 until m) {
                    val index = l + k*m
                    poke(c.io.d(index), fromD(d(k)(l)).litValue())
                }
                poke(c.io.a(k), fromD(a(k)).litValue())
            }
            for(l <- 0 until m) {
                poke(c.io.x(l), fromD(xTest(j)(l)).litValue())
            }

            // Fetch and store output.
            val pred = toD(peek(c.io.y).toInt)
            squareErr += Math.pow(yTest(j)-pred,2)

            step(1)
        }

        //Calculate MSE & write to file.
        res(i) = squareErr / nTest

    }

    class KafPredictorSingle(c: KafPredict[T], isTrace: Boolean = false) extends Tester(c, isTrace) { // Create single predictor.
        if (c.latency != 0) throw new Exception(f"""The predictor module requires a latency of zero, not "${c.latency}%d"""") //"// Quote mark to fix syntax highlighting.

        val d = Array.fill(n){ Array.fill(m){ 0.0 } }
        val a = Array.fill(n){ 0.0 }
        var cin: Int = 0

        for(i <- 0 until nTrain) {

            // Load the model at time i.
            val dLines = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_d.csv").getLines().toArray[String]
            val aLines = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_a.csv").getLines().toArray[String]
            cin = Source.fromFile(f"output/hwtmp/$testName%s_$i%d_c.csv").getLines().next.split(",")(0).toInt
            for(k <- 0 until dLines.length) {
                d(k) = dLines(k).split(",").map(_.toDouble)
                a(k) = aLines(k).split(",")(0).toDouble
            }

            var squareErr = 0.0

            // Connect model.
            poke(c.io.c, UInt(cin).litValue())
            for(k <- 0 until n) {
                for (l <- 0 until m) {
                    val index = l + k*m
                    poke(c.io.d(index), fromD(d(k)(l)).litValue())
                }
                poke(c.io.a(k), fromD(a(k)).litValue())
            }

            for(j <- 0 until nTest) {
                // Connect inputs.
                for(l <- 0 until m) {
                    poke(c.io.x(l), fromD(xTest(j)(l)).litValue())
                }

                // Fetch and store output.
                val pred = toD(peek(c.io.y).toInt)
                squareErr += Math.pow(yTest(j)-pred,2)

                step(1)
            }

            //Calculate MSE & write to file.
            res(i) = squareErr / nTest

        }
        //val f = new FileWriter(f"output/$testName%s.csv")
        //f.write(res.mkString("",",\n",","))
        //f.close()

    }

    def printModel(fileNamePrefix: String) = {
        val dFile = new FileWriter(fileNamePrefix + "_d.csv")
        val aFile = new FileWriter(fileNamePrefix + "_a.csv")
        val cFile = new FileWriter(fileNamePrefix + "_c.csv")

        dFile.write(dCur.map(_.mkString("",", ",",")).mkString("","\n",""))
        aFile.write(aCur.mkString("",",\n",","))
        cFile.write(cCur + ",")

        dFile.close()
        aFile.close()
        cFile.close()
    }

    val testArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )

    val nTrain = yTrain.length
    val nTest = yTest.length
    val dCur: Array[Array[Double]] = Array.fill(n){ Array.fill(m){ 0.0 } }
    val aCur: Array[Double] = Array.fill(n) { 0.0 }
    var cCur: Int = 0

    chiselMainTest(testArgs, c){ c => new KafTrainer(c) }

    val res = Array.fill(nTrain){ 0.0 }

    // Cut up series prediction into nTrain bits.
    //for(i <- 0 until nTrain) {
    //    chiselMainTest(testArgs, p){ p => new KafPredictor(p, i) }
    //}
    //val f = new FileWriter(f"output/$testName%s.csv")
    //f.write(res.mkString("",",\n",","))
    //f.close()

    // Run predictor on all examples.
    chiselMainTest(testArgs, p){ p => new KafPredictorSingle(p) }

    //Seq("rm", "output/hwtmp/*").!
}

