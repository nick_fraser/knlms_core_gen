
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

//n,m are positive ints.
//Size of doReg = 2

class AddToDict[T <: Data](dtype: T, val doReg: Array[Boolean], val n: Int, val m: Int, gt: (T, T) => Bool, DtoT: Double => T, pgt: Int = 0, preDelay: Int = 0) extends Module {
    val io = new Bundle{
        val k = Vec.fill(n){dtype.clone.asInput}
        val knn = dtype.clone.asInput
        val x = Vec.fill(m){dtype.clone.asInput}
        val thresh = dtype.clone.asInput
        val valid_in = Bool(INPUT)
        val D = Vec.fill(m*n){dtype.clone.asOutput}
        val kbar = Vec.fill(n){dtype.clone.asOutput}
        val c = UInt(OUTPUT, log2Up(n) + 1)
        val valid_out = Bool(OUTPUT)
    }

    //Determine whether current entry should be added using coherence criterion.
    val ttree = Module(new PipelinedTree[Bool](Bool(), doReg(0) +: Array.fill(log2Up(n)-1){ false } :+ doReg(1), n, _ | _))
    for (i <- 0 until n) {
        ttree.io.x(i) := gt(io.k(i), io.thresh)
    }

    //Delay the valid signal by the same length as the pipelined tree.
    val dval = Module(new VarShiftRegister[Bool](Bool(), Array.fill(ttree.latency + pgt){ true }, 1, Bool(false)))
    dval.io.vin(0) := io.valid_in
    io.valid_out := dval.io.vout(0)

    //Increment counter.
    val c = Reg(init = UInt(0, log2Up(n) + 1))
    val update = if(preDelay == 0) {
        (!ttree.io.res) && dval.io.vout(0) && (c < UInt(n))
    } else {
        val dictCount = Reg(init = UInt(0, log2Up(preDelay + pgt + ttree.latency)))
        val dictFlag = dictCount === UInt(0)
        val tmpUpdate = (!ttree.io.res) && dval.io.vout(0) && (c < UInt(n)) && dictFlag
        when(tmpUpdate) {
            dictCount := UInt(preDelay + pgt + ttree.latency)
        } .otherwise {
            dictCount := dictCount - (!dictFlag)
        }
        tmpUpdate
    }
    val cn = c + update
    c := cn
    io.c := cn

    //delay x by the same amount as doReg (the pipelined tree).
    val xsr = Module(new VarShiftRegister[T](dtype, Array.fill(ttree.latency + pgt){ true }, m))
    for (i <- 0 until m) {
        xsr.io.vin(i) := io.x(i)
    }

    //delay k and knn by the same amount as doReg (the pipelined tree).
    val ksr = Module(new VarShiftRegister[T](dtype, Array.fill(ttree.latency + pgt){ true }, n))
    for (i <- 0 until n) {
        ksr.io.vin(i) := io.k(i)
    }
    val knnsr = Module(new VarShiftRegister[T](dtype, Array.fill(ttree.latency + pgt){ true }, 1))
    knnsr.io.vin(0) := io.knn

    // Update dictionary & connect to outputs. Stored in row major format.
    val D = Vec.fill(n*m){dtype.clone}
    for (i <- 0 until n) {
        for (j <- 0 until m) {
            val index: Int = j + i*m
            val reg = RegInit(DtoT(0.0))
            reg := D(index)
            when (((UInt(i + 1)) === cn) && update) {
                D(index) := xsr.io.vout(j)
            } .otherwise {
                D(index) := reg
            }
            io.D(index) := D(index)
        }
    }

    // Update dictionary & connect to outputs. Stored in row major format.
    for (i <- 0 until n) {
        when (((UInt(i + 1)) === cn) && update) {
            io.kbar(i) := knnsr.io.vout(0)
        } .otherwise {
            io.kbar(i) := ksr.io.vout(i)
        }
    }

    val latency: Int = pgt + ttree.latency
}

