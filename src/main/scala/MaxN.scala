
package PKAF

import Chisel._

// n is any positive int.
// doReg is of length log2Up(n)

class MaxN[T <: Data](dtype: T, val doReg: Array[Boolean], val n : Int, max: (T, T) => T, pmax: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){ dtype.clone.asInput }
        val res = dtype.clone.asOutput
    }

    val tree = Module(new PipelinedTree[T](dtype, false +: doReg, n, max, pmax))

    //Connect inputs to the tree.
    for (i <- 0 until n) {
        tree.io.x(i) := io.x(i)
    }
    io.res := tree.io.res

    val latency = tree.latency
}

object FloMax {
    def max(x: Flo, y: Flo): Flo = {
        val m = Flo()
        when(x > y) {
            m := x
        } .otherwise {
            m := y
        }
        m
    }
}

