
package PKAF

import Chisel._

//class Rege[T <: Data](dtype: T, doReg: Boolean, init: T = None) extends Module {
class Rege[T <: Data](dtype: T, doReg: Boolean, init: T = null.asInstanceOf[T]) extends Module {
    val io = new Bundle {
        val din = dtype.clone.asInput
        val dout = dtype.clone.asOutput
    }

    if(!doReg) {
        io.dout := io.din
    } else {
        val preg = if(init == null) Reg(dtype) else RegInit(init)
        preg := io.din
        io.dout := preg
    }
}

object CountReg {
    def nreg(regs: Array[Boolean]): Int = {
        var cnt: Int = 0
        for (i <- 0 until regs.size) {
            if(regs(i))
                cnt += 1
        }
        cnt
    }
}

