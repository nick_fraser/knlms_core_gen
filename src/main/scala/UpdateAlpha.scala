
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

//n,m are any positive ints.
//Size of doReg 3?

class NormalisedUpdateAlpha[T <: Data](dtype: T, val doReg: Array[Boolean], val n: Int, mul: (T, T) => T, add: (T, T) => T, div: (T, T) => T, DtoT: Double => T, pmul: Int = 0, padd: Int = 0, pdiv: Int = 0) extends Module {
    val io = new Bundle {
        val k = Vec.fill(n){dtype.clone.asInput}
        val epsilon = dtype.clone.asInput
        val eta = dtype.clone.asInput
        val err = dtype.clone.asInput
        val kdotk = dtype.clone.asInput
        val valid_in = Bool(INPUT)
        val alpha = Vec.fill(n){dtype.clone.asOutput}
        val valid_out = Bool(OUTPUT)

        //Debugging signals.
    }

    try {
        if (padd != 0) throw new Exception("padd must be 0.")
        if (padd != pmul) throw new Exception("padd and pmul must be equal.")
    }

    //First register.
    val den = Module(new Rege(dtype, doReg(0)))
    den.io.din := add(io.kdotk, io.epsilon)

    val num = Module(new Rege(dtype, doReg(0)))
    num.io.din := mul(io.eta, io.err)

    //Second register.
    val step = Module(new Rege(dtype, doReg(1)))
    step.io.din := div(num.io.dout, den.io.dout)

    //delay k by the same amount as doReg (the pipelined tree).
    val ksr = Module(new VarShiftRegister[T](dtype, Array(doReg(0), doReg(1)) ++ Array.fill(pdiv){ true }, n))
    for (i <- 0 until n) {
        ksr.io.vin(i) := io.k(i)
    }

    val deltaAlpha = Module(new VectorScalar[T](dtype, Array(doReg(2)), n, mul, pmul))
    deltaAlpha.io.alpha := step.io.dout
    for (i <- 0 until n) {
        deltaAlpha.io.x(i) := ksr.io.vout(i)
    }

    val vsr = Module(new VarShiftRegister[Bool](Bool(false), Array(doReg(0), doReg(1)) ++ Array.fill(pmul + pdiv + deltaAlpha.latency){ true }, 1))
    vsr.io.vin(0) := io.valid_in

    //Update alpha.
    val alphaReg = Vec.fill(n){ RegInit(DtoT(0.0)) }
    for (i <- 0 until n) {
        val tmp = dtype.clone
        when (vsr.io.vout(0)) {
            tmp := add(alphaReg(i), deltaAlpha.io.y(i))
        } .otherwise {
            tmp := alphaReg(i)
        }
        alphaReg(i) := tmp
        io.alpha(i) := tmp
    }
    io.valid_out := vsr.io.vout(0)

    val latency: Int = CountReg.nreg(Array(doReg(0), doReg(1))) + pmul + pdiv + deltaAlpha.latency
}

