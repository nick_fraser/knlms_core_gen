
package PKAF

import Chisel._
import util.Random

//A module to test a single operation.
class OpMod[T <: Data](dtype: T, val numInRegs : Int, val numOutRegs : Int, f: (T, T) => T, pf: Int = 0) extends Module {
    val io = new Bundle {
        val x = dtype.clone.asInput
        val y = dtype.clone.asInput
        val res = dtype.clone.asOutput
    }
    if ((numInRegs == 0) && (numOutRegs == 0)) {
        io.res := f(io.x, io.y)
    }
    else if (numOutRegs == 0) {
        val srin = Module(new VarShiftRegister(dtype, Array.fill(numInRegs){ true }, 2))
        srin.io.vin(0) := io.x
        srin.io.vin(1) := io.y
        io.res := f(srin.io.vout(0), srin.io.vout(1))
    }
    else if (numInRegs == 0) {
        val srres = Module(new VarShiftRegister(dtype, Array.fill(numOutRegs){ true }, 1))
        srres.io.vin(0) := f(io.x, io.y)
        io.res := srres.io.vout(0)
    }
    else {
        val srin = Module(new VarShiftRegister(dtype, Array.fill(numInRegs){ true }, 2))
        srin.io.vin(0) := io.x
        srin.io.vin(1) := io.y
        val srres = Module(new VarShiftRegister(dtype, Array.fill(numOutRegs){ true }, 1))
        srres.io.vin(0) := f(srin.io.vout(0), srin.io.vout(1))
        io.res := srres.io.vout(0)
    }

    val latency: Int = pf + numInRegs + numOutRegs 
}

//A module to test a single function.
class FunMod[T <: Data](dtype: T, val numInRegs : Int, val numOutRegs : Int, f: T => T, pf: Int = 0) extends Module {
    val io = new Bundle {
        val x = dtype.clone.asInput
        val res = dtype.clone.asOutput
    }
    if ((numInRegs == 0) && (numOutRegs == 0)) {
        io.res := f(io.x)
    }
    else if (numOutRegs == 0) {
        val srin = Module(new VarShiftRegister(dtype, Array.fill(numInRegs){ true }, 1))
        srin.io.vin(0) := io.x
        io.res := f(srin.io.vout(0))
    }
    else if (numInRegs == 0) {
        val srres = Module(new VarShiftRegister(dtype, Array.fill(numOutRegs){ true }, 1))
        srres.io.vin(0) := f(io.x)
        io.res := srres.io.vout(0)
    }
    else {
        val srin = Module(new VarShiftRegister(dtype, Array.fill(numInRegs){ true }, 1))
        srin.io.vin(0) := io.x
        val srres = Module(new VarShiftRegister(dtype, Array.fill(numOutRegs){ true }, 1))
        srres.io.vin(0) := f(srin.io.vout(0))
        io.res := srres.io.vout(0)
    }

    val latency: Int = pf + numInRegs + numOutRegs 
}

