
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

/** Polynomial evaluation using Estrin's method.
  *
  * The method is from 'The Art of Computer Programming: Seminumerical Algorithms', page 488.
  * Estrin's method is equivalent to a parallel version of horner's method.
  * doReg.length = 2*log2Up(order+1)
  * coefficients are in order from largest powers of x to the smallest.
  * Need to add a check that doReg is the right size.
  */
class PolyEvalEstrins[T <: Bits](dtype: T, val order: Int, val doReg: Array[Boolean], add: (T, T) => T, mul: (T, T) => T, padd: Int = 0, pmul: Int = 0) extends Module {
    /** A recursive function to create the hardware for Estrin's method. Note: coeffs in ordered in largest powers of x to the smallest. */
    def estrins(x: T, coeffs: Vec[T], doReg: Array[Boolean]): T = {
        val ops = coeffs.length
        val odd = !(ops % 2 == 0)
        val regs = Array(doReg(0), doReg(1))
        val nextReg = doReg.slice(2, doReg.length)
        val next = ArrayBuffer[T]()
        val addReg = ArrayBuffer[T]()
        val curReg = coeffs.to[ArrayBuffer]

        // Pass the leftover value straight to the next stage (with some delay).
        if (odd) {
            val sr = Module(new VarShiftRegister(dtype, regs ++ Array.fill(pmul + padd){ true }, 1))
            sr.io.vin(0) := coeffs(0)
            curReg -= curReg(0)
            next += sr.io.vout(0)
        }

        // Calculate multiply stages.
        while (curReg.length > 0) {
            val r = Module(new Rege(dtype, regs(0)))
            val sr = Module(new VarShiftRegister(dtype, regs.slice(0, 1) ++ Array.fill(pmul){ true }, 1))
            r.io.din := mul(curReg(0), x)
            sr.io.vin(0) := curReg(1)

            // Add values to the next stage.
            addReg += r.io.dout
            addReg += sr.io.vout(0)

            // Remove values from curReg.
            curReg -= curReg(0)
            curReg -= curReg(0)
        }

        // Calculate the addition stages, addReg to nextReg.
        while (addReg.length > 0) {
            val r = Module(new Rege(dtype, regs(1)))
            r.io.din := add(addReg(0), addReg(1))
            next += r.io.dout

            // Remove values from addReg.
            addReg -= addReg(0)
            addReg -= addReg(0)
        }

        if (next.length == 1) {
            next(0)
        } else {
            // Calculate x^2 and delay it, create next stage in the tree.
            val sr = Module(new VarShiftRegister(dtype, regs ++ Array.fill(padd){ true }, 1))
            sr.io.vin(0) := mul(x,x)
            estrins(sr.io.vout(0), Vec(next), nextReg)
        }
    }

    val io = new Bundle{
        val x = dtype.clone.asInput
        val coeffs = Vec.fill(order + 1){ dtype.clone.asInput }
        val y = dtype.clone.asOutput
    }

    /*
    val counter = Reg(init=UInt(0, width=32))
    counter := counter + UInt(1)
    printf("cnt: %d ", counter);

    for (i <- 0 to order) {
        if (i == 0) printf("{%d, ", io.coeffs(i))
        else if (i == order) printf("%d}\n", io.coeffs(i))
        else printf("%d, ", io.coeffs(i))
    }
    */

    if (order == 0) {
        io.y := io.coeffs(0)
    } else {
        io.y := estrins(io.x, io.coeffs, doReg)
    }
}

