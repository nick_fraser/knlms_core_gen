
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

/*
class KAFSPFixed extends Bits with Num[KAFSPFixed] {
    var iL: Int = -1
    var bw: Int = -1
    type T = KAFSPFixed; //Not sure if this is needed.
    def matched (b: KAFSPFixed): Boolean = (this.iL == b.iL) && (this.bw == b.bw)
    def typeAssert (b: KAFSPFixed): Unit = if (!this.matched(b)) throw new Exception("Combining unmatched types in KAFSPFixed. This: bw=" + this.bw + ", iL=" + this.iL +  ". B: bw=" + b.bw + ", iL=" + b.iL + ".")
    def unary_-(): KAFSPFixed = castToKAFSPFixed(chiselCast(this){ SInt() }.unary_-(), bw, iL)
    def + (b: KAFSPFixed): KAFSPFixed = {
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } + chiselCast(b){ SInt() }
        castToKAFSPFixed(s.toSInt, bw, iL)
    }
    def * (b: KAFSPFixed): KAFSPFixed = { 
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } * chiselCast(b){ SInt() }
        val msb: Int = 2*bw-iL-1
        castToKAFSPFixed(s(msb,msb-bw+1).toSInt, bw, iL)
    }
    def / (b: KAFSPFixed): KAFSPFixed = { 
        this.typeAssert(b)
        //val s = Cat(chiselCast(this){ SInt() } / chiselCast(b){ SInt() }, chiselCast(this){ SInt() } % chiselCast(b){ SInt() })
        //val msb: Int = 2*bw-iL-1
        //castToKAFSPFixed(s(msb, msb-bw+1).toSInt,iL)
        val s = Cat(chiselCast(this){ SInt() }, SInt(0, bw-iL)).toSInt / chiselCast(b){ SInt() }
        val msb: Int = bw-1
        castToKAFSPFixed(s(msb, msb-bw+1).toSInt, bw, iL)
    }
    def % (b: KAFSPFixed): KAFSPFixed = { 
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } % chiselCast(b){ SInt() }
        val msb: Int = bw-1
        castToKAFSPFixed(s(msb,msb-bw+1)toSInt, bw, iL)
    }
    def - (b: KAFSPFixed): KAFSPFixed = {
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } - chiselCast(b){ SInt() }
        //castToKAFSPFixed(s(bw-1,0).toSInt, bw, iL)
        castToKAFSPFixed(s.toSInt, bw, iL)
    }
    def < (b: KAFSPFixed): Bool = {
        this.typeAssert(b)
        chiselCast(this){ SInt() } < chiselCast(b){ SInt() }
    }
    def <= (b: KAFSPFixed): Bool = {
        this.typeAssert(b)
        chiselCast(this){ SInt() } <= chiselCast(b){ SInt() }
    }
    def > (b: KAFSPFixed): Bool = {
        this.typeAssert(b)
        chiselCast(this){ SInt() } > chiselCast(b){ SInt() }
    }
    def >= (b: KAFSPFixed): Bool = {
        this.typeAssert(b)
        chiselCast(this){ SInt() } >= chiselCast(b){ SInt() }
    }

    def truncate(): SInt = this(bw-1, bw-iL).toSInt

    override def fromNode(n: Node): this.type = {
        KAFSPFixed(OUTPUT).asTypeFor(n).asInstanceOf[this.type]
    }

    override def fromInt(x: Int): this.type = {
        KAFSPFixed(x).asInstanceOf[this.type]
    }

    override def clone: this.type = {
        val res = super.clone
        res.iL = iL
        res.bw = bw
        res
    }

    def castToKAFSPFixed(s: SInt, width: Int, iL: Int): KAFSPFixed = {
        val k = chiselCast(s){ KAFSPFixed() }
        k.iL = iL
        k.bw = width
        k
    }

    def frac(): KAFSPFixed = {
        val s = Cat(SInt(0, iL), this(bw-iL-1, 0)).toSInt
        val k = castToKAFSPFixed(s, bw, iL)
        k
    }
}

object KAFSPFixed {
    def apply(x: Int): KAFSPFixed = Lit(x){ KAFSPFixed() };
    def apply(x: Int, width: Int): KAFSPFixed = {
        val k = Lit(x, width){ KAFSPFixed() }
        k.bw = width
        k
    }
    def apply(x: Int, width: Int, iL: Int): KAFSPFixed = { 
        val res = Lit(x, width){ KAFSPFixed() }
        res.iL = iL
        res.bw = width
        res
    }
    def apply(x: BigInt): KAFSPFixed = Lit(x){ KAFSPFixed() };
    def apply(x: BigInt, width: Int): KAFSPFixed = {
        val k = Lit(x, width){ KAFSPFixed() }
        k.bw = width
        k
    }
    def apply(x: BigInt, width: Int, iL: Int): KAFSPFixed = {
        val res = Lit(x, width){ KAFSPFixed() }
        res.iL = iL
        res.bw = width
        res
    }
    def apply(dir: IODirection = null, width: Int = -1, iL: Int = -1): KAFSPFixed = {
        val res = new KAFSPFixed();
        res.create(dir, width)
        res.iL = iL
        res.bw = width
        res
    }
    def apply(b: Double, width: Int, iL: Int): KAFSPFixed = {
        val bin: BigInt = (b*math.pow(2,width-iL)).toInt
        val k = KAFSPFixed(bin, width, iL)
        k
    }
    def toD(b: KAFSPFixed): Double = {
        val bin: BigInt = if(b(b.bw-1).litValue() == 1) b.litValue() - math.pow(2, b.bw).toInt else b.litValue()
        val d: Double = (bin.toDouble*math.pow(2,b.iL-b.bw))
        d
    }
    def toD(ubin: BigInt, b: KAFSPFixed): Double = {
        val bin: BigInt = if(ubin >= math.pow(2, b.bw-1).toInt) ubin - math.pow(2, b.bw).toInt else ubin
        val d: Double = (bin.toDouble*math.pow(2,b.iL-b.bw))
        d
    }
}
*/

object TtoDouble {
    def apply[T <: Data](v: Vec[T], toD: T => Double): Array[Double] = {
        var x = new Array[Double](v.length)
        for(i <- 0 until v.length)
            x(i) = toD(v(i))
        x
    }
    def reverse[T <: Data](v: Array[Double], fromD: Double => T): Vec[T] = {
        var x = new ArrayBuffer[T]()
        for(i <- 0 until v.length)
            x += fromD(v(i))
        Vec(x)
    }
}

