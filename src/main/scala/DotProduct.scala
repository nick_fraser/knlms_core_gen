
package PKAF

import Chisel._

// n is any positive int
// doReg is of length log2Up(n) + 1

class DotProdTree[T <: Data](dtype: T, val doReg: Array[Boolean], val n : Int, mul: (T, T) => T, add: (T, T) => T, pmul: Int = 0, padd: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){dtype.clone.asInput}
        val y = Vec.fill(n){dtype.clone.asInput}
        val res = dtype.clone.asOutput
    }

    val tree = Module(new PipelinedTree[T](dtype, doReg, n, add, padd))

    //Multiply and connect to tree.
    for (i <- 0 until n) {
        tree.io.x(i) := mul(io.x(i), io.y(i))
    }
    io.res := tree.io.res

    val latency: Int = tree.latency + pmul
}

