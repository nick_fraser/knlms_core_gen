
package PKAF

import Chisel._

// n is any positive int.
// doReg is of length 1.

class VectorScalar[T <: Data](dtype: T, doReg: Array[Boolean], n: Int, f: (T, T) => T, pf: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(n){dtype.clone.asInput}
        val alpha = dtype.clone.asInput
        val y = Vec.fill(n){dtype.clone.asOutput}
    }

    for (i <- 0 until n) {
        val reg = Module(new Rege(dtype, doReg(0)))
        reg.io.din := f(io.x(i), io.alpha)
        io.y(i) := reg.io.dout
    }

    val latency: Int = CountReg.nreg(doReg) + pf
}

