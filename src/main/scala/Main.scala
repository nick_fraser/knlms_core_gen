
package PKAF

import Chisel._
import scala.sys.process._
import java.io._
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

object UnitTests {
    /** A main function which generates verilog for the KNLMS algorithm.
      *
      * The main function provides the following tasks:
      *     -generates verilog of KNLMS with some set parameters.
      *     -generates, synthesises and P&Rs several designs for altera (DE5) or Xilinx (VC707).
      * TODO: Allow passing of parameters for KNLMS generation.
      * TODO: Allow generation of PsspFixed hardware.
      */
    def main(args: Array[String]): Unit = {
        val tutArgs = args.slice(1, args.length) 
        val w = 18
        val iL = 5
        val res = 
        args(0) match {
            case "KNLMS" =>
                // KNLMS parameters.
                val n: Int = 8
                val m: Int = 2
                val neg_gamma = -0.5
                val thresh = 0.5
                val epsilon = 0.0001
                val eta = 0.1
                val doReg = Array.fill(KNLMS.getRegLength(n, m)){false}

                // Datatype parameters - PsspFixed.
                val pdiv: Int = 0
                val iL: Int = 5
                val w: Int = 18
                val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
                val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, 0)
                val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
                chiselMain(tutArgs, () => Module(
                    new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
                        neg_gamma, thresh, epsilon, eta, doReg, n, m,
                        _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv)))

            case "KNLMSTimeSeriesWrapper" =>
                // KNLMS parameters.
                val n: Int = 32
                val m: Int = 8
                val p = 64
                val neg_gamma = -1.0
                val thresh = 0.5
                val epsilon = 0.01
                val eta = 0.1

                // Datatype parameters.
                val iL: Int = 5
                val w: Int = 18
                //val divDelay = w-1
                val divDelay = 6 // Use this for divLutLi.
                val expDelay = 5
                val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
                val pdiv = CountReg.nreg(divReg)
                val pexp = CountReg.nreg(expReg)
                val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
                val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, expReg)
                //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(divReg, _)
                //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(divReg, _)
                val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(thresh, 1.0 + thresh*(n-1), 64, divReg, _)
                val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
                //println(f"p = $p%d, pdiv = $pdiv%d, pexp = $pexp%d, po = ${CountReg.nreg(doReg)}%d")
                chiselMain(tutArgs, () => Module(
                    new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
                        neg_gamma, thresh, epsilon, eta, doReg, n, m,
                        _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv)))

                //val fromD: Double => Flo = Flo(_)
                //val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -4, 0, 64, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
                //val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
                //chiselMainTest(tutArgs, () => Module(
                //    new KNLMSTimeSeriesWrapper[Flo](Flo(), fromD(neg_gamma), fromD(thresh), fromD(epsilon), fromD(eta), doReg, n, m,
                //        _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                //            c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, false)
                //        }

            case "KnlmsTesterAltera" =>
                val wl: Int = 18
                val il: Int = 5

                //Outer loop.
                //val n: Int = 2 //Number of dictionary entries.
                //val m: Int = 2 //Number of features in input vector.
                //val ps = Array[Int](0, 1, 2, 4, 8, 16, 32, 64) // These will be executed in ascending order
                //val ns = Array[Int](2, 4, 8, 16, 32, 64, 128)
                //val ms = Array[Int](2, 4, 8, 16, 32, 64, 128)
                val ps = Array[Int](0)
                val ns = Array[Int](2)
                val ms = Array[Int](2)
                val knlmsTypes = Array[(Boolean, Boolean, String)]((true, false, "DKNLMS"), (false, false, "MDKNLMS"), (false, true, "MDKNLMS+DG"))
                //val divTypes = Array[((PsspFixed, Array[Boolean], PsspFixed) => PsspFixed, Int, String)](
                //    (_.manualDivisionNew(_, _),
                //    17,
                //    "manualDivisionNew"),
                //    (_.divLutLi(0.1, 15.9, 64, _, _),
                //    6,
                //    "divLutLi(0.1, 15.9, 64)"),
                //    (_.manualDivision(_, _),
                //    17,
                //    "manualDivision"),
                //    (_.divLutPow2(0.5, 8.4921875, 1024, _, _), // Operation to test
                //    4,
                //    "divLutPow2-0.5_8.4921875_1024_0") // Label to use for the Op
                //)
                //val ps = Array[Int](0, 1)
                //val ns = Array[Int](2, 4)
                //val ms = Array[Int](2, 4)
                //val knlmsTypes = Array[(Boolean, Boolean, String)]((true, false,"DKNLMS"))
                val expTypes = Array[((PsspFixed, Array[Boolean]) => PsspFixed, Int, String)](
                    (_.funLutPow2(-3.99609375, 0, math.exp(_), 1024, _), // Function to test
                    3,
                    "funLutPow2--3.99609375_0_math.exp_1024") // Label to use for the Op
                )
                val divTypes = Array[((PsspFixed, Array[Boolean], PsspFixed) => PsspFixed, Int, String)](
                    (_.divLutPow2(0.5, 8.4921875, 1024, _, _), // Operation to test
                    4,
                    "divLutPow2-0.5_8.4921875_1024") // Label to use for the Op
                )
                val clean = false // Should we delete the generated projects when finished.
                val projectTemplateDir = "src/main/scripts/altera"
                val outputDir = "output"
                val outputFile = f"$outputDir%s/altera_designs.csv"
                val writer = new FileWriter(outputFile)
                writer.write("Dictionary Length, Feature Length, Latency (cycles), KNLMS Type, Exp Type, Div Type, p, preg, pexp, pdiv, Fmax (MHz), DSPs, ALMs, BRAMs, Synth. + P&R time (ms)\n")
                writer.close()

                val permutations = ps.length * ns.length * ms.length * knlmsTypes.length * expTypes.length * divTypes.length
                var count = 0

                for (n <- ns) {
                    for (m <- ms) {
                        for (p <- ps.sortWith(_<_)) {
                            for ((divFun, divDelay, divType) <- divTypes) {
                                for ((expFun, expDelay, expType) <- expTypes) {
                                    for ((singleDelay, addMask, dknlmsType) <- knlmsTypes) {
                                        //val divDelay = 17
                                        //val expDelay = 5
                                        val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
                                        val regCount = CountReg.nreg(doReg)
                                        val pexp: Int = CountReg.nreg(expReg)
                                        val pdiv: Int = CountReg.nreg(divReg)
                                        val latency: Int = regCount + pexp + pdiv
                                        var skip: Boolean = latency != p
                                        if (skip) { // Work out if we should skip this example.
                                            val previousIndex = ps.indexWhere(p == _)-1
                                            if (previousIndex < 0) {
                                                skip = false
                                            } else {
                                                skip = latency <= ps(previousIndex)
                                            }
                                        }
                                        val fromD: Double => PsspFixed = PsspFixed(_, wl, il, 0, 0, 0, pdiv, 0)
                                        val dtype = PsspFixed(width=w,iL=iL,pdiv=pdiv)
                                        val toD: PsspFixed => Double = PsspFixed.toD
                                        val typetoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=wl, iL=il, pdiv=pdiv))
                                        //val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, expReg)
                                        val exp: PsspFixed => PsspFixed = expFun(_, expReg)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(0.1, 15.9, 64, divReg, _)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(divReg, _)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(divReg, _)
                                        val div: (PsspFixed, PsspFixed) => PsspFixed = divFun(_, divReg, _)
                                        //Generate the verilog code.
                                        chiselMain(tutArgs, () => Module(
                                            new KNLMSTimeSeriesWrapper[PsspFixed](dtype, -0.5, 0.5, 0.0001, 0.01,
                                                doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD,
                                                pdiv=pdiv, pexp=pexp, singleDelay=singleDelay, addMask=addMask)))
            
                                        val name = f"$outputDir%s/altera_knlmsts_n$n%03d_m$m%03d_p$p%02d_doReg$regCount%02d_pdiv$pdiv%02d_pexp$pexp%02d"
            
                                        count += 1
                                        print(f"Synthesizing Design: $count%d of $permutations%d...\n")
                                        print(f"Configuration: n=$n%d, m=$m%d, latency=$latency%d, dknlmsType=$dknlmsType%s, expType=$expType%s, divType=$divType%s, p=$p%d, preg=$regCount%d, pexp=$pexp%d, pdiv=$pdiv%d\n")

                                        if (skip) {
                                            print(f"Configuration redundant, skipping...\n")
                                        } else {
                                            try {
                                                if (Seq("cp", "-r", projectTemplateDir, name).! != 0) throw new Exception(f"Failed to copy $projectTemplateDir%s to $name%s") //Copy the template directory.
                                                if (Seq("cp", "verilog/KNLMSTimeSeriesWrapper.v", name).! != 0) throw new Exception(f"Failed to copy verilog files to $name%s") //Copy the verilog file.
                                                val t1 = System.currentTimeMillis
                                                if (Seq("bash", "-c", f"cd $name%s && quartus_sh --flow compile KNLMS > /dev/null").! != 0) throw new Exception(f"Failed to build $name%s/KNLMS project") //Compile project.
                                                val t2 = System.currentTimeMillis // Get running time of the compilation process in ms.
                                                val time = t2-t1
                                                val AreaStr = Seq("cat", f"$name%s/output_files/KNLMS.fit.summary").!!
                                                val FmaxStr = Seq("bash", "-c", f"cat $name%s/output_files/KNLMS.sta.rpt | grep -E '(Fmax|MHz)'").!!
                                                if (Seq("bash", "-c", f"cd $name%s && quartus_sh --clean KNLMS > /dev/null").! != 0) throw new Exception(f"Failed to clean $name%s/KNLMS project") //Compile project.
                                                if (Seq("bash", "-c", f"cd $name%s && rm -rf db/ *.ddb simulation/ output_files/*.sof").! != 0) throw new Exception(f"Failed to purge $name%s/KNLMS project")
            
                                                //Retrieve Fmax.
                                                val regFmax = """\d+\.\d+\sMHz""".r //Define regex for retrieving a list of clock frequencies.
                                                val Fmaxs = (regFmax.findAllIn(FmaxStr)).toList //Get a list of strings XXX.X MHz.
                                                val freq = Fmaxs(Fmaxs.size-1).split(" ")(0).toDouble //Convert the highest frequency to a double.
            
                                                //Retrieve Area usage.
                                                val regArea = """(,|\d)+""".r
                                                val area = Array("ALMs", "block memory", "DSP")
                                                val areaUse = Array.fill(area.length){ 0 }
                                                for (i <- 0 until area.length) {
                                                    val regTmp = (area(i) + """.+?(,|\d)+""").r
                                                    val frag = regTmp.findFirstIn(AreaStr).toList(0)
                                                    val intStr = regArea.findFirstIn(frag).toList(0).trim
                                                    areaUse(i) = """,""".r.replaceAllIn(intStr, "").toInt
                                                }
                                                val alms = areaUse(0)
                                                val brams = areaUse(1)
                                                val dsps = areaUse(2)
            
                                                // Write the output to a csv file.
                                                val writer = new FileWriter(outputFile, true)
                                                writer.write(f"$n%d, $m%d, $latency%d, $dknlmsType%s, $expType%s, $divType%s, $p%d, $regCount%d, $pexp%d, $pdiv%d, $freq%3.2f, $dsps%d, $alms%d, $brams%d, $time%d\n")
                                                writer.close()
            
                                                //Remove the directory.
                                                if (clean) {
                                                    Seq("rm", "-rf", name).!
                                                }
                                            }
                                            catch {
                                                case other: Exception =>
                                                println("Exception caught: " + other)
                                                if(clean) {
                                                    Seq("rm", "-rf", name).!
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            case "KnlmsTesterVivado" =>
                val wl: Int = 18
                val il: Int = 5

                //Outer loop.
                //val n: Int = 2 //Number of dictionary entries.
                //val m: Int = 2 //Number of features in input vector.
                //val ps = Array[Int](0, 1, 2, 4, 8, 16, 32, 64) // These will be executed in ascending order
                //val ns = Array[Int](2, 4, 8, 16, 32, 64, 128)
                //val ms = Array[Int](2, 4, 8, 16, 32, 64, 128)
                val ps = Array[Int](0)
                val ns = Array[Int](2)
                val ms = Array[Int](2)
                val knlmsTypes = Array[(Boolean, Boolean, String)]((true, false, "DKNLMS"), (false, false, "MDKNLMS"), (false, true, "MDKNLMS+DG"))
                val expTypes = Array[((PsspFixed, Array[Boolean]) => PsspFixed, Int, String)](
                    (_.funLutPow2(-3.99609375, 0, math.exp(_), 1024, _), // Function to test
                    3,
                    "funLutPow2--3.99609375_0_math.exp_1024") // Label to use for the Op
                )
                val divTypes = Array[((PsspFixed, Array[Boolean], PsspFixed) => PsspFixed, Int, String)](
                    (_.divLutPow2(0.5, 8.4921875, 1024, _, _), // Operation to test
                    4,
                    "divLutPow2-0.5_8.4921875_1024") // Label to use for the Op
                )
                //val device = "xc7z020clg400-1" // PYNQ-Z1
                //val device = "xczu3eg-sbva484-1-i" // Ultra96
                //val device = "xc7vx485tffg1761-2" // VC707
                val device = "xcvu9p-flgb2104-2-i" // AWS-F1
                val clkNs = 2.00 // Target clock constraint for the generated project in ns.
                val clkMhz = 1.0e3 / clkNs // Target clock constraint for the generated project in Mhz.
                val clean = false // Should we delete the generated projects when finished.
                val projectTemplateDir = "src/main/scripts/vivado"
                val outputDir = "output"
                val outputFile = f"$outputDir%s/vivado_designs.csv"
                val writer = new FileWriter(outputFile)
                writer.write("Dictionary Length, Feature Length, Latency (cycles), KNLMS Type, Exp Type, Div Type, p, preg, pexp, pdiv, Fmax (MHz), DSPs, Slices/CLBs (LUTs), Slices/CLBs (Registers), BRAMs, Synth. + Impl. + P&R time (ms), Device, Target Clock\n")
                writer.close()

                val permutations = ps.length * ns.length * ms.length * knlmsTypes.length * expTypes.length * divTypes.length
                var count = 0

                for (n <- ns) {
                    for (m <- ms) {
                        for (p <- ps.sortWith(_<_)) {
                            for ((divFun, divDelay, divType) <- divTypes) {
                                for ((expFun, expDelay, expType) <- expTypes) {
                                    for ((singleDelay, addMask, dknlmsType) <- knlmsTypes) {
                                        //val divDelay = 6
                                        //val expDelay = 5
                                        val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
                                        val regCount = CountReg.nreg(doReg)
                                        val pexp: Int = CountReg.nreg(expReg)
                                        val pdiv: Int = CountReg.nreg(divReg)
                                        val latency: Int = regCount + pexp + pdiv
                                        var skip: Boolean = latency != p
                                        if (skip) { // Work out if we should skip this example.
                                            val previousIndex = ps.indexWhere(p == _)-1
                                            if (previousIndex < 0) {
                                                skip = false
                                            } else {
                                                skip = latency <= ps(previousIndex)
                                            }
                                        }
                                        val fromD: Double => PsspFixed = PsspFixed(_, wl, il, 0, 0, 0, pdiv, 0)
                                        val dtype = PsspFixed(width=w,iL=iL,pdiv=pdiv)
                                        val toD: PsspFixed => Double = PsspFixed.toD
                                        val typetoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=wl, iL=il, pdiv=pdiv))
                                        //val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, expReg)
                                        val exp: PsspFixed => PsspFixed = expFun(_, expReg)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(0.1, 15.9, 64, divReg, _)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(divReg, _)
                                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(divReg, _)
                                        val div: (PsspFixed, PsspFixed) => PsspFixed = divFun(_, divReg, _)
                                        //Generate the verilog code.
                                        chiselMain(tutArgs, () => Module(
                                            new KNLMSTimeSeriesWrapper[PsspFixed](dtype, -0.5, 0.5, 0.0001, 0.01,
                                                doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD,
                                                pdiv=pdiv, pexp=pexp, singleDelay=singleDelay, addMask=addMask)))

                                        val name = f"$outputDir%s/vivado_knlmsts_n$n%03d_m$m%03d_p$p%02d_doReg$regCount%02d_pdiv$pdiv%02d_pexp$pexp%02d"

                                        count += 1
                                        print(f"Synthesizing Design: $count%d of $permutations%d...\n")
                                        print(f"Configuration: n=$n%d, m=$m%d, latency=$latency%d, dknlmsType=$dknlmsType%s, expType=$expType%s, divType=$divType%s, p=$p%d, preg=$regCount%d, pexp=$pexp%d, pdiv=$pdiv%d, device=$device%s, target_clk=$clkMhz%3.2fMHz\n")

                                        if (skip) {
                                            print(f"Configuration redundant, skipping...\n")
                                        } else {
                                            try {
                                                if (Seq("cp", "-r", projectTemplateDir, name).! != 0) throw new Exception(f"Failed to copy $projectTemplateDir%s to $name%s") //Copy the template directory.
                                                if (Seq("cp", "verilog/KNLMSTimeSeriesWrapper.v", name).! != 0) throw new Exception(f"Failed to copy verilog files to $name%s") //Copy the verilog file.
                                                val t1 = System.currentTimeMillis
                                                if (Seq("bash", "-c", f"cd $name%s && make place_n_route CLK_NS=$clkNs%3.2f DEVICE=$device%s DESIGN=KNLMSTimeSeriesWrapper > /dev/null").! != 0) throw new Exception(f"Failed to build $name%s/KNLMS project") //Compile project.
                                                val t2 = System.currentTimeMillis // Get running time of the compilation process in ms.
                                                val time = t2-t1
                                                //grep -E "(Slice (LU|R)|DSPs)" KnlmsTemplate/vivado/KNLMSTimeSeriesWrapper_utilization_opt.rpt
                                                val AreaStr = Seq("bash", "-c", f"grep -E '((Slice|CLB) (LU|R)|DSPs|Block RAM Tile +\\|)' $name%s/KNLMSTimeSeriesWrapper_utilization_route.rpt").!!
                                                //grep clk KnlmsTemplate/vivado/KNLMSTimeSeriesWrapper_timing_opt.rpt | head -n 2
                                                val FmaxStr = Seq("bash", "-c", f"grep clk $name%s/KNLMSTimeSeriesWrapper_timing_route.rpt | head -n 2").!!
                                                //if (Seq("bash", "-c", f"cd $name%s && make clean > /dev/null").! != 0) throw new Exception(f"Failed to clean $name%s/KNLMS project") //Compile project.

                                                //Retrieve Fmax.
                                                val regTarget = """  \d+\.\d+""".r //Define regex for retrieving a list of clock frequencies.
                                                val target = regTarget.findFirstIn(FmaxStr).toList(0).trim.toDouble
                                                val regSlack = """(-| )\d+\.\d+""".r
                                                val slack = regSlack.findFirstIn(FmaxStr.split("\n")(1)).toList(0).trim.toDouble
                                                val Fmax = 1.0e3/(target - slack) //Fmax in MHz.

                                                //Retrieve Area.
                                                val regArea = """\d+""".r
                                                val area = Array.fill(4){ 0 }
                                                for (i <- 0 until area.length) {
                                                    area(i) = regArea.findFirstIn(AreaStr.split("\n")(i)).toList(0).trim.toInt
                                                }
                                                val slices_luts = area(0)
                                                val slices_regs = area(1)
                                                val dsps = area(3)
                                                val brams = area(2)
                                                // Write the output to the csv file.
                                                val writer = new FileWriter(outputFile, true)
                                                writer.write(f"$n%d, $m%d, $latency%d, $dknlmsType%s, $expType%s, $divType%s, $p%d, $regCount%d, $pexp%d, $pdiv%d, $Fmax%3.2f, $dsps%d, $slices_luts%d, $slices_regs%d, $brams%d, $time%d, $device%s, $clkMhz%3.2f\n")
                                                writer.close()

                                                //Remove the directory.
                                                if(clean) {
                                                    Seq("rm", "-rf", name).!
                                                }
                                            }
                                            catch {
                                                case other: Exception =>
                                                println("Exception caught: " + other)
                                                if(clean) {
                                                    Seq("rm", "-rf", name).!
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            case "ExpFunMod" =>

                // Generate some version of the exp function to determine the one most suitable for KNLMS.
                // Make tradeoffs between clock frequency, latency and accuracy.

                // Fixed point wordlength settings.
                val wl = 18
                val il = 5
                val pdiv = 0
                val low = -4.0
                val high = 0.0

                // Using lutLi
                val expDelay = 5
                val expReg = Array.fill(expDelay){ true }
                val exp: PsspFixed => PsspFixed = _.expLutLi(low, high, 64, expReg)

                // Using Remez + estrins.
                //val order = 7
                //val splits = 8
                //val expDelay = 2*log2Up(order+1)+1
                //val expReg = Array.fill(expDelay){ true }
                //val exp: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, Math.exp, Math.exp, order, expReg, splits=splits)

                // Generate the verilog.
                chiselMain(tutArgs, () => Module(new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, exp, expDelay)))

            case "DivOpMod" =>

                // Generate some version of the exp function to determine the one most suitable for KNLMS.
                // Make tradeoffs between clock frequency, latency and accuracy.

                // Fixed point wordlength settings.
                val wl = 18
                val il = 5
                val low = 0.1
                val high = 15.9

                // Using lutLi
                val pdiv = 6
                val divReg = Array.fill(pdiv){ true }
                val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(low, high, 64, divReg, _)

                // Using Remez + estrins.
                //val order = 3
                //val splits = 8
                //val pinv = 2*log2Up(order+1)+1
                //val invReg = Array.fill(pinv){ true }
                //val inv: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, 1/_, -1/Math.pow(_,-2), order, invReg, splits=splits)
                //val pdiv = pinv + 1
                //val div = (a: PsspFixed, b: PsspFixed) => Reg(init=PsspFixed(0.0,w,iL,0,0,0,pdiv=pdiv,0),next=(a*(inv(b))))

                // Generate the verilog.
                chiselMain(tutArgs, () => Module(new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, div, pdiv)))

            case "CompareOpsVivado" =>

                // Generate modules of different Ops to compare resources and clock frequency

                // OpMod / FunMod settings
                val preRegs = 1 // Number of registers to place before the operation
                val postRegs = 1 // Number of register to place after the operation

                // Fixed point wordlength settings.
                val wl = 18
                val il = 5

                val ops = Array[(String, String, PsspFixed, PsspFixed => PsspFixed, (PsspFixed, PsspFixed) => PsspFixed, Int)](
                    (
                        "plus", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _+_, // Operation to test
                        0
                    ),
                    (
                        "multiply", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _*_, // Operation to test
                        0
                    ),
                    (
                        "divide", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _/_, // Operation to test
                        0
                    ),
                    (
                        "divLutLi-0.1_15.9_64_0", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutLi(0.1, 15.9, 64, Array.fill(6){ false }, _), // Operation to test
                        0
                    ),
                    (
                        "divLutLi-0.1_15.9_64_6", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutLi(0.1, 15.9, 64, Array.fill(6){ true }, _), // Operation to test
                        6
                    ),
                    (
                        "divLutLi-0.1_15.9_1024_0", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutLi(0.1, 15.9, 1024, Array.fill(6){ false }, _), // Operation to test
                        0
                    ),
                    (
                        "divLutLi-0.1_15.9_1024_6", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutLi(0.1, 15.9, 1024, Array.fill(6){ true }, _), // Operation to test
                        6
                    ),
                    (
                        "divRemez-0.1_15.9_3_0_8",
                        "OpMod",
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(),
                        (a: PsspFixed, b: PsspFixed) => a*(b.funLutInterpRemez(0.1, 15.9, 1/_, -1/Math.pow(_,-2), 3, Array.fill(2*log2Up(3+1)+1){ false }, splits=8)),
                        0
                    ),
                    (
                        "divRemez-0.1_15.9_3_6_8",
                        "OpMod",
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(),
                        (a: PsspFixed, b: PsspFixed) => a*Reg(init=PsspFixed(0.0,w,iL,0,0,0,pdiv=2*log2Up(3+1)+2,0),next=(b.funLutInterpRemez(0.1, 15.9, 1/_, -1/Math.pow(_,-2), 3, Array.fill(2*log2Up(3+1)+1){ true }, splits=8))),
                        6
                    ),
                    (
                        "divLutPow2-0.5_8.4921875_1024_0", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutPow2(0.5, 8.4921875, 1024, Array.fill(4){ false }, _), // Operation to test
                        0
                    ),
                    (
                        "divLutPow2-0.5_8.4921875_1024_4", // Label to use for the Op
                        "OpMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.abs(), // Place holder
                        _.divLutPow2(0.5, 8.4921875, 1024, Array.fill(4){ true }, _), // Operation to test
                        4
                    ),
                    (
                        "expLutLi--4_0_64_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 64, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "expLutLi--4_0_64_5", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 64, 5), // Function to test
                        _+_, // Place holder
                        5
                    ),
                    (
                        "expLutLi--4_0_1024_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 1024, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "expLutLi--4_0_1024_5", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 1024, 5), // Function to test
                        _+_, // Place holder
                        5
                    ),
                    (
                        "expLutLi--4_0_1024_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 1024, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "expLutLi--4_0_1024_5", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.expLutLi(-4, 0, 1024, 5), // Function to test
                        _+_, // Place holder
                        5
                    ),
                    (
                        "funLutPow2--3.96875_0_math.exp_128_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.96875, 0, math.exp(_), 128, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "funLutPow2--3.96875_0_math.exp_128_3", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.96875, 0, math.exp(_), 128, 3), // Function to test
                        _+_, // Place holder
                        3
                    ),
                    (
                        "funLutPow2--3.99609375_0_math.exp_1024_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.99609375, 0, math.exp(_), 1024, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "funLutPow2--3.99609375_0_math.exp_1024_3", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.99609375, 0, math.exp(_), 1024, 3), // Function to test
                        _+_, // Place holder
                        3
                    ),
                    (
                        "funLutPow2--3.998046875_0_math.exp_2048_0", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.998046875, 0, math.exp(_), 2048, 0), // Function to test
                        _+_, // Place holder
                        0
                    ),
                    (
                        "funLutPow2--3.998046875_0_math.exp_2048_3", // Label to use for the Op
                        "FunMod", // OpMod = 2 inputs, FunMod = 1 input
                        PsspFixed(width=w, iL=iL), // Datatype to use for the Op
                        _.funLutPow2(-3.998046875, 0, math.exp(_), 2048, 3), // Function to test
                        _+_, // Place holder
                        3
                    )
                    //(
                    //    "expRemez--4_0_7_0_8", // Label to use for the Op
                    //    "FunMod",
                    //    PsspFixed(width=w, iL=iL),
                    //    _.funLutInterpRemez(0.1, 15.9, Math.exp, Math.exp, 7, Array.fill(2*log2Up(7+1)+1){ false }, splits=8),
                    //    _+_,
                    //    0
                    //),
                    //(
                    //    "expRemez--4_0_7_9_8", // Label to use for the Op
                    //    "FunMod",
                    //    PsspFixed(width=w, iL=iL),
                    //    _.funLutInterpRemez(0.1, 15.9, Math.exp, Math.exp, 7, Array.fill(2*log2Up(7+1)+1){ true }, splits=8),
                    //    _+_,
                    //    9
                    //)
                )

                //val device = "xc7z020clg400-1" // PYNQ-Z1
                //val device = "xczu3eg-sbva484-1-i" // Ultra96
                //val device = "xc7vx485tffg1761-2" // VC707
                val device = "xcvu9p-flgb2104-2-i" // AWS-F1
                val clkNs = 2.00 // Target clock constraint for the generated project in ns.
                val clkMhz = 1.0e3 / clkNs // Target clock constraint for the generated project in Mhz.
                val clean = false // Should we delete the generated projects when finished.
                val projectTemplateDir = "src/main/scripts/vivado"
                val outputDir = "output"
                val outputFile = f"$outputDir%s/compare_ops_vivado.csv"
                val writer = new FileWriter(outputFile)
                writer.write("Op Type, Fmax (MHz), DSPs, Slices/CLBs (LUTs), Slices/CLBs (Registers), BRAMs, Synth. + Impl. + P&R time (ms), Device, Target Clock (MHz)\n")
                writer.close()

                val permutations = ops.length
                var count = 0
                for ((opName, opType, dtype, fun, op, latency) <- ops) {
                    // Generate the verilog for the current Op
                    if (opType == "OpMod") {
                        chiselMain(tutArgs, () => Module(new OpMod[PsspFixed](dtype, preRegs, postRegs, op, latency)))
                    } else { // (opType == "FunMod")
                        chiselMain(tutArgs, () => Module(new FunMod[PsspFixed](dtype, preRegs, postRegs, fun, latency)))
                    }
                    val name = f"$outputDir%s/vivado_$opName%s"

                    count += 1
                    print(f"Synthesizing design $opName - $count%d of $permutations%d...\n")

                    try {
                        if (Seq("cp", "-r", projectTemplateDir, name).! != 0) throw new Exception(f"Failed to copy $projectTemplateDir%s to $name%s") //Copy the template directory.
                        if (Seq("cp", f"verilog/$opType.v", name).! != 0) throw new Exception(f"Failed to copy verilog files to $name%s") //Copy the verilog file.
                        val t1 = System.currentTimeMillis
                        if (Seq("bash", "-c", f"cd $name%s && make place_n_route CLK_NS=$clkNs%3.2f DEVICE=$device%s DESIGN=$opType > /dev/null").! != 0) throw new Exception(f"Failed to build $name%s/$opType project") //Compile project.
                        val t2 = System.currentTimeMillis // Get running time of the compilation process in ms.
                        val time = t2-t1
                        val AreaStr = Seq("bash", "-c", f"grep -E '((Slice|CLB) (LU|R)|DSPs|Block RAM Tile +\\|)' $name%s/$opType%s_utilization_route.rpt").!!
                        val FmaxStr = Seq("bash", "-c", f"grep clk $name%s/$opType%s_timing_route.rpt | head -n 2").!!

                        //Retrieve Fmax.
                        val regTarget = """  \d+\.\d+""".r //Define regex for retrieving a list of clock frequencies.
                        val target = regTarget.findFirstIn(FmaxStr).toList(0).trim.toDouble
                        val regSlack = """(-| )\d+\.\d+""".r
                        val slack = regSlack.findFirstIn(FmaxStr.split("\n")(1)).toList(0).trim.toDouble
                        val Fmax = 1.0e3/(target - slack) //Fmax in MHz.

                        //Retrieve Area.
                        val regArea = """\d+""".r
                        val area = Array.fill(4){ 0 }
                        for (i <- 0 until area.length) {
                            area(i) = regArea.findFirstIn(AreaStr.split("\n")(i)).toList(0).trim.toInt
                        }
                        val slices_luts = area(0)
                        val slices_regs = area(1)
                        val dsps = area(3)
                        val brams = area(2)
                        // Write the output to the csv file.
                        val writer = new FileWriter(outputFile, true)
                        writer.write(f"$opName%s, $Fmax%3.2f, $dsps%d, $slices_luts%d, $slices_regs%d, $brams%d, $time%d, $device%s, $clkMhz%3.2f\n")
                        writer.close()

                        //Remove the directory.
                        if(clean) {
                            Seq("rm", "-rf", name).!
                        }
                    }
                    catch {
                        case other: Exception =>
                        println("Exception caught: " + other)
                        if(clean) {
                            Seq("rm", "-rf", name).!
                        }
                    }
                }

            case "MgSoftCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMgSeries()
                val trainSize = 1000
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15)
                val threshes = Array.fill(delays.length){ 1.2e-2 }
                val gammas = Array[Double](8e-1, 1e-0, 1.25e0)
                val etas = Array[Double](1e-1, 5e-2, 1e-2)
                val epsilons = Array[Double](1e-4)
                val ns = Array[Int](128)
                val mu0s = Array[Double](0.5, 0.75, 0.9)
                val m = xsTrain(0).length
                val outputFile = "output/mg-dknlms-cv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgMse,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    for (gamma <- gammas) {
                        for (eta <- etas) {
                            for (epsilon <- epsilons) {
                                for (n <- ns) {
                                    for (mu0 <- mu0s) {
                                        var avgMse = 0.0
                                        var avgC = 0.0
                                        val step: Int = trainSize / nFold
                                        for (fold <- 0 until nFold) {
                                            // Create the current model.
                                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                                            // Create training and cv example.
                                            val startInd = fold*step
                                            val endInd = startInd + step
                                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                                            val xcv = xsTrain.slice(startInd, endInd)
                                            val ycv = ysTrain.slice(startInd, endInd)
                                            avgMse += regTrainAndTest(model, xt, yt, xcv, ycv)
                                            avgC += model.c
                                        }
                                        avgMse = avgMse / nFold
                                        avgC = avgC / nFold
                                        if (avgMse < thresh) {
                                            val writer = new FileWriter(outputFile, true)
                                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgMse%f,\n")
                                            writer.close()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            case "MgSoftRandCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMgSeries()
                val trainSize = 1000
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val n = 128

                //val dknlmsType = "dknlms"
                val dknlmsType = "dknlms-md"
                //val dknlmsType = "dknlms-md+dg"
                //val dknlmsType = "dknlms-ct"

                val cvType = "orig"
                //val cvType = "rand"

                val threshes = if (cvType == "rand") {
                    if (dknlmsType == "dknlms") {
                        Array.fill(delays.length-2){ 1.5e-2 } :+ 1.8e-2 :+ 2.0e-2
                    } else if (dknlmsType == "dknlms-md") {
                        Array(1.5e-2, 1.8e-2, 1.5e-2, 1.8e-2, 2.0e-2)
                    } else if (dknlmsType == "dknlms-md+dg") {
                        Array(5.0e-2, 5.0e-2, 5.0e-2, 5.0e-2, 5.0e-2)
                    } else { // dknlmsType == "dknlms-ct"
                        Array.fill(delays.length){ 1.5e-2 }
                    }
                } else { // if (cvType == "orig")
                    Array.fill(delays.length){ 3.4028235E38 }
                }

                val (rgamma, reta, repsilon, rmu0) = if (cvType == "rand") {
                    if (dknlmsType == "dknlms") {
                        (
                            Array[Double](6e-1, 5e-0),
                            Array[Double](1e-2, 5e-1),
                            Array[Double](2e-4, 1e-1),
                            Array[Double](0.5, 0.9)
                        )
                    } else if (dknlmsType == "dknlms-md") {
                        (
                            Array[Double](6e-1, 5e-0),
                            Array[Double](1e-2, 5e-1),
                            Array[Double](2e-4, 1e-1),
                            Array[Double](0.5, 0.9)
                        )
                    } else if (dknlmsType == "dknlms-md+dg") {
                        (
                            Array[Double](2.5e-1, 8e0),
                            Array[Double](3.125e-2, 1.0e0),
                            Array[Double](1.25e-1, 4.0e0),
                            Array[Double](1.25e-1, 9.99219e-1)
                        )
                    } else { // else if (dknlmsType == "dknlms-md-ct")
                        (
                            Array[Double](6e-1, 5e-0),
                            Array[Double](1e-2, 5e-1),
                            Array[Double](2e-4, 1e-1),
                            Array[Double](0.5, 0.9)
                        )
                    }
                } else { // if (cvType == "orig")
                    (
                        Array[Double](1.39, 1.39),
                        Array[Double](1e-1, 1e-1),
                        Array[Double](1e-4, 1e-4),
                        Array[Double](0.9, 0.9)
                    )
                }
                val trials = if (cvType == "rand") {
                    200
                } else { // if (cvType == "orig")
                    1
                }
                val m = xsTrain(0).length
                val outputFile = f"output/mg-$dknlmsType%s-$cvType%scv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                val convergenceTime = 500
                val testMode = "convergence"
                val seed = 1
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgMse,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    val rng = new Random(seed)
                    for (i <- 0 until trials) {
                        var avgMse = 0.0
                        var avgC = 0.0
                        val step: Int = trainSize / nFold

                        // Randomize parameters.
                        val gamma = (rgamma(1) - rgamma(0))*(rng.nextDouble() - 0.5) + (rgamma(1) + rgamma(0))/2.0
                        val eta = (reta(1) - reta(0))*(rng.nextDouble() - 0.5) + (reta(1) + reta(0))/2.0
                        val epsilon = (repsilon(1) - repsilon(0))*(rng.nextDouble() - 0.5) + (repsilon(1) + repsilon(0))/2.0
                        val mu0 = (rmu0(1) - rmu0(0))*(rng.nextDouble() - 0.5) + (rmu0(1) + rmu0(0))/2.0
                        for (fold <- 0 until nFold) {
                            // Create the current model.
                            val model = if(dknlmsType == "dknlms") {
                                    new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)
                                } else if (dknlmsType == "dknlms-md") {
                                    val dictDelay = delay / 2
                                    val alphaDelay = delay - dictDelay
                                    new MultiDelayKnlms(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay)
                                } else if (dknlmsType == "dknlms-md+dg") {
                                    val dictDelay = delay / 2
                                    val alphaDelay = delay - dictDelay
                                    new MultiDelayKnlms(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay, addMask=true)
                                } else { // (dknlmsType == "dknlms-ct")
                                    val dictDelay = delay / 2
                                    val alphaDelay = delay - dictDelay
                                    new MultiDelayKnlmsWithCorrections(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay)
                                }

                            // Create training and cv example.
                            val startInd = fold*step
                            val endInd = startInd + step
                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                            val xcv = xsTrain.slice(startInd, endInd)
                            val ycv = ysTrain.slice(startInd, endInd)
                            testMode match {
                                case "convergence" => // Determine parameters using avgMSE after a convergence period.
                                    val mseVec = regConvergence(model, xt, yt, xcv, ycv)
                                    avgMse += mseVec.slice(convergenceTime,mseVec.length).reduce(_+_) / (mseVec.length - convergenceTime).toDouble
                                case "mse" => avgMse += regTrainAndTest(model, xt, yt, xcv, ycv) // Determine parameters using mean squared error alone.
                            }
                            avgC += model.c
                        }
                        avgMse = avgMse / nFold
                        avgC = avgC / nFold
                        if (avgMse < thresh) {
                            val writer = new FileWriter(outputFile, true)
                            writer.write(f"$dknlmsType%s, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgMse%f,\n")
                            writer.close()
                        }
                    }
                }

            case "MgSoftTest" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMgSeries()
                val trainSize = 1000
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val ysTest = ysFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val skips = Array[Boolean](false, false, false, false, false) // Optionally skip and of the above delays to improve runtime, if not all results are needed.
                val n = 128

                val dknlmsType = "dknlms"
                //val dknlmsType = "dknlms-md"
                //val dknlmsType = "dknlms-md+dg"
                //val dknlmsType = "dknlms-ct"

                val runType = "orig"
                //val runType = "cv"
                val outputFile = f"output/mg-$dknlmsType%s-convergence-$runType%s.csv"

                // Original plot.
                val (gammas, etas, epsilons, mu0s) = if (runType == "orig") {
                    (
                        Array.fill(delays.length){ 1.39 },  // gammas
                        Array.fill(delays.length){ 1e-1 },  // etas
                        Array.fill(delays.length){ 1e-4 },  // epsilons
                        Array.fill(delays.length){ 0.9 }    // mu0s
                    )
                } else { // (runType == "cv")
                    if (dknlmsType == "dknlms") {
                        (
                            Array(1.479593, 1.479593, 1.479593, 0.884545, 2.135518), // gammas
                            Array(0.153729, 0.153729, 0.153729, 0.067604, 0.036016), // etas
                            Array(0.049815, 0.049815, 0.049815, 0.089980, 0.060698), // epsilons
                            Array(0.689559, 0.689559, 0.689559, 0.746731, 0.772611)  // mu0s
                        )
                    } else if (dknlmsType == "dknlms-md") {
                        (
                            Array(1.479593, 1.228955, 1.479593, 1.479593, 0.889293), // gammas
                            Array(0.153729, 0.131763, 0.153729, 0.153729, 0.063323), // etas
                            Array(0.049815, 0.062636, 0.049815, 0.049815, 0.089345), // epsilons
                            Array(0.689559, 0.873459, 0.689559, 0.689559, 0.845751)  // mu0s
                        )
                    } else if (dknlmsType == "dknlms-md+dg") {
                        (
                            Array(1.479593, 1.357819, 2.213625, 1.923921, 3.242041), // gammas
                            Array(0.153729, 0.271980, 0.378036, 0.490383, 0.721552), // etas
                            Array(0.049815, 2.549252, 3.058048, 3.716564, 3.887113), // epsilons
                            Array(0.689559, 0.941213, 0.703138, 0.424590, 0.278723)  // mu0s
                        )
                    } else { // (dknlmsType == "dknlms-ct")
                        (
                            Array.fill(delays.length){ 1.479593 }, // gammas
                            Array.fill(delays.length){ 0.153729 }, // etas
                            Array.fill(delays.length){ 0.049815 }, // epsilons
                            Array.fill(delays.length){ 0.689559 }  // mu0s
                        )
                    }
                }

                val m = xsTrain(0).length
                val res = new ArrayBuffer[Array[Double]]()
                for(i <- 0 until delays.length) {
                    val delay = delays(i)
                    val gamma = gammas(i)
                    val eta = etas(i)
                    val epsilon = epsilons(i)
                    val mu0 = mu0s(i)
                    val skip = skips(i)
                    val model = if(dknlmsType == "dknlms") {
                            new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)
                        } else if (dknlmsType == "dknlms-md") {
                            val dictDelay = delay / 2
                            val alphaDelay = delay - dictDelay
                            new MultiDelayKnlms(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay)
                        } else if (dknlmsType == "dknlms-md+dg") {
                            val dictDelay = delay / 2
                            val alphaDelay = delay - dictDelay
                            new MultiDelayKnlms(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay, addMask=true)
                        } else { // (dknlmsType == "dknlms-ct")
                            val dictDelay = delay / 2
                            val alphaDelay = delay - dictDelay
                            new MultiDelayKnlmsWithCorrections(n, m, gamma, eta, epsilon, mu0, dictDelay, alphaDelay)
                        }
                    if (!skip) {
                        res += regConvergence(model, xsTrain, ysTrain, xsTest, ysTest)
                    } else {
                        res += Array.fill(ysTrain.length){ 0.0 }
                    }
                }
                val writer = new FileWriter(outputFile)
                writer.write("index, " + delays.map(x => (x+1).toString).mkString("", ", ", ",\n"))
                for(i <- 0 until ysTrain.length) {
                    writer.write(f"$i%d, " + res.map(x => x(i).toString).mkString("", ", ", ",\n"))
                }
                writer.close()

            case "MgHwCv" =>
                val (xs: Array[Array[Double]], ys: Array[Double]) = getMgSeries()
                println(f"xs.length = ${xs.length}%d, ys.length = ${ys.length}%d")

            case "MgHwTest" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMgSeries()
                //val xsFull = xs.map(x => x :+ 0.0)
                val trainSize = 1000
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val ysTest = ysFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                //val delays = Array[Int](0)
                val wordlengths = Array[Int](14, 16, 18, 20, 22, 24, 32)
                //val wordlengths = Array[Int](18)
                // DKNLMS
                val gammas = Array(1.479593, 1.479593, 1.479593, 0.884545, 2.135518)
                val etas = Array(0.153729, 0.153729, 0.153729, 0.067604, 0.036016)
                val epsilons = Array(0.049815, 0.049815, 0.049815, 0.089980, 0.060698)
                val mu0s = Array(0.689559, 0.689559, 0.689559, 0.746731, 0.772611)
                val typeIndex = 0
                // MDKNLMS
                //val gammas = Array(1.479593, 1.228955, 1.479593, 1.479593, 2.135518)
                //val etas = Array(0.153729, 0.131763, 0.153729, 0.153729, 0.036016)
                //val epsilons = Array(0.049815, 0.062636, 0.049815, 0.049815, 0.060698)
                //val mu0s = Array(0.689559, 0.873459, 0.689559, 0.689559, 0.772611)
                //val typeIndex = 1
                // DKNLMS+DG
                //val gammas = Array(1.479593, 1.357819, 2.213625, 1.923921, 3.242041)
                //val etas = Array(0.153729, 0.271980, 0.378036, 0.490383, 0.721552)
                //val epsilons = Array(0.049815, 2.549252, 3.058048, 3.716564, 3.887113)
                //val mu0s = Array(0.689559, 0.941213, 0.703138, 0.424590, 0.278723)
                //val typeIndex = 2
                val knlmsTypes = Array[(Boolean, Boolean, String)]((true, false, "DKNLMS"), (false, false, "MDKNLMS"), (false, true, "MDKNLMS+DG"))
                val (singleDelay, addMask, dknlmsType) = knlmsTypes(typeIndex)

                val n: Int = 128
                val m: Int = xsTrain(0).length

                // Datatype parameters - Flo.
                //val prefix = "mg-dknlms-flo"
                /*for(i <- 0 until delays.length) {
                    val delay = delays(i)
                    val gamma = gammas(i)
                    val eta = etas(i)
                    val epsilon = epsilons(i)
                    val mu0 = mu0s(i)
                    val (divDelay, expDelay) = (0, 0)
                    val fromD: Double => Flo = Flo(_)
                    val bitoD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
                    val tToD: Flo => Double = _.floLitValue.toDouble
                    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, delay, divDelay, expDelay)
                    val pdiv: Int = 0
                    val pexp: Int = 0
                    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
                    val npExp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
                    val knlms = () => Module(new KNLMS[Flo](Flo(), fromD(-gamma), fromD(mu0), fromD(epsilon), fromD(eta), doReg, n, m, _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, pexp=pexp))
                    val pred = () => Module(new KafPredict[Flo](Flo(), Array.fill(KafPredict.getRegLength(n, m)){ false }, n, m, fromD, -gamma, _*_, _/_, _+_, _-_, npExp))
                    val hwsim = new KafHardwareSim[Flo](knlms, pred, bitoD, fromD, tToD, n, m, xsTrain, ysTrain, xsTest, ysTest, "convergence", f"$prefix%s-d=${delay+1}%d")
                    res += hwsim.res
                }*/

                // Datatype parameters - PsspFixed.
                for(w <- wordlengths) {
                    val iL = 5
                    val prefix = f"mg-dknlms-psspfixed($w%d.${w-iL}%d)-$dknlmsType%s"
                    val res = ArrayBuffer[Array[Double]]()
                    for(i <- 0 until delays.length) {
                        val delay = delays(i)
                        val gamma = gammas(i)
                        val eta = etas(i)
                        val epsilon = epsilons(i)
                        val mu0 = mu0s(i)
                        val (divDelay, expDelay) = (4, 3)
                        val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, delay, divDelay, expDelay)
                        val regCount = CountReg.nreg(doReg)
                        val pexp: Int = CountReg.nreg(expReg)
                        val pdiv: Int = CountReg.nreg(divReg)
                        val latency: Int = regCount + pexp + pdiv
                        val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
                        val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
                        val tToD: PsspFixed => Double = PsspFixed.toD(_)
                        val expMin: Double = -1023*math.pow(2,-8)
                        val exp: PsspFixed => PsspFixed = _.funLutPow2(expMin, 0, math.exp(_), 1024, expReg)
                        val npExp: PsspFixed => PsspFixed = _.funLutPow2(expMin, 0, math.exp(_), 1024, 0)
                        //val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(mu0, 1.0 + mu0*(n-1), 64, divReg, _)
                        val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutPow2(math.pow(2,-1), math.pow(2,-1) + 1023*math.pow(2,-7), 1024, divReg, _)
                        val knlms = () => Module(new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv), -gamma, mu0, epsilon, eta, doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, pexp=pexp, singleDelay=singleDelay, addMask=addMask))
                        val pred = () => Module(new KafPredict[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv), Array.fill(KafPredict.getRegLength(n, m)){ false }, n, m, fromD, -gamma, _*_, div, _+_, _-_, npExp))
                        val hwsim = new KafHardwareSim[PsspFixed](knlms, pred, bitoD, fromD, tToD, n, m, xsTrain, ysTrain, xsTest, ysTest, "convergence", f"$prefix%s-d=${delay+1}%d")
                        res += hwsim.res
                    }

                    val writer = new FileWriter(f"output/$prefix%s.csv")
                    writer.write("index, " + delays.map(x => (x+1).toString).mkString("", ", ", ",\n"))
                    for(i <- 0 until ysTrain.length) {
                        writer.write(f"$i%d, " + res.map(x => x(i).toString).mkString("", ", ", ",\n"))
                    }
                    writer.close()
                }

            case "HenSoftRandCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getHenSeries()
                val trainSize = 1000
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                //val threshes = Array.fill(delays.length-2){ 6e-3 } :+ 7e-3 :+ 9.9e-3
                val threshes = Array.fill(delays.length){ 1e1 }
                val n = 128
                //First try.
                //val rgamma = Array[Double](6e-1, 5e-0)
                //val reta = Array[Double](1e-2, 5e-1)
                //val repsilon = Array[Double](2e-4, 1e-1)
                //val rmu0 = Array[Double](0.5, 0.9)
                //Second try.
                val rgamma = Array[Double](3e-0, 1e1)
                val reta = Array[Double](3e-1, 1e-0)
                val repsilon = Array[Double](1e-5, 5e-1)
                val rmu0 = Array[Double](0.2, 0.9)
                //Third try.
                //val rgamma = Array[Double](3e-0, 4e1)
                //val reta = Array[Double](1e-1, 1e1)
                //val repsilon = Array[Double](1e-4, 1e-0)
                //val rmu0 = Array[Double](0.5, 0.9)
                val trials = 200
                val m = xsTrain(0).length
                val outputFile = "output/hen-dknlms-randcv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                val convergenceTime = 500
                val testMode = "convergence"
                val seed = 1
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgMse,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    val rng = new Random(seed)
                    for (i <- 0 until trials) {
                        var avgMse = 0.0
                        var avgC = 0.0
                        val step: Int = trainSize / nFold

                        // Randomize parameters.
                        val gamma = (rgamma(1) - rgamma(0))*(rng.nextDouble() - 0.5) + (rgamma(1) + rgamma(0))/2.0
                        val eta = (reta(1) - reta(0))*(rng.nextDouble() - 0.5) + (reta(1) + reta(0))/2.0
                        val epsilon = (repsilon(1) - repsilon(0))*(rng.nextDouble() - 0.5) + (repsilon(1) + repsilon(0))/2.0
                        val mu0 = (rmu0(1) - rmu0(0))*(rng.nextDouble() - 0.5) + (rmu0(1) + rmu0(0))/2.0
                        for (fold <- 0 until nFold) {
                            // Create the current model.
                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                            // Create training and cv example.
                            val startInd = fold*step
                            val endInd = startInd + step
                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                            val xcv = xsTrain.slice(startInd, endInd)
                            val ycv = ysTrain.slice(startInd, endInd)
                            testMode match {
                                case "convergence" => // Determine parameters using avgMSE after a convergence period.
                                    val mseVec = regConvergence(model, xt, yt, xcv, ycv)
                                    avgMse += mseVec.slice(convergenceTime,mseVec.length).reduce(_+_) / (mseVec.length - convergenceTime).toDouble
                                case "mse" => avgMse += regTrainAndTest(model, xt, yt, xcv, ycv) // Determine parameters using mean squared error alone.
                            }
                            avgC += model.c
                        }
                        avgMse = avgMse / nFold
                        avgC = avgC / nFold
                        if (avgMse < thresh) {
                            val writer = new FileWriter(outputFile, true)
                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgMse%f,\n")
                            writer.close()
                        }
                    }
                }

            case "HenSoftTest" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getHenSeries()
                val trainSize = 1000
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val ysTest = ysFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val n = 128

                // CV on KNLMS.
                val gammas = Array.fill(delays.length){ 7.823905 }
                val etas = Array.fill(delays.length){ 0.821048 }
                val epsilons = Array.fill(delays.length){ 0.366377 }
                val mu0s = Array.fill(delays.length){ 0.671445 }
                val outputFile = "output/hen-dknlms-convergence-orig.csv"

                // After CV.
                //val gammas = Array(7.823905, 7.086203, 9.044052, 7.086203, 8.635488)
                //val etas = Array(0.821048, 0.558827, 0.786857, 0.558827, 0.395083)
                //val epsilons = Array(0.366377, 0.472997, 0.434981, 0.472997, 0.422363)
                //val mu0s = Array(0.671445, 0.680501, 0.645107, 0.680501, 0.850649)
                //val outputFile = "output/hen-dknlms-convergence-cv.csv"

                val m = xsTrain(0).length
                val res = new ArrayBuffer[Array[Double]]()
                for(i <- 0 until delays.length) {
                    val delay = delays(i)
                    val gamma = gammas(i)
                    val eta = etas(i)
                    val epsilon = epsilons(i)
                    val mu0 = mu0s(i)
                    val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)
                    res += regConvergence(model, xsTrain, ysTrain, xsTest, ysTest)
                }
                val writer = new FileWriter(outputFile)
                writer.write("index, " + delays.map(x => (x+1).toString).mkString("", ", ", ",\n"))
                for(i <- 0 until ysTrain.length) {
                    writer.write(f"$i%d, " + res.map(x => x(i).toString).mkString("", ", ", ",\n"))
                }
                writer.close()

            case "LorSoftRandCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getLorSeries(true)
                val trainSize = 1000
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                //val threshes = Array.fill(delays.length-2){ 1.5e-2 } :+ 1.8e-2 :+ 2.0e-2
                val threshes = Array.fill(delays.length){ 1e2 }
                val n = 128
                //First try.
                //val rgamma = Array[Double](6e-1, 5e-0)
                //val reta = Array[Double](1e-2, 5e-1)
                //val repsilon = Array[Double](2e-4, 1e-1)
                //val rmu0 = Array[Double](0.5, 0.9)
                //Second try.
                //val rgamma = Array[Double](1e-1, 5e0)
                //val reta = Array[Double](5e-2, 5e-1)
                //val repsilon = Array[Double](1e-2, 1e0)
                //val rmu0 = Array[Double](0.2, 0.9)
                //Third try.
                //val rgamma = Array[Double](3e-1, 4e0)
                //val reta = Array[Double](5e-2, 5e-1)
                //val repsilon = Array[Double](1e-1, 2e0)
                //val rmu0 = Array[Double](5e-2, 9.5e-1)
                //Fourth try.
                //val rgamma = Array[Double](1e-1, 5e0)
                //val reta = Array[Double](1e-2, 1e0)
                //val repsilon = Array[Double](1e-2, 4e0)
                //val rmu0 = Array[Double](5e-2, 9.5e-1)
                //Fifth try.
                val rgamma = Array[Double](5e-2, 5e0)
                val reta = Array[Double](5e-3, 2e0)
                val repsilon = Array[Double](1e-2, 8e0)
                val rmu0 = Array[Double](5e-2, 9.5e-1)
                val trials = 200
                val m = xsTrain(0).length
                val outputFile = "output/lor-dknlms-randcv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                val convergenceTime = 500
                val testMode = "convergence"
                val seed = 1
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgMse,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    val rng = new Random(seed)
                    for (i <- 0 until trials) {
                        var avgMse = 0.0
                        var avgC = 0.0
                        val step: Int = trainSize / nFold

                        // Randomize parameters.
                        val gamma = (rgamma(1) - rgamma(0))*(rng.nextDouble() - 0.5) + (rgamma(1) + rgamma(0))/2.0
                        val eta = (reta(1) - reta(0))*(rng.nextDouble() - 0.5) + (reta(1) + reta(0))/2.0
                        val epsilon = (repsilon(1) - repsilon(0))*(rng.nextDouble() - 0.5) + (repsilon(1) + repsilon(0))/2.0
                        val mu0 = (rmu0(1) - rmu0(0))*(rng.nextDouble() - 0.5) + (rmu0(1) + rmu0(0))/2.0
                        for (fold <- 0 until nFold) {
                            // Create the current model.
                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                            // Create training and cv example.
                            val startInd = fold*step
                            val endInd = startInd + step
                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                            val xcv = xsTrain.slice(startInd, endInd)
                            val ycv = ysTrain.slice(startInd, endInd)
                            testMode match {
                                case "convergence" => // Determine parameters using avgMSE after a convergence period.
                                    val mseVec = regConvergence(model, xt, yt, xcv, ycv)
                                    avgMse += mseVec.slice(convergenceTime,mseVec.length).reduce(_+_) / (mseVec.length - convergenceTime).toDouble
                                case "mse" => avgMse += regTrainAndTest(model, xt, yt, xcv, ycv) // Determine parameters using mean squared error alone.
                            }
                            avgC += model.c
                        }
                        avgMse = avgMse / nFold
                        avgC = avgC / nFold
                        if (avgMse < thresh) {
                            val writer = new FileWriter(outputFile, true)
                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgMse%f,\n")
                            writer.close()
                        }
                    }
                }

            case "LorSoftTest" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getLorSeries(true)
                val trainSize = 1000
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val ysTest = ysFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val n = 128

                // CV on KNLMS.
                //val gammas = Array.fill(delays.length){ 1.341518 }
                //val etas = Array.fill(delays.length){ 0.211088 }
                //val epsilons = Array.fill(delays.length){ 0.759346 }
                //val mu0s = Array.fill(delays.length){ 0.662924 }
                //val outputFile = "output/lor-dknlms-convergence-orig.csv"

                // After CV.
                val gammas = Array(1.341518, 0.872390, 0.572946, 0.572946, 1.645572)
                val etas = Array(0.211088, 0.068980, 0.055184, 0.055184, 0.061012)
                val epsilons = Array(0.759346, 0.384837, 0.461897, 0.461897, 0.456498)
                val mu0s = Array(0.662924, 0.750626, 0.774575, 0.774575, 0.458394)
                val outputFile = "output/lor-dknlms-convergence-cv.csv"

                val m = xsTrain(0).length
                val res = new ArrayBuffer[Array[Double]]()
                for(i <- 0 until delays.length) {
                    val delay = delays(i)
                    val gamma = gammas(i)
                    val eta = etas(i)
                    val epsilon = epsilons(i)
                    val mu0 = mu0s(i)
                    val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)
                    res += regConvergence(model, xsTrain, ysTrain, xsTest, ysTest)
                }
                val writer = new FileWriter(outputFile)
                writer.write("index, " + delays.map(x => (x+1).toString).mkString("", ", ", ",\n"))
                for(i <- 0 until ysTrain.length) {
                    writer.write(f"$i%d, " + res.map(x => x(i).toString).mkString("", ", ", ",\n"))
                }
                writer.close()

            case "MadSoftCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMadSet()
                val trainSize = 3000
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val ysTest = ysFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val threshes = Array.fill(delays.length){ 8e-3 }
                val gammas = Array[Double](2e-1, 5e-1, 1.25e0)
                val etas = Array[Double](1e-1, 5e-2, 1e-2)
                val epsilons = Array[Double](1e-4)
                val ns = Array[Int](128)
                val mu0s = Array[Double](0.25, 0.5, 0.75)
                val m = xsTrain(0).length
                val outputFile = "output/mad-dknlms-cv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgErr,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    for (gamma <- gammas) {
                        for (eta <- etas) {
                            for (epsilon <- epsilons) {
                                for (n <- ns) {
                                    for (mu0 <- mu0s) {
                                        var avgErr = 0.0
                                        var avgC = 0.0
                                        val step: Int = trainSize / nFold
                                        for (fold <- 0 until nFold) {
                                            // Create the current model.
                                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                                            // Create training and cv example.
                                            val startInd = fold*step
                                            val endInd = startInd + step
                                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                                            val xcv = xsTrain.slice(startInd, endInd)
                                            val ycv = ysTrain.slice(startInd, endInd)
                                            avgErr += classifyTrainAndTest(model, xt, yt, xcv, ycv)
                                            avgC += model.c
                                        }
                                        avgErr = avgErr / nFold
                                        avgC = avgC / nFold
                                        if (avgErr < thresh) {
                                            val writer = new FileWriter(outputFile, true)
                                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgErr%f,\n")
                                            writer.close()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            case "MadSoftRandCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double]) = getMgSeries()
                val trainSize = 1000
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val threshes = Array.fill(delays.length){ 8e-2 }
                val rgamma = Array[Double](2e-1, 1.25e0)
                val reta = Array[Double](1e-1, 1e-2)
                val repsilon = Array[Double](1e-4, 1e-2)
                val rmu0 = Array[Double](0.25, 0.75)
                val n = 128
                val trials = 200
                val m = xsTrain(0).length
                val outputFile = "output/mad-dknlms-randcv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                val convergenceTime = 500
                val testMode = "convergence"
                val seed = 1
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgErr,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    val rng = new Random(seed)
                    for (i <- 0 until trials) {
                        var avgErr = 0.0
                        var avgC = 0.0
                        val step: Int = trainSize / nFold

                        // Randomize parameters.
                        val gamma = (rgamma(1) - rgamma(0))*(rng.nextDouble() - 0.5) + (rgamma(1) + rgamma(0))/2.0
                        val eta = (reta(1) - reta(0))*(rng.nextDouble() - 0.5) + (reta(1) + reta(0))/2.0
                        val epsilon = (repsilon(1) - repsilon(0))*(rng.nextDouble() - 0.5) + (repsilon(1) + repsilon(0))/2.0
                        val mu0 = (rmu0(1) - rmu0(0))*(rng.nextDouble() - 0.5) + (rmu0(1) + rmu0(0))/2.0

                        for (fold <- 0 until nFold) {
                            // Create the current model.
                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                            // Create training and cv example.
                            val startInd = fold*step
                            val endInd = startInd + step
                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                            val xcv = xsTrain.slice(startInd, endInd)
                            val ycv = ysTrain.slice(startInd, endInd)
                            avgErr += classifyTrainAndTest(model, xt, yt, xcv, ycv)
                            avgC += model.c
                        }
                        avgErr = avgErr / nFold
                        avgC = avgC / nFold
                        if (avgErr < thresh) {
                            val writer = new FileWriter(outputFile, true)
                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgErr%f,\n")
                            writer.close()
                        }
                    }
                }

            case "MadSoftTest" =>
                val (xs: Array[Array[Double]], ys: Array[Double]) = getMadSet()
                println(f"xs.length = ${xs.length}%d, ys.length = ${ys.length}%d")

            case "WeinSoftTest" =>
                val (xsFull: Array[Array[Double]], y1sFull: Array[Double], y2sFull: Array[Double]) = genWeinerSeriesExample()
                val split = 500
                val trainSize = 2*split
                val testSize = 100
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = y1sFull.slice(0,split) ++ y2sFull.slice(split, trainSize)
                val xsTest = xsFull.slice(trainSize,trainSize+testSize)
                val y1sTest = y1sFull.slice(trainSize,trainSize+testSize)
                val y2sTest = y2sFull.slice(trainSize,trainSize+testSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                val n = 128

                // CV on KNLMS.
                val gammas = Array.fill(delays.length){ 0.5 }
                val etas = Array.fill(delays.length){ 0.2 }
                val epsilons = Array.fill(delays.length){ 1e-4 }
                val mu0s = Array.fill(delays.length){ 0.8 }
                val outputFile = "output/wein-dknlms-convergence-orig.csv"

                // After CV.
                //val gammas = Array(0.313850, 0.313850, 0.345669, 0.345669, 0.491390)
                //val etas = Array(0.099927, 0.099927, 0.097283, 0.097283, 0.068367)
                //val epsilons = Array(0.015571, 0.015571, 0.070927, 0.070927, 0.043919)
                //val mu0s = Array(0.722476, 0.722476, 0.822848, 0.822848, 0.860209)
                //val outputFile = "output/wein-dknlms-convergence-cv.csv"

                val m = xsTrain(0).length
                val res = new ArrayBuffer[Array[Double]]()
                for(i <- 0 until delays.length) {
                    val delay = delays(i)
                    val gamma = gammas(i)
                    val eta = etas(i)
                    val epsilon = epsilons(i)
                    val mu0 = mu0s(i)
                    val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)
                    res += regConvergence(model, xsTrain.slice(0,split), ysTrain.slice(0,split), xsTest, y1sTest) ++
                           regConvergence(model, xsTrain.slice(split,trainSize), ysTrain.slice(split,trainSize), xsTest, y2sTest)
                    //res += regConvergence(model, xsTrain, ysTrain, xsTest, ysTest)
                }
                val writer = new FileWriter(outputFile)
                writer.write("index, " + delays.map(x => (x+1).toString).mkString("", ", ", ",\n"))
                for(i <- 0 until ysTrain.length) {
                    writer.write(f"$i%d, " + res.map(x => x(i).toString).mkString("", ", ", ",\n"))
                }
                writer.close()

            case "WeinSoftRandCv" =>
                val (xsFull: Array[Array[Double]], ysFull: Array[Double], z: Array[Double]) = genWeinerSeriesExample()
                val trainSize = 500
                val xsTrain = xsFull.slice(0,trainSize)
                val ysTrain = ysFull.slice(0,trainSize)
                val delays = Array[Int](0, 3, 7, 15, 31)
                //val threshes = Array.fill(delays.length-2){ 1.5e-2 } :+ 1.8e-2 :+ 2.0e-2
                val threshes = Array.fill(delays.length){ 1e2 }
                val n = 128
                //First try.
                //val rgamma = Array[Double](6e-1, 5e-0)
                //val reta = Array[Double](1e-2, 5e-1)
                //val repsilon = Array[Double](2e-4, 1e-1)
                //val rmu0 = Array[Double](0.5, 0.9)
                //Second try.
                //val rgamma = Array[Double](1e-1, 2e0)
                //val reta = Array[Double](5e-2, 1e0)
                //val repsilon = Array[Double](1e-3, 1e0)
                //val rmu0 = Array[Double](0.2, 0.95)
                //Third try.
                //val rgamma = Array[Double](5e-2, 2e0)
                //val reta = Array[Double](5e-2, 2e0)
                //val repsilon = Array[Double](1e-3, 2e0)
                //val rmu0 = Array[Double](0.2, 9.5e-1)
                //Fourth try.
                //val rgamma = Array[Double](5e-2, 4e0)
                //val reta = Array[Double](5e-2, 2e0)
                //val repsilon = Array[Double](5e-1, 4e0)
                //val rmu0 = Array[Double](0.05, 9.5e-1)
                //Fifth try.
                //val rgamma = Array[Double](1e-1, 3e0)
                //val reta = Array[Double](1e-1, 4e0)
                //val repsilon = Array[Double](1e-1, 8e0)
                //val rmu0 = Array[Double](0.1, 9.9e-1)
                //Sixth try.
                //val rgamma = Array[Double](1e-1, 3e0)
                //val reta = Array[Double](1e-1, 8e0)
                //val repsilon = Array[Double](1e-1, 16e0)
                //val rmu0 = Array[Double](0.1, 9.5e-1)
                //Seventh try.
                //val rgamma = Array[Double](1e-1, 3e0)
                //val reta = Array[Double](1e-2, 1e-1)
                //val repsilon = Array[Double](1e-2, 1e-1)
                //val rmu0 = Array[Double](0.1, 9.5e-1)
                //Eighth try.
                //val rgamma = Array[Double](1e-1, 6e-1)
                //val reta = Array[Double](1e-2, 1e-1)
                //val repsilon = Array[Double](1e-2, 1e-1)
                //val rmu0 = Array[Double](4e-1, 9.5e-1)
                //Eighth try.
                val rgamma = Array[Double](1e-1, 6e-1)
                val reta = Array[Double](1e-2, 3e-1)
                val repsilon = Array[Double](1e-2, 1e-1)
                val rmu0 = Array[Double](4e-1, 9.5e-1)
                val trials = 200
                val m = xsTrain(0).length
                val outputFile = "output/wein-dknlms-randcv.csv"
                val writer = new FileWriter(outputFile)
                val nFold = 10
                val convergenceTime = 300
                val testMode = "convergence"
                val seed = 1
                writer.write(f"type, delay, gamma, eta, epsilon, mu0, n, m, avgC, avgMse,\n")
                writer.close()

                // Run loop for DKNLMS.
                for ((delay, thresh) <- (delays, threshes).zipped) {
                    val rng = new Random(seed)
                    for (i <- 0 until trials) {
                        var avgMse = 0.0
                        var avgC = 0.0
                        val step: Int = trainSize / nFold

                        // Randomize parameters.
                        val gamma = (rgamma(1) - rgamma(0))*(rng.nextDouble() - 0.5) + (rgamma(1) + rgamma(0))/2.0
                        val eta = (reta(1) - reta(0))*(rng.nextDouble() - 0.5) + (reta(1) + reta(0))/2.0
                        val epsilon = (repsilon(1) - repsilon(0))*(rng.nextDouble() - 0.5) + (repsilon(1) + repsilon(0))/2.0
                        val mu0 = (rmu0(1) - rmu0(0))*(rng.nextDouble() - 0.5) + (rmu0(1) + rmu0(0))/2.0
                        for (fold <- 0 until nFold) {
                            // Create the current model.
                            val model = new DKNLMSSoft(n, m, gamma, eta, epsilon, mu0, delay)

                            // Create training and cv example.
                            val startInd = fold*step
                            val endInd = startInd + step
                            val xt = xsTrain.slice(0, startInd) ++ xsTrain.slice(endInd, xsTrain.length)
                            val yt = ysTrain.slice(0, startInd) ++ ysTrain.slice(endInd, ysTrain.length)
                            val xcv = xsTrain.slice(startInd, endInd)
                            val ycv = ysTrain.slice(startInd, endInd)
                            testMode match {
                                case "convergence" => // Determine parameters using avgMSE after a convergence period.
                                    val mseVec = regConvergence(model, xt, yt, xcv, ycv)
                                    avgMse += mseVec.slice(convergenceTime,mseVec.length).reduce(_+_) / (mseVec.length - convergenceTime).toDouble
                                case "mse" => avgMse += regTrainAndTest(model, xt, yt, xcv, ycv) // Determine parameters using mean squared error alone.
                            }
                            avgC += model.c
                        }
                        avgMse = avgMse / nFold
                        avgC = avgC / nFold
                        if (avgMse < thresh) {
                            val writer = new FileWriter(outputFile, true)
                            writer.write(f"DKNLMS, ${delay+1}%d, $gamma%f, $eta%f, $epsilon%f, $mu0%f, $n%d, $m%d, $avgC%f, $avgMse%f,\n")
                            writer.close()
                        }
                    }
                }

            case "MadHwCv" =>
                val (xs: Array[Array[Double]], ys: Array[Double]) = getMadSet()
                println(f"xs.length = ${xs.length}%d, ys.length = ${ys.length}%d")

            case "MadHwTest" =>
                val (xs: Array[Array[Double]], ys: Array[Double]) = getMadSet()
                println(f"xs.length = ${xs.length}%d, ys.length = ${ys.length}%d")

            case other =>
                error(f"Error: $other%s does not match any module.")
        }
    }

    /* A bunch of helper methods for performing cross validation / testing */
    def getMgSeries(): (Array[Array[Double]], Array[Double]) = {
        val in = getClass.getResourceAsStream("/mg3.1k.dat")
        val reader = new BufferedReader(new InputStreamReader(in))
        val xs = ArrayBuffer[Array[Double]]()
        val ys = ArrayBuffer[Double]()
        var line = Option[String](reader.readLine)
        while (line != None) {
            val vec = line.get.split(",").map(_.toDouble)
            xs += vec.slice(0, vec.length-1)
            ys += vec(vec.length-1)
            line = Option[String](reader.readLine)
        }
        (xs.toArray[Array[Double]], ys.toArray[Double])
    }

    def getMadSet(): (Array[Array[Double]], Array[Double]) = {
        val inx = getClass.getResourceAsStream("/mad3.1kx.dat")
        val readx = new BufferedReader(new InputStreamReader(inx))
        val iny = getClass.getResourceAsStream("/mad3.1ky.dat")
        val ready = new BufferedReader(new InputStreamReader(iny))
        val xs = ArrayBuffer[Array[Double]]()
        val ys = ArrayBuffer[Double]()
        var lx = Option[String](readx.readLine)
        var ly = Option[String](ready.readLine)
        while (lx != None && ly != None) {
            val x = lx.get.split(",").map(_.toDouble)
            val y = ly.get.toDouble
            xs += x
            ys += y
            lx = Option[String](readx.readLine)
            ly = Option[String](ready.readLine)
        }
        (xs.toArray[Array[Double]], ys.map(y => if (y == 1.0) 1.0 else -1.0).toArray[Double])
    }

    def getHenSeries(normalise: Boolean = false): (Array[Array[Double]], Array[Double]) = {
        val in = getClass.getResourceAsStream("/henon1.1k.dat")
        val reader = new BufferedReader(new InputStreamReader(in))
        val xs = ArrayBuffer[Array[Double]]()
        val ys = ArrayBuffer[Double]()
        var line = Option[String](reader.readLine)
        while (line != None) {
            val vec = line.get.split(",").map(_.toDouble)
            xs += vec.slice(0, vec.length-1)
            ys += vec(vec.length-1)
            line = Option[String](reader.readLine)
        }
        if (normalise) throw new Exception("Normalisation not implemented yet.")
        (xs.toArray[Array[Double]], ys.toArray[Double])
    }

    def getLorSeries(normalise: Boolean = false): (Array[Array[Double]], Array[Double]) = {
        val in = getClass.getResourceAsStream("/lorenz1.5k.dat")
        val reader = new BufferedReader(new InputStreamReader(in))
        val xs = ArrayBuffer[Double]()
        //val xs = ArrayBuffer[Array[Double]]()
        //val ys = ArrayBuffer[Double]()
        var line = Option[String](reader.readLine)
        while (line != None) {
            val vec = line.get.split(",").map(_.toDouble)
            xs += vec(vec.length-1)
            //xs += vec.slice(0, vec.length-1)
            //ys += vec(vec.length-1)
            line = Option[String](reader.readLine)
        }
        val xn = if (normalise) {
            normaliseArray(xs.toArray[Double])
        } else {
            xs.toArray[Double]
        }
        //timeSeriesToIoPairs(xn, 5, 10)
        timeSeriesToIoPairs(xn, 5, 0)
    }

    def genWeinerSystem(numFeatures: Int, length: Int, seed: Int = 0, noise: Double = 0.0, f: Double => Double = Math.atan) = {
        val rng = new Random(seed)
        val w = Array.fill(numFeatures){ rng.nextGaussian() }
        val xs = ArrayBuffer[Array[Double]]()
        val ys = ArrayBuffer[Double]()
        for (i <- 0 until length) {
            val x = Array.fill(numFeatures){ rng.nextGaussian() }
            val y = f((w, x).zipped.map(_*_).reduce(_+_)) + noise*rng.nextGaussian()
            xs += x
            ys += y
        }
        (xs.toArray[Array[Double]], ys.toArray[Double])
    }

    /*
     *  An example of a weiner series taken from: "Fixed-budget Kernel Least Mean Squares" by Dominik Rxepka.
     */
    def genWeinerSeriesExample(seed: Int = 0) = {
        val rng = new Random(seed)
        val h1 = Array(1.0, 0.8668, -0.4764, 0.2070).reverse
        val h2 = Array(1.0, -0.8326, 0.6656, 0.7153).reverse
        val in = Array(-1.0, -0.3333, 0.3333, 1)
        val trainSize = 500
        val testSize = 100
        val dBSnr = 30
        val length = 2*(trainSize + testSize)
        val xs = Array.fill(length + 100){ in(rng.nextInt(in.length)) }
        val x = ArrayBuffer[Array[Double]]()
        val y1 = ArrayBuffer[Double]()
        val y2 = ArrayBuffer[Double]()
        var xsIter = 0
        while (xsIter + h1.length <= length) {
            val xn = xs.slice(xsIter, xsIter + h1.length)
            x += xn
            y1 += Math.atan((xn, h1).zipped.map(_*_).reduce(_+_))
            y2 += Math.atan((xn, h2).zipped.map(_*_).reduce(_+_))
            xsIter += 1
        }
        (x.toArray[Array[Double]], addNoise(y1.toArray[Double], 30, seed = 1), addNoise(y2.toArray[Double], 30, seed = 2))
    }

    def regTrainAndTest(model: PipelinedKernelAdaptiveFilter, xTrain: Array[Array[Double]], yTrain: Array[Double], xTest: Array[Array[Double]], yTest: Array[Double]): Double = {
        // Training part.
        for (i <- 0 until yTrain.length) {
            model.train(xTrain(i), yTrain(i))
        }

        // Testing part.
        val err = (xTest, yTest).zipped.map((x, y) => y - model.predict(x))
        val mse = err.map(x => x*x).reduce(_+_) / err.length
        mse
    }

    def regConvergence(model: PipelinedKernelAdaptiveFilter, xTrain: Array[Array[Double]], yTrain: Array[Double], xTest: Array[Array[Double]], yTest: Array[Double]): Array[Double] = {
        val mse = Array.fill(yTrain.length){ 0.0 }

        // Perform a training update step, and predict the test set in each iteration.
        for (i <- 0 until yTrain.length) {
            // Update model.
            model.train(xTrain(i), yTrain(i))

            // Predict test set.
            val err = (xTest, yTest).zipped.map((x, y) => y - model.predict(x))
            mse(i) = err.map(x => x*x).reduce(_+_) / err.length
        }
        mse
    }

    def classifyTrainAndTest(model: PipelinedKernelAdaptiveFilter, xTrain: Array[Array[Double]], yTrain: Array[Double], xTest: Array[Array[Double]], yTest: Array[Double]): Double = {
        // Training part.
        for (i <- 0 until yTrain.length) {
            model.train(xTrain(i), yTrain(i))
        }

        // Testing part.
        val classify: Double => Double = (x: Double) => { if (x > 0.0) 1.0 else -1.0 }
        val err = (xTest, yTest).zipped.map((x, y) => Math.abs(y - classify(model.predict(x))) / 2.0).reduce(_+_)
        err / yTrain.length
    }

    def timeSeriesToIoPairs(x: Array[Double], inputLength: Int, predHorizon: Int): (Array[Array[Double]], Array[Double]) = {
        val xs = ArrayBuffer[Array[Double]]()
        val ys = ArrayBuffer[Double]()
        var xIt = 0
        var yIt = xIt + inputLength + predHorizon
        while(yIt < x.length) {
            xs += x.slice(xIt, xIt + inputLength)
            ys += x(yIt)
            xIt += 1
            yIt += 1
        }
        (xs.toArray[Array[Double]], ys.toArray[Double])
    }

    def normaliseArray(x: Array[Double]): Array[Double] = {
        val mean = x.reduce(_+_)/x.length
        val std = Math.sqrt(x.map(v => Math.pow(v-mean,2)).reduce(_+_)/x.length)
        x.map(v => (v - mean)/std)
    }

    def addNoise(x: Array[Double], dBSnr: Double, seed: Int = 0) = {
        val rng = new Random(seed)
        val dBSigPow = 10*Math.log10(x.map(z => z*z).reduce(_+_)/x.length)
        val dBNoisePow = dBSigPow - dBSnr
        val noise = Math.sqrt(Math.pow(10, dBNoisePow))
        val noiseVec = Array.fill(x.length){ noise*rng.nextGaussian() }
        (x, noiseVec).zipped.map(_+_)
    }
}

