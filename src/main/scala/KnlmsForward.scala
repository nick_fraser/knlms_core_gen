
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer

// n is any positive int.
// m is any positive int.
// doReg is of size (log2Up(m) + 4) + (2) + (log2Up(n) + 1) + 1 + (3) + a*pmul + b*pdiv + c*padd + d*psub + e*pexp + f*pgt.

class KNLMSForward[T <: Data](dtype: T, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double, val doReg: Array[Boolean], val n: Int, val m: Int,
                                mul: (T, T) => T, div: (T, T) => T, add: (T, T) => T, sub: (T, T) => T, exp: T => T, gt: (T, T) => Bool, DtoT: Double => T,
                                pmul: Int = 0, pdiv: Int = 0, padd: Int = 0, psub: Int = 0, pexp: Int = 0, pgt: Int = 0,
                                val singleDelay: Boolean = true, val addMask: Boolean = false, val correctionTerms: Int = 0) extends Module {
    val io = new Bundle {
        val x = Vec.fill(m){ dtype.clone.asInput }
        val y = dtype.clone.asInput
        val ybar = dtype.clone.asOutput
        val valid_in = Bool(INPUT)
        val Din = Vec.fill(m*n){ dtype.clone.asInput }
        val Dout = Vec.fill(m*n){ dtype.clone.asOutput }
        val Ain = Vec.fill(n){ dtype.clone.asInput }
        val Aout = Vec.fill(n){ dtype.clone.asOutput }
        val cin = UInt(INPUT, log2Up(n) + 1)
        val cout = UInt(OUTPUT, log2Up(n) + 1)
        val valid_out = Bool(OUTPUT)
    }

    /******* KERNEL EVALUATION *******/

    //Create kernel modules.
    val KMs = Array.fill(n){ Module(new ExpKernel[T](dtype, neg_gamma, DtoT, doReg.slice(0,log2Up(m)+4), m, mul, add, sub, exp, pmul, padd, psub, pexp)) }
    val k = Vec.fill(n){ dtype.clone }

    //Connect up inputs to kernel module to Dictionary and outputs to k vector.
    for (i <- 0 until n) {
        for (j <- 0 until m) {
            KMs(i).io.x(j) := io.x(j)
            val index = i*m + j
            KMs(i).io.y(j) := io.Din(index)
        }
        k(i) := KMs(i).io.res
    }

    /******* KERNEL VECTOR MASK *******/

    //Shift all csr by doReg.slice(0, log2Up(n)+4).
    val csr = Module(new VarShiftRegister[UInt](UInt(), Array.fill(KMs(0).latency){ true }, 1))
    csr.io.vin(0) := io.cin

    //Apply a mask to k vector.
    val kmask = Module(new VectorMask[T](dtype, DtoT(0.0), n))
    kmask.io.cin := csr.io.vout(0)
    for (i <- 0 until n) {
        kmask.io.x(i) := k(i)
    }
    //Debug k output.

    /******* ADD TO DICTIONARY *******/

    //Delay valid signal.
    val vsr_atd = Module(new VarShiftRegister[Bool](Bool(), Array.fill(KMs(0).latency){ true }, 1, Bool(false)))
    vsr_atd.io.vin(0) := io.valid_in

    //Delay inputs excluding kmask.
    val xsr = Module(new VarShiftRegister[T](dtype, Array.fill(KMs(0).latency){ true }, m))
    for (i <- 0 until m) {
        xsr.io.vin(i) := io.x(i)
    }

    //Add to dictionary? 
    try {
        if (singleDelay && addMask) throw new Exception("singleDelay and addMask cannot both be set.")
    }
    val dictDelay = if(addMask) KMs(0).latency else 0
    val addto = Module(new AddToDict(dtype, doReg.slice(log2Up(m)+4, log2Up(m)+6), n, m, gt, DtoT, pgt, dictDelay))
    addto.io.knn := DtoT(1.0)
    addto.io.thresh := DtoT(thresh)
    addto.io.valid_in := vsr_atd.io.vout(0)
    for (i <- 0 until n) {
        addto.io.k(i) := kmask.io.y(i)
    }
    for (i <- 0 until m) {
        addto.io.x(i) := xsr.io.vout(i)
    }

    /******* ESTIMATE Y AND CALCULATE NORMALISATION FACTOR *******/

    //Delay alpha.
    val asrTmp = Vec.fill(n){ dtype.clone }
    if (singleDelay) {
        val asr = Module(new VarShiftRegister[T](dtype, Array.fill(KMs(0).latency + addto.latency){ true }, n))
        for (i <- 0 until n) {
            asr.io.vin(i) := io.Ain(i)
            asrTmp(i) := asr.io.vout(i)
        }
    } else {
        for (i <- 0 until n) {
            asrTmp(i) := io.Ain(i)
        }
    }

    val y_est = Module(new DotProdTree[T](dtype, doReg.slice(log2Up(m)+6, log2Up(m)+log2Up(n)+7), n, mul, add, pmul, padd))
    for (i <- 0 until n) {
        y_est.io.x(i) := addto.io.kbar(i)
        y_est.io.y(i) := asrTmp(i)
    }

    val kdotk = Module(new DotProdTree[T](dtype, doReg.slice(log2Up(m)+6, log2Up(m)+log2Up(n)+7), n, mul, add, pmul, padd))
    for (i <- 0 until n) {
        kdotk.io.x(i) := addto.io.kbar(i)
        kdotk.io.y(i) := addto.io.kbar(i)
    }

    /******* UPDATE ALPHA *******/

    //Delay y.
    val ysr = Module(new VarShiftRegister[T](dtype, Array.fill(KMs(0).latency + addto.latency + y_est.latency){ true }, 1))
    ysr.io.vin(0) := io.y

    val errRegLoc: Int = log2Up(m)+log2Up(n)+7

    //Extra register for the subtraction operation.
    val err = Module(new Rege(dtype, doReg(errRegLoc)))
    err.io.din := sub(ysr.io.vout(0), y_est.io.res)
    //Add extra subtraction for kdotk also.
    val kdotksr = Module(new Rege(dtype, doReg(errRegLoc)))
    kdotksr.io.din := kdotk.io.res

    //Delay kbar.
    val kbar = Module(new VarShiftRegister[T](dtype, Array.fill(y_est.latency + psub){ true } :+ doReg(errRegLoc), n))
    for (i <- 0 until n) {
        kbar.io.vin(i) := addto.io.kbar(i)
        //io.k(i) := kbar.io.vout(i) //Add if we want to debug the k vector.
    }

    //Delay vsr_atd
    val vsr_ua = Module(new VarShiftRegister[Bool](Bool(), Array.fill(y_est.latency + psub){ true } :+ doReg(errRegLoc), 1, Bool(false)))
    vsr_ua.io.vin(0) := addto.io.valid_out

    val AlphaUpdate = Module(new NormalisedUpdateAlpha[T](dtype, doReg.slice(log2Up(m)+log2Up(n)+8, log2Up(m)+log2Up(n)+11), n, mul, add, div, DtoT, pmul, padd, pdiv))
    AlphaUpdate.io.epsilon := DtoT(epsilon)
    AlphaUpdate.io.eta := DtoT(eta)
    AlphaUpdate.io.err := err.io.dout
    AlphaUpdate.io.kdotk := kdotksr.io.dout
    for (i <- 0 until n) {
        AlphaUpdate.io.k(i) := kbar.io.vout(i)
    }
    AlphaUpdate.io.valid_in := vsr_ua.io.vout(0)

    /******* CONNECT OUTPUTS *******/

    //Delay Cout and connect to output.
    if (singleDelay) {
        val cout = Module(new VarShiftRegister[UInt](UInt(), Array.fill(y_est.latency + AlphaUpdate.latency + psub){ true } :+ doReg(errRegLoc), 1))
        cout.io.vin(0) := addto.io.c
        io.cout := cout.io.vout(0)
    } else {
        io.cout := addto.io.c
    }

    //Connect alpha to outputs.
    for (i <- 0 until n) {
        io.Aout(i) := AlphaUpdate.io.alpha(i)
    }

    //Delay ybar and connect to output.
    val ybarsr = Module(new VarShiftRegister[T](dtype, Array.fill(AlphaUpdate.latency + psub){ true } :+ doReg(errRegLoc), 1))
    ybarsr.io.vin(0) := y_est.io.res
    io.ybar := ybarsr.io.vout(0)

    //Delay Dout and connect to output.
    if (singleDelay) {
        val dout = Module(new VarShiftRegister[T](dtype, Array.fill(y_est.latency + AlphaUpdate.latency + psub){ true } :+ doReg(errRegLoc), n*m))
        for (i <- 0 until n*m) {
            dout.io.vin(i) := addto.io.D(i)
            io.Dout(i) := dout.io.vout(i)
        }
    } else {
        for (i <- 0 until n*m)
            io.Dout(i) := addto.io.D(i)
    }

    //Connect valid out from AlphaUpdate
    io.valid_out := AlphaUpdate.io.valid_out

    val latency = KMs(0).latency + addto.latency + y_est.latency + AlphaUpdate.latency + psub + CountReg.nreg(doReg.slice(errRegLoc, errRegLoc+1))
    val (d1, d2) = if (singleDelay) { (latency, latency) } else { (KMs(0).latency + addto.latency, y_est.latency + AlphaUpdate.latency + psub + CountReg.nreg(doReg.slice(errRegLoc, errRegLoc+1))) }
}

