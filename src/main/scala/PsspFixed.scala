
package PKAF

import Chisel._
import scala.collection.mutable.ArrayBuffer
import Remez.Remez

// A class for implementing a pipelined, signed, single precision fixed point datatype in chisel.
class PsspFixed extends Bits with Num[PsspFixed] {
    var iL: Int = -1
    var bw: Int = -1
    var padd: Int = 0
    var psub: Int = 0
    var pmul: Int = 0
    var pdiv: Int = 0
    var pgt: Int = 0
    type T = PsspFixed; //Not sure if this is needed.
    def matched (b: PsspFixed): Boolean = (this.iL == b.iL) && (this.bw == b.bw) && (this.padd == b.padd) && (this.psub == b.psub) && (this.pmul == b.pmul) && (this.pdiv == b.pdiv) && (this.pgt == b.pgt)
    //def typeAssert (b: PsspFixed): Unit = if (!this.matched(b)) throw new Exception(
    //    "Combining unmatched types in PsspFixed.\n" +
    //    f"This: bw=${this.bw}%d, iL=${this.iL}%d, padd=${this.padd}%d, psub=${this.psub}%d, pmul=${this.pmul}%d, pdiv=${this.pdiv}%d, pgt=${this.pgt}%d.\n" +
    //    f"B: bw=${b.bw}%d, iL=${b.iL}%d, padd=${this.padd}%d, psub=${this.psub}%d, pmul=${this.pmul}%d, pdiv=${this.pdiv}%d, pgt=${this.pgt}%d.")
    def typeAssert (b: PsspFixed): Unit = if (!this.matched(b)) println(
        "WARNING: Combining unmatched types in PsspFixed.\n" +
        f"This: bw=${this.bw}%d, iL=${this.iL}%d, padd=${this.padd}%d, psub=${this.psub}%d, pmul=${this.pmul}%d, pdiv=${this.pdiv}%d, pgt=${this.pgt}%d.\n" +
        f"B: bw=${b.bw}%d, iL=${b.iL}%d, padd=${this.padd}%d, psub=${this.psub}%d, pmul=${this.pmul}%d, pdiv=${this.pdiv}%d, pgt=${this.pgt}%d.")
    def unary_-(): PsspFixed = {
        val res = castToPsspFixed(chiselCast(this){ SInt() }.unary_-(), bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, psub)
        pres
    }
    def + (b: PsspFixed): PsspFixed = {
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } + chiselCast(b){ SInt() }
        val res = castToPsspFixed(s.toSInt, bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, padd)
        pres
    }
    def * (b: PsspFixed): PsspFixed = { 
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } * chiselCast(b){ SInt() }
        val msb: Int = 2*bw-iL-1
        val res = castToPsspFixed(s(msb,msb-bw+1).toSInt, bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, pmul)
        pres
    }
    def / (b: PsspFixed): PsspFixed = { 
        this.typeAssert(b)
        //val s = Cat(chiselCast(this){ SInt() } / chiselCast(b){ SInt() }, chiselCast(this){ SInt() } % chiselCast(b){ SInt() })
        //val msb: Int = 2*bw-iL-1
        //castToPsspFixed(s(msb, msb-bw+1).toSInt,iL)
        val s = Cat(chiselCast(this){ SInt() }, SInt(0, bw-iL)).toSInt / chiselCast(b){ SInt() }
        val msb: Int = bw-1
        val res = castToPsspFixed(s(msb, msb-bw+1).toSInt, bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, pdiv)
        pres
    }
    def % (b: PsspFixed): PsspFixed = { 
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } % chiselCast(b){ SInt() }
        val msb: Int = bw-1
        val res = castToPsspFixed(s(msb,msb-bw+1).toSInt, bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, pdiv)
        pres
    }
    def - (b: PsspFixed): PsspFixed = {
        this.typeAssert(b)
        val s = chiselCast(this){ SInt() } - chiselCast(b){ SInt() }
        val res = castToPsspFixed(s.toSInt, bw, iL, padd, psub, pmul, pdiv, pgt)
        val pres = pout[PsspFixed](res.clone, res, psub)
        pres
    }
    def < (b: PsspFixed): Bool = {
        this.typeAssert(b)
        val res = chiselCast(this){ SInt() } < chiselCast(b){ SInt() }
        val pres = pout[Bool](res.clone, res, pgt)
        pres
    }
    def <= (b: PsspFixed): Bool = {
        this.typeAssert(b)
        val res = chiselCast(this){ SInt() } <= chiselCast(b){ SInt() }
        val pres = pout[Bool](res.clone, res, pgt)
        pres
    }
    def > (b: PsspFixed): Bool = {
        this.typeAssert(b)
        val res = chiselCast(this){ SInt() } > chiselCast(b){ SInt() }
        val pres = pout[Bool](res.clone, res, pgt)
        pres
    }
    def >= (b: PsspFixed): Bool = {
        this.typeAssert(b)
        val res = chiselCast(this){ SInt() } >= chiselCast(b){ SInt() }
        val pres = pout[Bool](res.clone, res, pgt)
        pres
    }

    def truncate(): SInt = this(bw-1, bw-iL).toSInt

    def convertTo(resType: PsspFixed): PsspFixed = {
        val iB = Bits()
        val fB = Bits()
        val iLDiff: Int = this.iL - resType.iL
        val fLDiff: Int = (this.bw-this.iL) - (resType.bw-resType.iL)
        if(iLDiff < 0) {
            iB := Cat(Fill(-iLDiff, this(this.bw-1, this.bw-1)), this(this.bw-1, bw-this.iL))
        }
        else {
            iB := this(this.bw-iLDiff-1, this.bw-this.iL)
        }
        if(fLDiff < 0) {
            fB := Cat(this(this.bw-this.iL-1, 0), Fill(-fLDiff, this(this.bw-1, this.bw-1)))
        }
        else {
            fB := this(this.bw-this.iL-1, fLDiff)
        }
        val res = chiselCast(Cat(iB, fB)){ resType.clone }
        res
    }

    def convertTo(newBw: Int, newIl: Int, newPAdd: Int, newPSub: Int, newPMul: Int, newPDiv: Int, newPGt: Int): PsspFixed = {
        val res = PsspFixed(width=newBw, iL=newIl, padd=newPAdd, psub=newPSub, pmul=newPMul, pdiv=newPDiv, pgt=newPGt)
        convertTo(res)
    }

    override def fromNode(n: Node): this.type = {
        PsspFixed(OUTPUT).asTypeFor(n).asInstanceOf[this.type]
    }

    override def fromInt(x: Int): this.type = {
        PsspFixed(x).asInstanceOf[this.type]
    }

    override def clone: this.type = {
        val res = super.clone
        res.iL = iL
        res.bw = bw
        res.padd = padd
        res.psub = psub
        res.pmul = pmul
        res.pdiv = pdiv
        res.pgt = pgt
        res
    }

    def castToPsspFixed(s: SInt, width: Int, iL: Int, padd: Int, psub: Int, pmul: Int, pdiv: Int, pgt: Int): PsspFixed = {
        val k = chiselCast(s){ PsspFixed() }
        k.iL = iL
        k.bw = width
        k.padd = padd
        k.psub = psub
        k.pmul = pmul
        k.pdiv = pdiv
        k.pgt = pgt
        k
    }

    def frac(): PsspFixed = {
        val s = Cat(SInt(0, iL), this(bw-iL-1, 0)).toSInt
        val k = castToPsspFixed(s, bw, iL, padd, psub, pmul, pdiv, pgt)
        k
    }

    def abs(): PsspFixed = {
        val res = PsspFixed(width=this.bw, iL=this.iL, padd=this.padd, psub=this.psub, pmul=this.pmul, pdiv=this.pdiv, pgt=this.pgt)
        when(this(this.bw-1)) {
            res := -this
        } .otherwise {
            res := this
        }
        res
    }

    //An algorithms to do manual division.
    //Length doReg = this.bw-1
    def manualDivision(doReg: Array[Boolean], b: PsspFixed): PsspFixed = {
        this.typeAssert(b)

        //Convert inputs to positive values if required.
        val as = chiselCast(this.abs()) { SInt() }
        val bs = chiselCast(b.abs()) { SInt() }
        //val flip = (chiselCast(this){ SInt() } < SInt(0)) ^ (chiselCast(b){ SInt() } < SInt(0))
        val flip = this(this.bw-1) ^ b(b.bw-1)

        //Calculate division as a sequence of subtractions.
        val numReg = new ArrayBuffer[SInt]()
        val denReg = new ArrayBuffer[SInt]()
        val quoReg = new ArrayBuffer[SInt]()
        numReg += Cat(as, SInt(0, bw-iL-2)).toSInt
        denReg += bs
        quoReg += SInt(0, this.bw)
        for (i <- 1 until this.bw) {
            //Calculate the denominator for this iteration.
            val paddingSize: Int = this.bw-i-3
            val curDen = SInt(width=this.bw + paddingSize)
            if (paddingSize > 0) {
                curDen := Cat(denReg(0), SInt(0, paddingSize)).toSInt
            }
            else {
                curDen := denReg(0)(bw-1,-paddingSize)
            }

            //Subtract the current denominator from the current numerator.
            val intres = numReg(0) - curDen
            val curWidth = (this.bw+this.bw-this.iL-2).max(this.bw+this.bw-4)
            val res = intres.clone
            val curQuo = quoReg(0).clone
            //when(intres < SInt(0)) {
            when(intres(curWidth-1)) {
                res := numReg(0)
                curQuo := quoReg(0)
            } .otherwise {
                res := intres
                curQuo := quoReg(0) | (UInt(1,2) << UInt(this.bw-i-1)).toSInt
            }

            //Add an optional register to each signal and add to the ArrayBuffers.
            val sr = Module(new VarShiftRegister[SInt](SInt(), Array(doReg(i-1)), 3))
            sr.io.vin(0) := denReg(0)
            sr.io.vin(1) := res
            sr.io.vin(2) := curQuo

            //Remove old values from I/O registers and add new values.
            val nextDen = denReg.remove(0).clone
            val nextNum = numReg.remove(0).clone
            val nextQuo = quoReg.remove(0).clone
            nextDen := sr.io.vout(0)
            nextNum := sr.io.vout(1)
            nextQuo := sr.io.vout(2)
            denReg += nextDen
            numReg += nextNum
            quoReg += nextQuo
        }
        //Pipeline the 'flip' signal by the same amount as doReg.
        val flipsr = Module(new VarShiftRegister[Bool](Bool(), doReg, 1))
        flipsr.io.vin(0) := flip

        //Shift the original numerator and denominator.
        val quo = this.clone
        when(flipsr.io.vout(0)) {
            quo := castToPsspFixed(-quoReg(0), this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        } .otherwise {
            quo := castToPsspFixed(quoReg(0), this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        }
        quo
    }

    //An algorithm to do manual division, may not work if iL is less than 1.
    def manualDivision(b: PsspFixed): PsspFixed = {
        this.typeAssert(b)
        val regLen: Int = this.bw-1
        val doReg = Array.fill(this.bw-1){ false }

        //Add 'pdiv' registers to doReg - the array which determines where the pipeline registers will be.
        val pmax = this.pdiv+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }

        manualDivision(doReg, b)
    }

    //Length doReg = this.bw-1
    def manualDivisionNew(doReg: Array[Boolean], b: PsspFixed): PsspFixed = {
        this.typeAssert(b)

        //Convert inputs to positive values if required.
        val as = chiselCast(this.abs()) { SInt() }
        val bs = chiselCast(b.abs()) { SInt() }
        //val flip = (chiselCast(this){ SInt() } < SInt(0)) ^ (chiselCast(b){ SInt() } < SInt(0))
        val flip = this(this.bw-1) ^ b(b.bw-1)

        //Calculate division as a sequence of subtractions.
        val numReg = new ArrayBuffer[SInt]()
        val denReg = new ArrayBuffer[SInt]()
        val quoReg = new ArrayBuffer[SInt]()
        val asWidth: Int = bw+iL-1
        if((asWidth-bw) < 1) {
            numReg += as
        } else {
            numReg += Cat(SInt(0, asWidth-bw), as).toSInt
        }
        denReg += bs
        quoReg += SInt(0, this.bw)
        for (i <- 1 until this.bw) {
            //Calculate the denominator for this iteration.
            val paddingSize: Int = iL-1
            val curDenSize: Int = bw + paddingSize
            val curDen = SInt(width=curDenSize)
            curDen := Cat(denReg(0), SInt(0, paddingSize)).toSInt

            val prevNum = numReg(0)
            val curNum = SInt(width=asWidth)
            curNum := Cat(SInt(0,1), prevNum(asWidth-1) | prevNum(asWidth-2), prevNum(asWidth-3,0), SInt(0, 1)).toSInt

            //Subtract the current denominator from the current numerator.
            val intres = curNum - curDen
            val curWidth = curDenSize.max(asWidth)
            val res = intres.clone
            val curQuo = quoReg(0).clone
            //when(intres < SInt(0)) {
            when(intres(curWidth-1)) {
                res := curNum
                curQuo := quoReg(0)
            } .otherwise {
                res := intres
                curQuo := quoReg(0) | (UInt(1,2) << UInt(this.bw-i-1)).toSInt
            }

            //Add an optional register to each signal and add to the ArrayBuffers.
            val sr = Module(new VarShiftRegister[SInt](SInt(), Array(doReg(i-1)), 3))
            sr.io.vin(0) := denReg(0)
            sr.io.vin(1) := res
            sr.io.vin(2) := curQuo

            //Remove old values from I/O registers and add new values.
            val nextDen = denReg.remove(0).clone
            val nextNum = numReg.remove(0).clone
            val nextQuo = quoReg.remove(0).clone
            nextDen := sr.io.vout(0)
            nextNum := sr.io.vout(1)
            nextQuo := sr.io.vout(2)
            denReg += nextDen
            numReg += nextNum
            quoReg += nextQuo
        }
        //Pipeline the 'flip' signal by the same amount as doReg.
        val flipsr = Module(new VarShiftRegister[Bool](Bool(), doReg, 1))
        flipsr.io.vin(0) := flip

        //Shift the original numerator and denominator.
        val quo = this.clone
        when(flipsr.io.vout(0)) {
            quo := castToPsspFixed(-quoReg(0), this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        } .otherwise {
            quo := castToPsspFixed(quoReg(0), this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        }
        quo
    }

    //An algorithm to do manual division, may not work if iL is less than 1.
    def manualDivisionNew(b: PsspFixed): PsspFixed = {
        this.typeAssert(b)
        val regLen: Int = this.bw-1
        val doReg = Array.fill(regLen){ false }

        //Add 'pdiv' registers to doReg - the array which determines where the pipeline registers will be.
        val pmax = this.pdiv+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }
        manualDivisionNew(doReg, b)
    }

    //def blackBox(b: PsspFixed): PsspFixed = {
    //    this.typeAssert(b)
    //    val as = Cat(Cat(SInt(0,1), chiselCast(this){ SInt() }), SInt(0, bw-iL)).toSInt
    //    val bs = Cat(chiselCast(b){ SInt() }, SInt(0, bw-iL)).toSInt
    //    val doReg = Vec.fill(this.bw){ Bool(false) }
    //    //Add some registers to doReg.
    //    val pmax = this.pdiv+1
    //    for (d <- 1 until pmax) {
    //        val i = log2Up((this.bw*d)/pmax)
    //        doReg(i) = Bool(true)
    //    }

    //    //Calculate division as a sequence of subtractions.
    //    numeratorReg = new ArrayBuffer[SInt]()
    //    for (i <- 0 until this.bw) {
    //    }
    //}

    //Helper method. A Fifo to apply to output of each operation - Add some pipeline registers to the output of the instruntions.
    def pout[S <: Data](dtype: S, a: S, cycles: Int): S = {
        if (cycles == 0) {
            val res = a
            res
        }
        else {
            val sr = Module(new VarShiftRegister[S](dtype, Array.fill(cycles){true}, 1))
            sr.io.vin(0) := a
            val res = a.clone
            res := sr.io.vout(0)
            res
        }
        //else {
        //    val regs = Vec.fill(cycles){ Reg(dtype) }
        //    val res = a.clone
        //    regs(0) := a
        //    res := regs(cycles-1)
        //    for(i <- 1 until cycles) {
        //        regs(i) := regs(i-1)
        //    }
        //    res
        //}
    }

    //Works on fixed point data types.
    //Size of doReg is 5?
    def expLutLi(low: Double, high: Double, n: Int, doReg: Array[Boolean]): PsspFixed = {
        funLutLi(low, high, math.exp(_), n, doReg)
    }

    def expLutLi(low: Double, high: Double, n: Int, pexp: Int): PsspFixed = {
        val regLen: Int = 5
        val doReg = Array.fill(regLen){ false }

        //Add 'pdiv' registers to doReg - the array which determines where the pipeline registers will be.
        val pmax = pexp+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }
        expLutLi(low, high, n, doReg)
    }

    //Works on fixed point data types no pipelining.
    def expLutLi(low: Double, high: Double, n: Int): PsspFixed = {
        funLutLi(low, high, math.exp(_), n, Array.fill(5){ false })
    }

    //Works on fixed point data types.
    //Size of doReg is 5?
    def funLutLi(low: Double, high: Double, f: Double => Double, n: Int, doReg: Array[Boolean]): PsspFixed = {
        //Generate the LUT.
        val lut = new ArrayBuffer[PsspFixed]()
        val b = -low
        val m = (n-1)/(high - low)
        val reqIL: Int = (log2Up(n)+2).max(this.iL)
        //val reqIL: Int = this.iL
        val dToP: Double => PsspFixed = PsspFixed(_, this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        val dToPNoOl: Double => PsspFixed = PsspFixed(_, this.bw, reqIL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        val bT = dToPNoOl(b)
        val mT = dToPNoOl(m)

        //generate the LUT.
        for (i <- 0 until n) {
            lut += dToP(f(low + i*((high-low)/(n-1))))
        }
        val lutT = Vec(lut)

        //Determine the indexes for the LUT. Store is luth lutl.
        val index = SInt(width = log2Up(n) + 1)
        val lutl = SInt(width = log2Up(n) + 1)
        val luth = SInt(width = log2Up(n) + 1)
        val excess = this.clone

        //Calculate index with pipeline stages.
        val xsr = Module(new VarShiftRegister[PsspFixed](bT, doReg.slice(0, 1), 1))
        xsr.io.vin(0) := bT + this.convertTo(bT)
        val scal_x = xsr.io.vout(0) * mT
        val scalxsr = Module(new VarShiftRegister[PsspFixed](bT, doReg.slice(1, 2), 1))
        scalxsr.io.vin(0) := scal_x
        //val scal_x = (bT + this.convertTo(bT)) * mT //non-pipelined version.
        index := scalxsr.io.vout(0).truncate()
        val excessTmp = scalxsr.io.vout(0).frac().convertTo(this)

        ////Calculate scal_x and index.
        //val scal_x = (bT + this.convertTo(bT)) * mT
        //index := scal_x.truncate()
        //val excessTmp = scal_x.frac().convertTo(this)

        when (index < SInt(0)) {
            lutl := SInt(0)
            luth := SInt(0)
            excess := dToP(0.0)
        } .elsewhen (index >= SInt(n-1)) {
            lutl := SInt(n-1)
            luth := SInt(n-1)
            excess := dToP(0.0)
        } .otherwise {
            //Include rounding?
            lutl := index
            luth := index + SInt(1)
            excess := excessTmp
        }

        //The code below calculates the following.
        //val ret = (((dToP(1.0) - excess) * lutT(UInt(lutl))) + (excess * lutT(UInt(luth))))

        //Pipelined version.
        val exsr = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(2,3), 1))
        exsr.io.vin(0) := excess
        val lutsr = Module(new VarShiftRegister[SInt](index, doReg.slice(2, 3), 2))
        lutsr.io.vin(0) := lutl
        lutsr.io.vin(1) := luth
        val lowlut = lutT(UInt(lutsr.io.vout(0)))
        val highlut = lutT(UInt(lutsr.io.vout(1)))
        val lutsr2 = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(3, 4), 2))
        lutsr2.io.vin(0) := lowlut
        lutsr2.io.vin(1) := highlut
        val addsr = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(3,4), 2))
        addsr.io.vin(0) := dToP(1.0) - exsr.io.vout(0)
        addsr.io.vin(1) := exsr.io.vout(0)
        val mulsr = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(4,5), 2))
        mulsr.io.vin(0) := addsr.io.vout(0) * lutsr2.io.vout(0)
        mulsr.io.vin(1) := addsr.io.vout(1) * lutsr2.io.vout(1)
        val ret = mulsr.io.vout(0) + mulsr.io.vout(1)

        return ret
    }

    def funLutLi(low: Double, high: Double, f: Double => Double, n: Int, plut: Int): PsspFixed = {
        val regLen: Int = 5
        val doReg = Array.fill(regLen){ false }

        //Add 'pexp' registers to doReg - the array which determines where the pipeline registers will be.
        val pmax = plut+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }
        funLutLi(low, high, f, n, doReg)
    }

    /** Function approximation, using a "bit aligned" lookup table
      *
      * This lookup table uses a small range from the input to look up values in the table.
      * Any value which falls outside if this range saturate to f(low) or f(high) respectively.
      *
      * low: the lowest input value in the table, as a Double
      * high: the highest input value in the table, as a Double
      * f: the function that we wish to model
      * n: the number of entries in the table (must be a power of two)
      * doReg: a vector of Booleans which decides how we will pipeline this module (doReg.length = 3)
      *
      * Example usage:
      *   val expEval: PsspFixed => PsspFixed = _.funLutPow2(-3.96875, 0, math.exp, 128, Array.fill(3){ true })
      */
    def funLutPow2(low: Double, high: Double, f: Double => Double, n: Int, doReg: Array[Boolean]): PsspFixed = {
        //Generate the LUT.
        val lut = new ArrayBuffer[PsspFixed]()
        val b = -low
        val m = (n-1)/(high - low)
        val shiftVal = -log2Up(m.toInt)
        val mCheck = math.pow(2,-shiftVal)
        if (mCheck != m) throw new Exception(f"The scaling value is not a power of two. Given: $m%f, Nearest: $mCheck%f")
        val addressBits = log2Up(n)
        val dToP: Double => PsspFixed = PsspFixed(_, this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        val bT = dToP(b)
        val highT = dToP(high)

        //generate the LUT.
        for (i <- 0 until n) {
            lut += dToP(f(low + i*((high-low)/(n-1))))
        }
        val lutT = Vec(lut)

        //Determine the datatype for the LUT index.
        val index = UInt(width = addressBits)

        //Calculate index with pipeline stages, the following but with optional registers:
        // index = this + bT
        val biasedVal = if(doReg(0)) RegNext(this + bT) else this + bT
        // Grab bits of interest and others which affect the index calculation.
        val maxIndex = if(doReg(0)) RegNext(this > highT) else this > highT
        val lsbBitIndex = bw-iL+shiftVal
        val msbBitIndex = lsbBitIndex + addressBits
        val sign = biasedVal(bw-1)
        val round = biasedVal(lsbBitIndex-1)

        // Calculate the index
        when (sign) { // Negative value
            index := UInt(0)
        } .elsewhen (maxIndex) { // Large positive value
            index := UInt(n-1)
        } .otherwise {
            index := round + chiselCast(biasedVal(msbBitIndex, lsbBitIndex)){ UInt() }
        }

        val delayIndex = if(doReg(1)) RegNext(index) else index
        val ret = if(doReg(2)) RegNext(lutT(delayIndex)) else lutT(delayIndex)

        return ret
    }

    /** Function approximation using a "bit-aligned" look up table.
      *
      * This lookup table uses a small range from the input to look up values in the table.
      * Any value which falls outside if this range saturate to f(low) or f(high) respectively.
      *
      * low: the lowest input value in the table, as a Double
      * high: the highest input value in the table, as a Double
      * f: the function that we wish to model
      * n: the number of entries in the table (must be a power of two)
      * plut: the number of pipeline registers to add to the evaluation (max(plut) = 3)
      *
      * Example usage:
      *   val expEval: PsspFixed => PsspFixed = _.funLutPow2(-3.96875, 0, math.exp, 128, 3)
      */
    def funLutPow2(low: Double, high: Double, f: Double => Double, n: Int, plut: Int): PsspFixed = {
        val regLen: Int = 3
        val doReg = Array.fill(regLen){ false }

        //Add 'pexp' registers to doReg - the array which determines where the pipeline registers will be.
        val pmax = plut+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }
        funLutPow2(low, high, f, n, doReg)
    }

    /** Function approximation using Remez
      *
      * Currently support arbitrary ranges, perhaps do a variant which uses the MSBs to access the LUTs.
      * doReg.length = 2*log2Up(order+1) + 1.
      * TODO: Calculate the index of the LUT better.
      */
    def funLutInterpRemez(low: Double, high: Double, f: Double => Double, fd: Double => Double, order: Int, doReg: Array[Boolean], splits: Int = 1): PsspFixed = {
        val interval = (high - low)/splits
        val junct = ArrayBuffer[Double]()
        val coeffs = ArrayBuffer[Array[Double]]()
        val fromD: Double => PsspFixed = PsspFixed(_, this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        val toD: PsspFixed => Double = PsspFixed.toD(_)

        // Generate the tables of polynomials.
        for (i <- 0 until splits) {
            val l = i*interval + low
            val h = (i+1)*(interval) + low
            if (i > 0) junct += l
            val rmz = new Remez(f, fd, l, h, order)
            val a = rmz.bCoeffs.toArray.reverse
            //println(Array(l, h).mkString("{",", ","}"))
            //println(a.mkString("{",", ","}"))
            //println(a.map(f => toD(fromD(f))).mkString("{",", ","}"))
            //println(rmz.coeffs.toArray.reverse.mkString("{",", ","}"))
            coeffs += a
        }

        // Convert tables to ROMs.
        val luts = ArrayBuffer[Vec[PsspFixed]]()
        for (j <- 0 to order) {
            val x = ArrayBuffer[PsspFixed]()
            for (i <- 0 until splits) {
                x += fromD(coeffs(i)(j))
            }
            luts += Vec(x)
        }

        // Find out the index that 'this' is in.
        val ind = UInt(width=log2Up(splits))
        if (splits != 1) {
            if (pgt != 0) { throw new Exception("Function interpolation using Remez (with multiple splits) currently only works when pgt=0") }
            val bitVal = junct.map(f => if (doReg(0)) { Reg(next=(fromD(f) < this)) } else { fromD(f) < this })
            ind := ReduceTree[UInt](bitVal.map(UInt(_)), (x:UInt,y:UInt) => UInt(Cat(UInt(0),x))+y)
            //ind := bitVal.map(UInt(_)).reduce((x,y) => UInt(Cat(UInt(0),x))+y)
        } else {
            ind := UInt(0)
        }

        // Get the correct coefficients.
        val c = Vec.fill(order+1){ this.clone }
        for (i <- 0 to order) {
            c(i) := luts(i)(ind)
        }

        // Delay 'this'.
        val insr = Module(new VarShiftRegister(this, doReg.slice(0, 1) ++ Array.fill(pgt){ true }, 1))
        insr.io.vin(0) := this

        // Calculate the polynomial and connect to outputs.
        val pe = Module(new PolyEvalEstrins[PsspFixed](this, order, doReg.slice(1, doReg.length), _+_, _*_, padd=this.padd, pmul=this.pmul))
        pe.io.x := insr.io.vout(0)
        pe.io.coeffs := c
        pe.io.y
    }

    def divLutLi(low: Double, high: Double, n: Int, doReg: Array[Boolean], den: PsspFixed): PsspFixed = {
        this.typeAssert(den)
        val invden = den.funLutLi(low, high, 1.0 / (_: Double), n, doReg.slice(0,5))
        val invdensr = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(5,6), 1))
        invdensr.io.vin(0) := invden
        val numsr = Module(new VarShiftRegister[PsspFixed](this, doReg, 1))
        numsr.io.vin(0) := this
        val res = numsr.io.vout(0) * invdensr.io.vout(0)
        res
    }

    def divLutLi(low: Double, high: Double, n: Int, den: PsspFixed): PsspFixed = {
        this.typeAssert(den)
        val regLen: Int = 6
        val doReg = Array.fill(regLen){ false }

        //Add 'pdiv' registers to doReg.
        val pmax = pdiv+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }

        divLutLi(low, high, n, doReg, den)
    }

    /** Division approximation via "bit-aligned" look up tables.
      *
      * low: the lowest input value in the table, as a Double
      * high: the highest input value in the table, as a Double
      * n: the number of entries in the table (must be a power of two)
      * doReg: a vector of Booleans which decides how we will pipeline this module (doReg.length = 4)
      * den: the denominator in the division (i.e., the divider)
      *
      * Example usage:
      *   val div: PsspFixed => PsspFixed = _.divLutPow2(2e-6, 127*2e-6, 128, divReg, _)
      */
    def divLutPow2(low: Double, high: Double, n: Int, doReg: Array[Boolean], den: PsspFixed): PsspFixed = {
        this.typeAssert(den)
        val invden = den.funLutPow2(low, high, 1.0 / (_: Double), n, doReg.slice(0,3))
        val invdensr = Module(new VarShiftRegister[PsspFixed](this, doReg.slice(3,4), 1))
        invdensr.io.vin(0) := invden
        val numsr = Module(new VarShiftRegister[PsspFixed](this, doReg, 1))
        numsr.io.vin(0) := this
        val res = numsr.io.vout(0) * invdensr.io.vout(0)
        res
    }

    /** Division approximation via "bit-aligned" look up tables.
      *
      * low: the lowest input value in the table, as a Double
      * high: the highest input value in the table, as a Double
      * f: the function that we wish to model
      * n: the number of entries in the table (must be a power of two)
      *
      * Example usage:
      *   val div: PsspFixed => PsspFixed = _.divLutPow2(2e-6, 127*2e-6, 128, _)
      */
    def divLutPow2(low: Double, high: Double, n: Int, den: PsspFixed): PsspFixed = {
        this.typeAssert(den)
        val regLen: Int = 4
        val doReg = Array.fill(regLen){ false }

        //Add 'pdiv' registers to doReg.
        val pmax = pdiv+1
        for (d <- 1 until pmax) {
            val i: Int = (regLen*d)/pmax
            doReg(i) = true
        }

        divLutPow2(low, high, n, doReg, den)
    }

    /** Polynomial evaluation using Horner's method.
      *
      * doReg.length = 2*(coeffs.length-1)
      * coefficients are in order from smallest powers of this to the largest.
      */
    def polyEvalHorners(coeffs: Array[Double], doReg: Array[Boolean]): PsspFixed = {
        val order = coeffs.length-1
        val res = this.clone
        val pipeIn = ArrayBuffer[VarShiftRegister[this.type]]()
        val pipeRes = ArrayBuffer[Rege[this.type]]()
        val fromD: Double => PsspFixed = PsspFixed(_, this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)
        pipeIn += Module(new VarShiftRegister[this.type](this, Array(false), 1))
        pipeRes += Module(new Rege(this, false))
        pipeRes(0).io.din := fromD(coeffs(order))
        pipeIn(0).io.vin(0) := this

        var ind = 0
        for (i <- 0 until order reverse) {
            // Create new pipeline registers.
            if (i > 0) pipeIn += Module(new VarShiftRegister[this.type](this, doReg.slice(ind, ind+2) ++ Array.fill(this.pmul + this.padd){ true }, 1))
            pipeRes += Module(new Rege(this, doReg(ind)))
            pipeRes += Module(new Rege(this, doReg(ind+1)))

            // Delay 'this'
            if (i > 0) pipeIn(1).io.vin(0) := pipeIn(0).io.vout(0)

            // Compute next iteration of horner's method.
            pipeRes(1).io.din := pipeRes(0).io.dout * pipeIn(0).io.vout(0)
            pipeRes(2).io.din := pipeRes(1).io.dout + fromD(coeffs(i))

            // Remove used registers from the pipeline.
            pipeIn -= pipeIn(0)
            pipeRes -= pipeRes(0)
            pipeRes -= pipeRes(0)

            //Increment doReg.
            ind += 2
        }
        res := pipeRes(0).io.dout
        res
    }

    /** Polynomial evaluation using Estrin's method.
      *
      * The method is from 'The Art of Computer Programming: Seminumerical Algorithms', page 488.
      * Estrin's method is equivalent to a parallel version of horner's method.
      * doReg.length = 2*log2Up(order+1)
      * coefficients are in order from smallest powers of this to the largest.
      * Need to add a check that doReg is the right size.
      */
    def polyEvalEstrins(coeffs: Array[Double], doReg: Array[Boolean]): PsspFixed = {
        val order = coeffs.length-1
        val a = ArrayBuffer[PsspFixed]()
        val fromD: Double => PsspFixed = PsspFixed(_, this.bw, this.iL, this.padd, this.psub, this.pmul, this.pdiv, this.pgt)

        // Add value to the pipeline in reverse order.
        for (i <- 0 to order reverse) {
            a += fromD(coeffs(i))
        }
        val res = if (order > 0) estrins(this, a.to[Array], doReg) else a(0)
        res
    }

    /** A recursive function to create the hardware for Estrin's method. Note: coeffs in ordered in largest powers of x to the smallest. */
    def estrins(x: PsspFixed, coeffs: Array[PsspFixed], doReg: Array[Boolean]): PsspFixed = {
        val ops = coeffs.length
        val odd = !(ops % 2 == 0)
        val regs = Array(doReg(0), doReg(1))
        val nextReg = doReg.slice(2, doReg.length)
        val next = ArrayBuffer[PsspFixed]()
        val add = ArrayBuffer[PsspFixed]()
        val cur = coeffs.to[ArrayBuffer]

        // Pass the leftover value straight to the next stage (with some delay).
        if (odd) {
            val sr = Module(new VarShiftRegister(this, regs ++ Array.fill(pmul + padd){ true }, 1))
            sr.io.vin(0) := coeffs(0)
            cur -= cur(0)
            next += sr.io.vout(0)
        }

        // Calculate multiply stages.
        while (cur.length > 0) {
            val r = Module(new Rege(this, regs(0)))
            val sr = Module(new VarShiftRegister(this, regs.slice(0, 1) ++ Array.fill(pmul){ true }, 1))
            r.io.din := cur(0) * x
            sr.io.vin(0) := cur(1)

            // Add values to the next stage.
            add += r.io.dout
            add += sr.io.vout(0)

            // Remove values from cur.
            cur -= cur(0)
            cur -= cur(0)
        }

        // Calculate the addition stages, add to nextReg.
        while (add.length > 0) {
            val r = Module(new Rege(this, regs(1)))
            r.io.din := add(0) + add(1)
            next += r.io.dout

            // Remove values from add.
            add -= add(0)
            add -= add(0)
        }

        if (next.length == 1) {
            next(0)
        } else {
            // Calculate x^2 and delay it, create next stage in the tree.
            val sr = Module(new VarShiftRegister(this, regs ++ Array.fill(padd){ true }, 1))
            sr.io.vin(0) := x*x
            estrins(sr.io.vout(0), next.to[Array], nextReg)
        }
    }

}

object PsspFixed {
    def apply(x: Int): PsspFixed = Lit(x){ PsspFixed() };
    def apply(x: Int, width: Int): PsspFixed = {
        val k = Lit(x, width){ PsspFixed() }
        k.bw = width
        k
    }
    def apply(x: Int, width: Int, iL: Int, padd: Int, psub: Int, pmul: Int, pdiv: Int, pgt: Int): PsspFixed = { 
        val res = Lit(x, width){ PsspFixed() }
        res.iL = iL
        res.bw = width
        res.padd = padd
        res.psub = psub
        res.pmul = pmul
        res.pdiv = pdiv
        res.pgt = pgt
        res
    }
    def apply(x: BigInt): PsspFixed = Lit(x){ PsspFixed() };
    def apply(x: BigInt, width: Int): PsspFixed = {
        val k = Lit(x, width){ PsspFixed() }
        k.bw = width
        k
    }
    def apply(x: BigInt, width: Int, iL: Int): PsspFixed = {
        val res = PsspFixed(x, width, iL, 0, 0, 0, 0, 0)
        res
    }
    def apply(x: BigInt, width: Int, iL: Int, padd: Int, psub: Int, pmul: Int, pdiv: Int, pgt: Int): PsspFixed = {
        val res = Lit(x, width){ PsspFixed() }
        res.iL = iL
        res.bw = width
        res.padd = padd
        res.psub = psub
        res.pmul = pmul
        res.pdiv = pdiv
        res.pgt = pgt
        res
    }
    def apply(dir: IODirection = null, width: Int = -1, iL: Int = -1, padd: Int = 0, psub: Int = 0, pmul: Int = 0, pdiv: Int = 0, pgt: Int = 0): PsspFixed = {
        val res = new PsspFixed()
        res.create(dir, width)
        res.iL = iL
        res.bw = width
        res.padd = padd
        res.psub = psub
        res.pmul = pmul
        res.pdiv = pdiv
        res.pgt = pgt
        res
    }
    def apply(b: Double, width: Int, iL: Int, padd: Int, psub: Int, pmul: Int, pdiv: Int, pgt: Int): PsspFixed = {
        val bin: BigInt = (b*math.pow(2,width-iL)).toInt
        val k = PsspFixed(bin, width, iL, padd, psub, pmul, pdiv, pgt)
        k
    }
    def apply(b: Double, width: Int, iL: Int): PsspFixed = {
        val bin: BigInt = (b*math.pow(2,width-iL)).toInt
        val k = PsspFixed(bin, width, iL, 0, 0, 0, 0, 0)
        k
    }
    def toD(b: PsspFixed): Double = {
        val bin: BigInt = if(b(b.bw-1).litValue() == 1) b.litValue() - math.pow(2, b.bw).toLong else b.litValue()
        val d: Double = (bin.toDouble*math.pow(2,b.iL-b.bw))
        d
    }
    def toD(ubin: BigInt, b: PsspFixed): Double = {
        val bin: BigInt = if(ubin >= math.pow(2, b.bw-1).toInt) ubin - math.pow(2, b.bw).toLong else ubin
        val d: Double = (bin.toDouble*math.pow(2,b.iL-b.bw))
        d
    }
}

