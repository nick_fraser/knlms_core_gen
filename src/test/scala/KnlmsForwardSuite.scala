
import PKAF._
import Chisel._
import org.scalatest._
import scala.collection.mutable.ArrayBuffer

/** A simple suite for testing the KNLMS forward path module.
  *
  * TODO: Test more complex latencies.
  * TODO: Test with more pipelined operations (pexp).
  */
class KnlmsForwardSuite extends FlatSpec with Matchers {
    /** A quick tester for the KNLMS forward path.
      *
      * TODO: Test multiple iterations.
      * TODO: Test using random numbers.
      */
    class KNLMSForwardTest[T <: Bits](c: KNLMSForward[T], toD: BigInt => Double, fromD: Double => T, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double, tol: Double, isTrace: Boolean = true, verbose: Int = 0) extends Tester(c, isTrace) {
        def ConnectInputs(x: Vec[T], y: T, din: Vec[T], ain: Vec[T], cin: UInt, valid: Boolean): Unit = {
            //Connect cin and y.
            poke(c.io.cin, cin.litValue())
            poke(c.io.y, y.litValue())
            poke(c.io.valid_in, Bool(valid).litValue())
    
            //Connect dictionary.
            for (i <- 0 until c.n) {
                for (j <- 0 until c.m) {
                    val index: Int = i*c.m + j
                    poke(c.io.Din(index), din(index).litValue())
                }
            }
    
            //Connect x.
            for (i <- 0 until c.m) {
                poke(c.io.x(i), x(i).litValue())
            }
    
            //Connect alpha.
            for (i <- 0 until c.n) {
                poke(c.io.Ain(i), ain(i).litValue())
            }
        }
    
        def FetchOutputs(): KNLMSOutVar = {
            //Get cin and y.
            val cin = Lit(peek(c.io.cout), log2Up(c.n) + 1){ UInt(width = log2Up(c.n) + 1) }
            val ybar = fromD(toD(peek(c.io.ybar)))
    
            //Get dictionary.
            val dout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                for (j <- 0 until c.m) {
                    val index: Int = i*c.m + j
                    dout += fromD(toD(peek(c.io.Dout(index))))
                }
            }
    
            //Connect alpha.
            val aout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                aout += fromD(toD(peek(c.io.Aout(i))))
            }
    
            //for debugging.
            //for (i <- 0 until c.n) {
            //    peek(c.io.k(i))
            //}
    
            val outvars = new KNLMSOutVar(cin, ybar, Vec(dout), Vec(aout), c.n, c.m)
            outvars
        }
    
        //Helper classes.
        protected class KNLMSOutVar(cout: UInt, ybar: T, dout: Vec[T], aout: Vec[T], n: Int, m: Int) {
            val c = cout
            val y = ybar
            val d = Vec[T](dout)
            val a = Vec[T](aout)
        }
    
        val n = c.n
        val m = c.m
        val x = Array.fill(m){ 1.0 }
        val y = 1.0
        val sw = new KNLMSSoft(n, m, -toD(fromD(neg_gamma).litValue()), toD(fromD(eta).litValue()), toD(fromD(epsilon).litValue()), toD(fromD(thresh).litValue()))
        //Pass inputs to KNLMSForwardTest.
        ConnectInputs(Vec(x.map(fromD(_))), fromD(y), Vec.fill(n*m){ fromD(0.0) }, Vec.fill(n){ fromD(0.0) }, UInt(0, log2Up(c.n)+1), true)
    
        //Step through by the required number of cycles.
        val cycles : Int = c.latency
        val preRead: Int = 1
        val postRead: Int = 0
        if (cycles == 1) {
            step(cycles)
        }
        else if (cycles > 1) {
            step(1)
            ConnectInputs(Vec.fill(c.m){ fromD(0.0) }, fromD(0.0), Vec.fill(c.n*c.m){ fromD(0.0) }, Vec.fill(c.n){ fromD(0.0) }, UInt(1, log2Up(c.n)+1), false)
            step(cycles - preRead)
        }
    
        var hwout: KNLMSOutVar = null
        for (i <- 0 until preRead + postRead) {
            val out = FetchOutputs()
            hwout = out
            step(1)
        }

        sw.train(x,y)
        for (i <- 0 until n){
            for (j <- 0 until m) {
                val k = j+i*m
                val dhw = toD(hwout.d(k).litValue())
                val dsw = sw.d(k)
                if (verbose > 0) println(f"hw: d($k%d) = ${toD(hwout.d(k).litValue())}%.2e, sw: d($k%d) = ${sw.d(k)}%.2e, err: ${toD(hwout.d(k).litValue())-sw.d(k)}%.2e")
                toD(hwout.d(j+i*m).litValue()) should be (sw.d(j+i*m) +- tol)
            }
        }
        for (i <- 0 until n){
            if (verbose > 0) println(f"hw: a($i%d) = ${toD(hwout.a(i).litValue())}%.2e, sw: a($i%d) = ${sw.a(i)}%.2e, err: ${toD(hwout.a(i).litValue())-sw.a(i)}%.2e")
            toD(hwout.a(i).litValue()) should be (sw.a(i) +- tol) 
        }

    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ false }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -4, 0, 64, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val div: (Flo, Flo) => Flo = _/_
    val fromD: Double => Flo = Flo(_)
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-9
    "The KNLMSForward Module" should f"update the dictionary and weights: test=$test%d, T=Flo, exp=lutLi, div=/, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[Flo](Flo(), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ true }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -4, 0, 64, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val div: (Flo, Flo) => Flo = _/_
    val fromD: Double => Flo = Flo(_)
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-9
    it should f"update the dictionary and weights: test=$test%d, T=Flo, exp=lutLi, div=/, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[Flo](Flo(), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ false }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val w = 32
    val iL = 16
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4.0, 0.0, 64)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _/_
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL))
    val tol = 1e-4
    it should f"update the dictionary and weights: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=lutLi, div=/, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[PsspFixed](PsspFixed(width=w,iL=iL), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ true }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val w = 32
    val iL = 16
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4.0, 0.0, 64)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _/_
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL))
    val tol = 1e-4
    it should f"update the dictionary and weights: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=lutLi, div=/, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[PsspFixed](PsspFixed(width=w,iL=iL), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ false }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val w = 32
    val iL = 16
    val pdiv = 31
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4.0, 0.0, 64)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 1e-4
    it should f"update the dictionary and weights: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=lutLi, div=manualDivision, latency=${CountReg.nreg(doReg)+pdiv}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

    {
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(13){ true }
    val thresh = 0.5
    val negGamma = -0.5
    val eps = 0.0001
    val eta = 0.01

    val w = 32
    val iL = 16
    val pdiv = 31
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4.0, 0.0, 64)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 1e-4
    it should f"update the dictionary and weights: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=lutLi, div=manualDivision, latency=${CountReg.nreg(doReg)+pdiv}%d, tol=$tol%.2e" in {
        chiselMainTest(tutArgs, () => Module(
            new KNLMSForward[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv), negGamma, thresh, eps, eta,
                                        doReg, n, m, _*_, div, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv)))
                { c => new KNLMSForwardTest(c, toD, fromD, negGamma, thresh, eps, eta, tol, verbose=0, isTrace=false) }
    }}

}

