
import PKAF._
import Chisel._
import org.scalatest._
import scala.collection.mutable.ArrayBuffer

/** A test suite for the AddToDict module.
  *
  * TODO: Test more bit widths.
  * TODO: Test more vector lengths.
  */
class AddToDictSuite extends FlatSpec with Matchers {
    /** A tester for the AddToDict Module.
      *
      * TODO: Test with random inputs.
      * TODO: Test over multiple iterations.
      * TODO: Make the test more generic, use DKNLMSSoft to simulate it.
      */
    class AddToDictTest[T <: Bits](c: AddToDict[T], toD: BigInt => Double, fromD: Double => T, isTrace: Boolean = true) extends Tester(c, isTrace) {
        def connectInputs(k: Vec[T], knn: T, x: Vec[T], thresh: T, n: Int, m: Int, valid: Boolean): Unit = {
            //Connect scalars.
            poke(c.io.knn, knn.litValue())
            poke(c.io.thresh, thresh.litValue())
            poke(c.io.valid_in, Bool(valid).litValue())
    
            //Connect k.
            for (i <- 0 until n)
                poke(c.io.k(i), k(i).litValue())
    
            //Connect x.
            for (i <- 0 until m)
                poke(c.io.x(i), x(i).litValue())
        }

        def fetchOutputs(): AddToDictOutVar = {
            //Add debug signals.
            //peek(c.io.update)
            //peek(c.io.tmp)
            //peek(c.io._c)
    
            //Get cin.
            val cin = Lit(peek(c.io.c), log2Up(c.n) + 1){ UInt(width = log2Up(c.n) + 1) }
            val valid = peek(c.io.valid_out)
    
            //Get dictionary.
            val dout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                for (j <- 0 until c.m) {
                    val index: Int = i*c.m + j
                    dout += fromD(toD(peek(c.io.D(index)).toInt))
                }
            }
    
            //Connect kbar.
            val kout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                kout += fromD(toD(peek(c.io.kbar(i)).toInt))
            }
            val outvars = new AddToDictOutVar(Vec(kout), Vec(dout), cin, c.n, c.m)
            outvars
        }
    
        protected class AddToDictOutVar(kbarin: Vec[T], Din: Vec[T], coutin: UInt, n: Int, m: Int) {
            def ===[T](that: AddToDictOutVar): Unit = {
                this.cout.litValue() should be (that.cout.litValue())
                for (i <- 0 until n) {
                    this.kbar(i).litValue() should be (that.kbar(i).litValue())
                    for(j <- 0 until m) { this.D(j + m*i).litValue() should be (that.D(j + m*i).litValue()) }
                }
            }
            val kbar = Vec[T](kbarin)
            val D = Vec[T](Din)
            val cout = coutin
        }

        protected class AddToDictSoft(val n: Int, val m: Int, val thresh: T) {
            def apply(kt: Vec[T], xt: Vec[T], knn: T): AddToDictOutVar = {
                val k = kt.map(t => toD(t.litValue())).toArray
                val x = xt.map(t => toD(t.litValue())).toArray
                val mu = k.map(math.abs(_)).max
                val thrsh = toD(thresh.litValue())
                if (mu < thrsh && c < n) {
                    // Update k, c. Add x to d.
                    k(c) = toD(knn.litValue())
                    for(i <- 0 until m){ d(c*m + i) = x(i) }
                    c += 1
                }
                // Pad k with zeros.
                for (i <- c until n){ k(i) = 0.0 }
                val kbar = Vec(k.map(fromD))
                val dt = Vec(d.map(fromD))
                new AddToDictOutVar(kbar, dt, UInt(c, log2Up(n)), n, m)
            }
            var d = Array.fill(n*m){ 0.0 }
            var c = 0
        }
    
        // Get values from the module.
        val m = c.m
        val n = c.n
        val latency = c.latency

        //Step through by the required number of cycles.
        val tests = 2
        val cycles : Int = latency + tests

        // Connect initial inputs.
        val thresh = fromD(0.9)
        val res = ArrayBuffer[AddToDictOutVar]()
        val exp = ArrayBuffer[AddToDictOutVar]()
        val sw = new AddToDictSoft(n, m, thresh)
        for (i <- 0 until cycles) {
            if (i == 0) {
                val k = Vec.fill(n){ fromD(0) }
                val knn = fromD(1)
                val x = Vec.fill(m){ fromD(1) }
                connectInputs(k, knn, x, thresh, n, m, true)
                if (i < tests) { exp += sw(k, x, knn) }
            } else {
                val k = Vec.fill(n){ fromD(0.8) }
                val knn = fromD(1)
                val x = Vec.fill(m){ fromD(0.5) }
                connectInputs(k, knn, x, thresh, n, m, true)
                if (i < tests) { exp += sw(k, x, knn) }
            }
            if (i >= latency) { res += fetchOutputs() }
            step(1)
        }

        for (i <- 0 until tests) {
            res(i) === exp(i)
        }
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    { // Basic Flo test.
    test += 1
    val n = 2
    val m = 2
    val doReg = Array.fill(2){ false }
    val fromD: Double => Flo = Flo(_)
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    "The AddToDict Module" should f"add some entries to the dictionary: test=$test%d, T=Flo, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new AddToDict[Flo](Flo(), doReg, n, m, _ > _, fromD)))
            { c => new AddToDictTest(c, toD, fromD, false) }
    }}

    { // PsspFixed test.
    test += 1
    val w = 18
    val iL = 5
    val n = 2
    val m = 2
    val doReg = Array.fill(2){ true }
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL))
    it should f"add some entries to the dictionary: test=$test%d, T=PsspFixed, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new AddToDict[PsspFixed](PsspFixed(width=w,iL=iL), doReg, n, m, _ > _, fromD)))
            { c => new AddToDictTest(c, toD, fromD, false) }
    }}

}

