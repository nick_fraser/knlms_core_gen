
import PKAF._
import Chisel._
import org.scalatest._
import scala.collection.mutable.ArrayBuffer

/** A test suite for the NormalisedUpdateAlpha module.
  *
  * TODO: Test more bit widths.
  * TODO: Test more vector lengths.
  * TODO: Test more latency values.
  */
class NormalisedUpdateAlphaSuite extends FlatSpec with Matchers {
    /** A test suite for the update alpha tester.
      *
      * TODO: Make test over multiple iterations.
      * TODO: Update test to use random values.
      */
    class NormalisedUpdateAlphaTest[T <: Bits](c: NormalisedUpdateAlpha[T], toD: BigInt => Double, fromD: Double => T, tol: Double, isTrace: Boolean = true) extends Tester(c, isTrace) {
        def connectInputs(k: Vec[T], epsilon: T, eta: T, err: T, kdotk: T, n: Int, valid: Boolean): Unit = {
            //Connect scalars.
            poke(c.io.kdotk, kdotk.litValue())
            poke(c.io.epsilon, epsilon.litValue())
            poke(c.io.eta, eta.litValue())
            poke(c.io.err, err.litValue())
            poke(c.io.valid_in, Bool(valid).litValue())
    
            //Connect alpha.
            for (i <- 0 until n)
                poke(c.io.k(i), k(i).litValue())
        }
        def fetchOutputs(): Vec[T] = {
            //Connect alpha.
            val aout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                aout += fromD(toD(peek(c.io.alpha(i)).toInt))
            }
            val outvars = Vec(aout)
            outvars
        }
        def archSim(k: Array[Double], epsilon: Double, eta: Double, err: Double, kdotk: Double, n: Int): Array[Double] = {
            val step = ((eta * err)/(epsilon + kdotk))
            return k.map((f:Double) => f*step)
        }
    
        connectInputs(Vec(fromD(1) +: List.fill(c.n-1){ fromD(0) }), fromD(0.0001), fromD(0.01), fromD(1), fromD(1), c.n, true)
    
        //Step through by the required number of cycles.
        val cycles : Int = c.latency
        if (cycles == 1) {
            step(cycles)
        }
        else if (cycles > 1) {
            step(1)
            connectInputs(Vec.fill(c.n){ fromD(0) }, fromD(0.0001), fromD(0.01), fromD(1), fromD(1), c.n, false)
            step(cycles - 1)
        }
     
        //step(1)
        val out = fetchOutputs()
        val simout = archSim(1.0 +: Array.fill(c.n-1){ 0.0 }, 0.0001, 0.01, 1, 1, c.n)
        for (i <- 0 until c.n) {
            //println("Expected value: " + simout(i) + ", Value: " + toD(out(i).litValue()) + ", Error: " + (simout(i) - toD(out(i).litValue())))
            toD(out(i).litValue()) should be (simout(i) +- tol)
        }
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 1
    val n = 2
    val doReg = Array.fill(3){ false }
    val fromD: Double => Flo = Flo(_)
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-9
    "The NormalisedUpdateAlpha Module" should f"update the weights: test=$test%d, T=Flo, n=$n%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%e" in {
    chiselMainTest(tutArgs, () => Module(
        new NormalisedUpdateAlpha[Flo](Flo(), doReg, n, _*_, _+_, _/_, fromD)))
            { c => new NormalisedUpdateAlphaTest(c, toD, fromD, tol, false) }
    }}

    {
    test += 1
    val w = 16
    val n = 2
    val doReg = Array.fill(3){ false }
    val dtoI: Double => Int = _.toInt
    val fromD: Double => SInt = dtoI.andThen(SInt(_, width=w))
    val toD: BigInt => Double = Lit(_, w){ SInt() }.litValue().toDouble
    val tol = 1e-2
    it should f"update the weights: test=$test%d, T=SInt(width=$w%d), n=$n%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%e" in {
    chiselMainTest(tutArgs, () => Module(
        new NormalisedUpdateAlpha[SInt](SInt(width=w), doReg, n, _*_, _+_, _/_, fromD)))
            { c => new NormalisedUpdateAlphaTest(c, toD, fromD, tol, false) }
    }}

    {
    test += 1
    val w = 18
    val iL = 5
    val n = 2
    val doReg = Array.fill(3){ false }
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL))
    val tol = 5e-4
    it should f"update the weights: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), n=$n%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%e" in {
    chiselMainTest(tutArgs, () => Module(
        new NormalisedUpdateAlpha[PsspFixed](PsspFixed(width=w, iL=iL), doReg, n, _*_, _+_, _/_, fromD)))
            { c => new NormalisedUpdateAlphaTest(c, toD, fromD, tol, false) }
    }}

}
