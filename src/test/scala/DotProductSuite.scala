
import PKAF._
import Chisel._
import org.scalatest._

/** A test suite for the DotProduct module.
  *
  * TODO: Test more bit widths.
  * TODO: Test more vector lengths.
  */
class DotProductSuite extends FlatSpec with Matchers {
    /** A helper class which extends for implementing the tester.
      *
      * TODO: Test random vectors.
      * TODO: Test multiple iterations.
      */
    class DotProdTreeTest[T <: Bits](c: DotProdTree[T], toD: BigInt => Double, fromD: Double => T, isTrace: Boolean = true) extends Tester(c, isTrace) {
        val x = Array.fill(c.n){-0.5}
        val y = Array.fill(c.n){0.75}
        val res = (x, y).zipped.map(_*_).reduce(_+_)
        for (i <- 0 until c.n) {
            poke(c.io.x(i), fromD(x(i)).litValue())
            poke(c.io.y(i), fromD(y(i)).litValue())
        }
        val cycles : Int = c.latency
        if (cycles == 1) {
            step(cycles)
        }
        else if (cycles > 1) {
            step(1)
            for (i <- 0 until c.n) {
                poke(c.io.x(i), 0)
                poke(c.io.y(i), 0)
            }
            step(cycles - 1)
        }
        val hwres = toD(peek(c.io.res).toInt)
        //Output fails.
        hwres should be (res)
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    { // Datatype specfications.
    test += 1
    val w = 18
    val iL = 5
    val n = 2
    val doReg = Array.fill(2){ false } 
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL))
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL)
    "The DotProduct Module" should f"pass test $test%d: n=$n%d, T=PsspFixed(width=$w%d, iL=$iL%d), latency=0" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[PsspFixed](PsspFixed(width=w,iL=iL), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 2
    val doReg = Array.fill(2){ false } 
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=0" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 2
    val doReg = Array.fill(2){ true }
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=2" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 7
    val doReg = Array.fill(log2Up(n)+1){ false }
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=0" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 7
    val doReg = Array.fill(log2Up(n)+1){ true }
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=${CountReg.nreg(doReg)}%d" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 62
    val doReg = Array.fill(log2Up(n)+1){ false }
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=0" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 62
    val doReg = Array.fill(log2Up(n)+1){ true }
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=${CountReg.nreg(doReg)}%d" in {
        chiselMainTest(tutArgs, () => Module(new DotProdTree[Flo](Flo(), doReg, n, _*_, _+_)))
            { c => new DotProdTreeTest(c, toD, fromD, false) }
    }}

}

