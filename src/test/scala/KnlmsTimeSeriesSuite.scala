
import PKAF._
import Chisel._
import org.scalatest._
import scala.collection.mutable.ArrayBuffer
import util.Random

/** A simple test suite for the KNLMSTimeSeriesWrapper module.
  *
  * TODO: Test with more datatypes.
  */
class KnlmsTimeSeriesSuite extends FlatSpec with Matchers {
    /** A tester class for KNLMSTS.
      *
      */
    class KNLMSTimeSeriesWrapperTest[T <: Bits](c: KNLMSTimeSeriesWrapper[T], toD: BigInt => Double, fromD: Double => T, ttod: T => Double, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double,
                                                tol: Double, seed: Int = 0, verbose: Int = 1, isTrace: Boolean = true) extends Tester(c, isTrace) {
        // Get some parameters from the hardware.
        val n = c.n
        val m = c.m
        val delay = c.latency
        //print(f"delay=$delay%d\n")
    
        // Set up number of trials and a success flag.
        val trials: Int = 50
        var success: Boolean = true
    
        // Set up the parameters for the experiment.
        val SoftMod = new DKNLMSSoft(n, m, -neg_gamma, eta, epsilon, thresh, delay)
        val rand = new Random(seed)
        val noise = 0.01
    
        var x = ArrayBuffer.fill(m){ 0.0 }
    
        // Run the simulation.
        for (i <- 0 until trials + delay) {
            // Calculate new value in series.
            val y = toD(fromD(noise*rand.nextGaussian() + math.sin(i.toDouble)).litValue())
    
            SoftMod.train(x.toArray, y)
            val yest_sw = ttod(fromD(SoftMod.ybar)) // Get the value and then quantise.
            //val yest_sw = SoftMod.ybar // Get the value and then quantise.
    
            // Connect y to the hardware.
            poke(c.io.y, fromD(y).litValue())
            poke(c.io.valid_in, Bool(true).litValue())
            val yest_hw = toD(peek(c.io.ybar).toInt)
    
            //Check the output.
            success = success && (yest_sw == yest_hw)
            val err = Math.abs(yest_sw - yest_hw)
            yest_hw should be (yest_sw +- tol)
            step(1)
            x += y
            x -= x(0)
            if (verbose > 0) println(f"dn: ${SoftMod.c}%d, y: $y%e, S/W: $yest_sw%e, H/W: $yest_hw%e, Error: $err%e")
        }
    
        success
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ false }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    "The KNLMSTimeSeries test" should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    it should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 7
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ false }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    it should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 7
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    it should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 2
    val m: Int = 7
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ false }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    it should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 2
    val m: Int = 7
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), fromD, (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_,32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    it should f"predict the time series: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[Flo](Flo(), neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = w-1
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivision, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 15
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = w-1
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivision, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = w-1
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivisionNew, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 15
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = w-1
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivisionNew, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 0
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = 6 // Use this for divLutLi.
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(epsilon, n, 1024, divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=divLutLi, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val p = 15
    val neg_gamma = -1.0
    val thresh = 0.5
    val epsilon = 0.01
    val eta = 0.1

    // Datatype parameters.
    val iL: Int = 15
    val w: Int = 32
    val divDelay = 6 // Use this for divLutLi.
    val expDelay = 5
    val (doReg, expReg, divReg) = KNLMS.estimateDoReg(n, m, p, divDelay, expDelay)
    val pdiv = CountReg.nreg(divReg)
    val pexp = CountReg.nreg(expReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 1024, expReg)
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(epsilon, n, 1024, divReg, _)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 3e-3
    it should f"predict the time series: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=divLutLi, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, tol=$tol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMSTimeSeriesWrapper[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, div, _+_, _-_, exp, _ > _, fromD, pexp=pexp, pdiv=pdiv))) {
                c => new KNLMSTimeSeriesWrapperTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
            }
    }}

}
