
import PKAF._
import Chisel._
import org.scalatest._
/** A test suite for the MaxNSuite module.
  *
  * TODO: Test using PsspFixed datatype.
  */
class MaxNSuite extends FlatSpec with Matchers {
    /** A helper class to test the MaxN Module.
      *
      * TODO: Test mulpitle iterations.
      */
    class MaxNTest[T <: Bits](c: MaxN[T], toD: BigInt => Double, fromD: Double => T, isTrace: Boolean = true) extends Tester(c, isTrace) {
        var res : Float = 0
        for (i <- 0 until c.n) {
            val in = rnd.nextFloat()
            poke(c.io.x(i), fromD(in).litValue())
            if (i == 0 || in > res)
                res = in
        }
        val cycles : Int = c.latency
        if (cycles == 1) {
            step(cycles)
        }
        else if (cycles > 1) {
            step(1)
            for (i <- 0 until c.n) {
                poke(c.io.x(i), fromD(0).litValue())
            }
            step(cycles - 1)
        }
        expect(c.io.res, fromD(res).litValue()) should be (true)
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 1
    val n = 2
    val doReg = Array.fill(1){ false }
    val max: (Flo, Flo) => Flo = FloMax.max
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    "The MaxN Module" should f"find the max of the vector in test $test%d: n=$n%d, T=Flo, latency=0" in {
        chiselMainTest(tutArgs, () => Module(new MaxN[Flo](Flo(), doReg, n, FloMax.max)))
            { c => new MaxNTest(c, toD, fromD, false) }
    }}

    {
    test += 1
    val n = 2
    val doReg = Array.fill(1){ true }
    val max: (Flo, Flo) => Flo = FloMax.max
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    it should f"find the max of the vector in test $test%d: n=$n%d, T=Flo, latency=1" in {
        chiselMainTest(tutArgs, () => Module(new MaxN[Flo](Flo(), doReg, n, FloMax.max)))
            { c => new MaxNTest(c, toD, fromD, false) }
    }}
}

