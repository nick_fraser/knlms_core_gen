
import PKAF._
import Chisel._
import org.scalatest._
import scala.collection.mutable.ArrayBuffer
import util.Random

/** A simple test suite for the KNLMS module.
  *
  * TODO: Test with more datatypes.
  */
class KnlmsSuite extends FlatSpec with Matchers {
    /** A tester class for KNLMS.
      *
      * TODO: Test over multiple iterations.
      */
    class KNLMSTest[T <: Bits](c: KNLMS[T], toD: BigInt => Double, fromD: Double => T, ttod: T => Double, neg_gamma: Double, thresh: Double, epsilon: Double, eta: Double, tol: Double,
                               seed: Int = 0, verbose: Int = 0, isTrace: Boolean = true) extends Tester(c, isTrace) {
        def ConnectInputs(x: Vec[T], y: T, valid: Boolean): Unit = {
            //Connect cin and y.
            poke(c.io.y, y.litValue())
            poke(c.io.valid_in, Bool(valid).litValue())
    
            //Connect x.
            for (i <- 0 until c.m) {
                poke(c.io.x(i), x(i).litValue())
            }
        }
    
        def FetchOutputs(): KNLMSOutVar = {
            //Get cin and y.
            val cin = Lit(peek(c.io.cout), log2Up(c.n) + 1){ UInt(width = log2Up(c.n) + 1) }
            val ybar = fromD(toD(peek(c.io.ybar).toInt))
    
            //Get dictionary.
            val dout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                for (j <- 0 until c.m) {
                    val index: Int = i*c.m + j
                    dout += fromD(toD(peek(c.io.Dout(index)).toInt))
                }
            }
    
            //Connect alpha.
            val aout = new ArrayBuffer[T]()
            for (i <- 0 until c.n) {
                aout += fromD(toD(peek(c.io.Aout(i)).toInt))
            }
            val outvars = new KNLMSOutVar(cin, ybar, Vec(dout), Vec(aout), c.n, c.m)
            outvars
        }
    
        def archSim(x: Vec[T], y: T, n: Int, m: Int): KNLMSOutVar = {
            //Convert inputs.
            val xd: Array[Double] = TtoDouble(x, ttod)
            val yd: Double = ttod(y)
    
            sim.train(xd, yd)
            val ybar = sim.ybar
    
            val out = new KNLMSOutVar(UInt(sim.c), fromD(ybar), TtoDouble.reverse(sim.d, fromD), TtoDouble.reverse(sim.a, fromD), n, m)
            out
        }
    
        //Helper classes.
        protected class KNLMSOutVar(cout: UInt, ybar: T, dout: Vec[T], aout: Vec[T], val n: Int, val m: Int) {
            val c = cout
            val y = ybar
            val d = dout
            val a = aout
    
            def prt(): Unit = {
                println("c = " + c.litValue().toInt)
                println("ybar = " + ttod(y))
                println("a = " + TtoDouble(a, ttod).mkString("{",", ","}"))
                val ds = TtoDouble(d, ttod)
                print("d = {")
                for (i <- 0 until n) {
                    print(ds.slice(i*m, (i+1)*m).mkString("{", ", ", "}"))
                    if (i != n-1) {
                        print(", ")
                    }
                }
                println("}")
            }

            def ===(that: KNLMSOutVar): Unit = {
                // Dictionary.
                for(i <- 0 until d.length) { 
                    val d1 = toD(d(i).litValue())
                    val d2 = toD(that.d(i).litValue())
                    d1 should be (d2 +- tol)
                }
                // Weights.
                for(i <- 0 until a.length) {
                    val a1 = toD(a(i).litValue())
                    val a2 = toD(that.a(i).litValue())
                    a1 should be (a2 +- tol)
                }
                val y1 = toD(y.litValue())
                val y2 = toD(that.y.litValue())
                y1 should be (y2 +- tol)
            }
        }
    
        // Hardware parameters.
        val n = c.n
        val m = c.m
        val delay = c.latency
    
        // Create the software simulator.
        val sim = if(c.singleDelay) { 
            new DKNLMSSoft(n, m, -neg_gamma, eta, epsilon, thresh, delay)
        } else {
            new MultiDelayKnlms(n, m, -neg_gamma, eta, epsilon, thresh, c.d1, c.d2, c.addMask)
        }
    
        // Test parameters.
        val trials = 20
        val rand = new Random(seed)
        val std = 1.0
        val w = Array.fill(m){ toD(fromD(std*rand.nextGaussian()).litValue()) }
        val noise = 0.01
    
        for (i <- 0 until c.latency+trials) {
            if (verbose > 0) println(f"====== CYCLE $i%d ======")
    
            val xd = Array.fill(m){ toD(fromD(std*rand.nextGaussian()).litValue()) }
            val x = Vec(xd.map(fromD))
            val y = fromD(noise*rand.nextGaussian + math.atan((xd, w).zipped.map(_*_).reduce(_+_)))
    
            // Connect inputs and fetch outputs.
            ConnectInputs(x, y, true)
            if (verbose > 0) println("=== H/W Output ===")
            val out = FetchOutputs()
            if (verbose > 0) out.prt()
    
            // Run the architecture simulation.
            if (verbose > 0) println("=== S/W Output ===")
            val sim_out = archSim(x, y, n, m)
            if (verbose > 0) sim_out.prt()
    
            // Test out and sim_out.
            out === sim_out

            step(1)
        }
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ false }

    // Datatype parameters - Flo.
    val pdiv: Int = 0
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-6
    "The KNLMS test" should f"learn the model: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[Flo](Flo(),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv))){
            c => new KNLMSTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - Flo.
    val pdiv: Int = 0
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-5
    it should f"learn the model: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[Flo](Flo(),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv))){
            c => new KNLMSTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - Flo.
    val pdiv: Int = 0
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-5
    it should f"learn the model: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, singleDelay=false" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[Flo](Flo(),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, singleDelay=false))){
            c => new KNLMSTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - Flo.
    val pdiv: Int = 0
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, -16, 0, 4096, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    val bitoD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val tol = 1e-5
    it should f"learn the model: test=$test%d, T=Flo, exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d, singleDelay=false, addMask=true" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[Flo](Flo(),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, singleDelay=false, addMask=true))){
            c => new KNLMSTest(c, bitoD, fromD, _.floLitValue.toDouble, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ false }

    // Datatype parameters - PsspFixed.
    val pdiv: Int = 0
    val iL: Int = 15
    val w: Int = 32
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-16, 0, 1024, 0)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 1e-3
    it should f"learn the model: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=/, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv))){
            c => new KNLMSTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - PsspFixed.
    val pdiv: Int = 31
    val pexp: Int = 5
    val iL: Int = 15
    val w: Int = 32
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-16, 0, 1024, pexp)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val tol = 1e-5
    it should f"learn the model: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivision, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, pexp=pexp))){
            c => new KNLMSTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - PsspFixed.
    val pdiv: Int = 31
    val pexp: Int = 5
    val iL: Int = 15
    val w: Int = 32
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-16, 0, 4096, pexp)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val tol = 1e-4
    it should f"learn the model: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivision, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, singleDelay=false" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, pexp=pexp, singleDelay=false))){
            c => new KNLMSTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val n: Int = 8
    val m: Int = 2
    val neg_gamma = -0.5
    val thresh = 0.5
    val epsilon = 0.0001
    val eta = 0.1
    val doReg = Array.fill(KNLMS.getRegLength(n, m)){ true }

    // Datatype parameters - PsspFixed.
    val pdiv: Int = 31
    val pexp: Int = 5
    val iL: Int = 15
    val w: Int = 32
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val exp: PsspFixed => PsspFixed = _.expLutLi(-16, 0, 4096, pexp)
    val bitoD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val div: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val tol = 3e-2
    it should f"learn the model: test=$test%d, T=PsspFixed(w=$w%d, iL=$iL%d), exp=LutLi, div=manualDivision, n=$n%d, m=$m%d, latency=${CountReg.nreg(doReg)+pdiv+pexp}%d, singleDelay=false, addMask=true" in {
    chiselMainTest(tutArgs, () => Module(
        new KNLMS[PsspFixed](PsspFixed(width=w,iL=iL,pdiv=pdiv),
            neg_gamma, thresh, epsilon, eta, doReg, n, m,
            _*_, _/_, _+_, _-_, exp, _ > _, fromD, pdiv=pdiv, pexp=pexp, singleDelay=false, addMask=true))){
            c => new KNLMSTest(c, bitoD, fromD, PsspFixed.toD, neg_gamma, thresh, epsilon, eta, tol, verbose=0, isTrace=false)
        }
    }}

}
