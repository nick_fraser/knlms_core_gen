
import PKAF._
import Chisel._
import org.scalatest._

/** A test suite for the DotProduct module.
  *
  * TODO: Test more bit widths.
  * TODO: Test more vector lengths.
  */
class ExpEvalSuite extends FlatSpec with Matchers {
    /** A helper class to test ExpEval. */
    class ExpModule[T <: Data](dtype: T, exp: T => T) extends Module {
        val io = new Bundle {
            val x = dtype.clone.asInput
            val ex = dtype.clone.asOutput
        }
        io.ex := exp(io.x)
    }
    
    /** A helper class which extends for implementing the tester.
      *
      * TODO: Test random inputs.
      * TODO: Test multiple iterations.
      */
    class ExpModuleTest[T <: Bits](c: ExpModule[T], toD: BigInt => Double, fromD: Double => T, tol: Double, isTrace: Boolean = true) extends Tester(c, isTrace) {
        val x = -0.5
        var ex: Double = 0
        poke(c.io.x, fromD(x).litValue())
        ex = math.exp(x)
        val c_io_ex = toD(peek(c.io.ex).toInt)
        //println("ExpHW: " + c_io_ex + ", " + "ExpSW: " + ex + ", Error: " + (ex-c_io_ex))
        c_io_ex should be (ex +- tol)
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    {
    test += 0
    val low = -4.0
    val hi = 0.0
    val entries = 1024
    val tol = 5e-4
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUT[Flo](_, low, hi, entries, _*_, _+_, (_).toSInt(), fromD)
    "The ExpEval object" should f"approximately calculate exponential in test $test%d: ExpType=LUT, T=Flo, low=$low%e, hi=$hi%e, entries=$entries%d, tol=$tol%e" in {
        chiselMainTest(tutArgs, () => Module(new ExpModule[Flo](Flo(), exp))){ c => new ExpModuleTest(c, toD, fromD, tol, false) }
    }}

    {
    test += 0
    val low = -4.0
    val hi = 0.0
    val entries = 1024
    val tol = 5e-4
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTRd[Flo](_, low, hi, entries, _*_, _+_, (_).toSInt(), fromD, _ >= _, (f: Flo) => f - Floor(f))
    it should f"approximately calculate exponential in test $test%d: ExpType=LUTrnd, T=Flo, low=$low%e, hi=$hi%e, entries=$entries%d, tol=$tol%e" in {
        chiselMainTest(tutArgs, () => Module(new ExpModule[Flo](Flo(), exp))){ c => new ExpModuleTest(c, toD, fromD, tol, false) }
    }}

    {
    test += 0
    val low = -4.0
    val hi = 0.0
    val entries = 1024
    val tol = 5e-7
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUTli[Flo](_, low, hi, entries, _*_, _+_, _-_, (_).toSInt(), Flo(_), (f: Flo) => f - Floor(f))
    it should f"approximately calculate exponential in test $test%d: ExpType=LUTli, T=Flo, low=$low%e, hi=$hi%e, entries=$entries%d, tol=$tol%e" in {
        chiselMainTest(tutArgs, () => Module(new ExpModule[Flo](Flo(), exp))){ c => new ExpModuleTest(c, toD, fromD, tol, false) }
    }}

}

