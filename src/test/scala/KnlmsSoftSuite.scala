
import PKAF._
import util.Random
import org.scalatest._

/** A test suite for the software versions of KNLMS.
  *
  * TODO: Add tests for KNLMSSoft, DKNLMSSoft, MultiDelayKnlms and MultiDelayKnlmsWithCorrections.
  */
class KnlmsSoftSuite extends FlatSpec with Matchers {
    def checkKaf[T <: PipelinedKernelAdaptiveFilter, R <: PipelinedKernelAdaptiveFilter](test: T, ref: R, tol: Double, verbose: Int = 0): Unit = {
        if (verbose > 0) println(f"test.a.length=${test.a.length}%d, ref.a.length=${ref.a.length}%d")
        test.a.length should be (ref.a.length)
        if (verbose > 0) println(f"test.d.length=${test.d.length}%d, ref.d.length=${ref.d.length}%d")
        test.d.length should be (ref.d.length)
        if (verbose > 0) println(f"test.c=${test.c}%d, ref.c=${ref.c}%d")
        test.c should be (ref.c)
        if (verbose > 0) println(f"test.ybar=${test.ybar}%f, ref.ybar=${ref.ybar}%f\n")
        test.ybar should be (ref.ybar +- tol)
        if (verbose > 0) println(f"test.a=" + test.a.mkString("{",", ","}"))
        if (verbose > 0) println(f"ref.a=" + ref.a.mkString("{",", ","}") + "\n")
        val m: Int = test.d.length/test.a.length
        for (i <- 0 until test.a.length) test.a(i) should be (ref.a(i) +- tol)
        if (verbose > 0) println(f"test.d=" + test.d.grouped(m).map(_.mkString("{",", ","}")).mkString("{",",\n","}\n"))
        if (verbose > 0) println(f"ref.d=" + ref.d.grouped(m).map(_.mkString("{",", ","}")).mkString("{",",\n","}\n"))
        for (i <- 0 until test.d.length) test.d(i) should be (ref.d(i) +- tol)
    }
    def printFifoState(kaf: MultiDelayKnlmsWithCorrections, dFifo: Boolean = true, aFifo: Boolean = true, addFifo: Boolean = true, sFifo: Boolean = true): Unit = {
        if (dFifo) {
            println("dictFifo (x, y, k, kk, valid)")
            kaf.dictFifo.foreach{ x => println(x.productIterator.map{y => y match {
                        case a: Array[_] => a.mkString("{",", ","}")
                        case _ => y.toString
                    }
                }.mkString("{",", ","}"))
            }
            println()
        }
        if (aFifo) {
            println("alphaFifo (y, ybar, k, vKtk)")
            kaf.alphaFifo.foreach{ x => println(x.productIterator.map{y => y match {
                        case a: Array[_] => a.mkString("{",", ","}")
                        case _ => y.toString
                    }
                }.mkString("{",", ","}"))
            }
            println()
        }
        if (addFifo) {
            println("addFifo(add)")
            println(kaf.addFifo.mkString("{",", ","}"))
            println()
        }
        if (sFifo) {
            println("stepFifo(step)")
            println(kaf.stepFifo.mkString("{",", ","}"))
            println()
        }
    }

    var test = 0

    { // A test to check that KNLMSSoft and MultiDelayKnlmsWithCorrections behave the same for a simple system identification problem.
    // Test setup.
    test += 1
    val tol = 1e-16
    val trials = 20

    // KAF setup.
    val n = 10
    val m = 5
    val gamma = 0.5
    val thresh = 0.5
    val eta = 0.01
    val epsilon = 0.001
    val alphaDelay = 0
    val dictDelay = 0
    val refDelay = 0
    val k1 = new DKNLMSSoft(n,m,gamma,eta,epsilon,thresh,refDelay)
    val k2 = new MultiDelayKnlmsWithCorrections(n,m,gamma,eta,epsilon,thresh,dictDelay,alphaDelay)

    // Unknown system setup.
    val seed = 0
    val r = new Random(0)
    val range = 1.0
    val noise = 0.001
    val w = Array.fill(m){ range*r.nextGaussian() }

    // Train the models.
    for (i <- 0 until trials) {
        val x = Array.fill(m){ range*r.nextGaussian() }
        val y = (x,w).zipped.map(_*_).reduce(_+_) + noise*r.nextGaussian()
        k1.train(x,y)
        k2.train(x,y)
    }

    // Test the results.
    "The KNLMS Classes" should f"find the same model: test=$test%d, DKNLMS(d=$refDelay%d), DKNLMS+CT(dd=$dictDelay%d,da=$alphaDelay%d), tol=$tol%.2e." in {
        checkKaf(k1, k2, tol)
    }}

    { // A test to check that KNLMSSoft and MultiDelayKnlmsWithCorrections behave the same for a simple system identification problem.
    // Test setup.
    test += 1
    val tol = 1e-16
    val trials = 20

    // KAF setup.
    val n = 10
    val m = 5
    val gamma = 0.4
    val thresh = 0.5
    val eta = 0.01
    val epsilon = 0.001
    val alphaDelay = 4
    val dictDelay = 4
    val refDelay = 0
    val k1 = new DKNLMSSoft(n,m,gamma,eta,epsilon,thresh,refDelay)
    val k2 = new MultiDelayKnlmsWithCorrections(n,m,gamma,eta,epsilon,thresh,dictDelay,alphaDelay)

    // Unknown system setup.
    val seed = 0
    val r = new Random(0)
    val range = 1.0
    val noise = 0.001
    val w = Array.fill(m){ range*r.nextGaussian() }

    // Train the models.
    for (i <- 0 until trials) {
        val x = Array.fill(m){ range*r.nextGaussian() }
        val y = (x,w).zipped.map(_*_).reduce(_+_) + noise*r.nextGaussian()
        k1.train(x,y)
        k2.train(x,y)
        //printFifoState(k2)
    }
    for (i <- 0 until (dictDelay + alphaDelay)) {
        k2.train(Array.fill(m){ 0.0 }, 0.0)
        //printFifoState(k2)
    }

    // Test the results.
    it should f"find the same model after some delay: test=$test%d, DKNLMS(d=$refDelay%d), DKNLMS+CT(dd=$dictDelay%d,da=$alphaDelay%d), tol=$tol%.2e." in {
        checkKaf(k2, k1, tol, 0)
    }}

    { // A test to check that KNLMSSoft and MultiDelayKnlmsWithCorrections behave the same for a simple system identification problem.
    // Test setup.
    test += 1
    val tol = 1e-16
    val trials = 20

    // KAF setup.
    val n = 10
    val m = 5
    val gamma = 0.4
    val thresh = 0.5
    val eta = 0.01
    val epsilon = 0.001
    val alphaDelay = 5
    val dictDelay = 3
    val refDelay = 0
    val k1 = new DKNLMSSoft(n,m,gamma,eta,epsilon,thresh,refDelay)
    val k2 = new MultiDelayKnlmsWithCorrections(n,m,gamma,eta,epsilon,thresh,dictDelay,alphaDelay)

    // Unknown system setup.
    val seed = 0
    val r = new Random(0)
    val range = 1.0
    val noise = 0.001
    val w = Array.fill(m){ range*r.nextGaussian() }

    // Train the models.
    for (i <- 0 until trials) {
        val x = Array.fill(m){ range*r.nextGaussian() }
        val y = (x,w).zipped.map(_*_).reduce(_+_) + noise*r.nextGaussian()
        k1.train(x,y)
        k2.train(x,y)
        //printFifoState(k2)
    }
    for (i <- 0 until (dictDelay + alphaDelay)) {
        k2.train(Array.fill(m){ 0.0 }, 0.0)
        //printFifoState(k2)
    }

    // Test the results.
    it should f"find the same model after some delay: test=$test%d, DKNLMS(d=$refDelay%d), DKNLMS+CT(dd=$dictDelay%d,da=$alphaDelay%d), tol=$tol%.2e." in {
        checkKaf(k2, k1, tol, 0)
    }}

    { // A test to check that KNLMSSoft and MultiDelayKnlmsWithCorrections behave the same for a simple system identification problem.
    // Test setup.
    test += 1
    val tol = 1e-14
    val trials = 2000

    // KAF setup.
    val n = 10
    val m = 5
    val gamma = 0.4
    val thresh = 0.5
    val eta = 0.01
    val epsilon = 0.001
    val alphaDelay = 11
    val dictDelay = 5
    val refDelay = 0
    val k1 = new DKNLMSSoft(n,m,gamma,eta,epsilon,thresh,refDelay)
    val k2 = new MultiDelayKnlmsWithCorrections(n,m,gamma,eta,epsilon,thresh,dictDelay,alphaDelay)

    // Unknown system setup.
    val seed = 0
    val r = new Random(0)
    val range = 1.0
    val noise = 0.001
    val w = Array.fill(m){ range*r.nextGaussian() }

    // Train the models.
    for (i <- 0 until trials) {
        val x = Array.fill(m){ range*r.nextGaussian() }
        val y = (x,w).zipped.map(_*_).reduce(_+_) + noise*r.nextGaussian()
        k1.train(x,y)
        k2.train(x,y)
        //printFifoState(k2)
    }
    for (i <- 0 until (dictDelay + alphaDelay)) {
        k2.train(Array.fill(m){ 0.0 }, 0.0)
        //printFifoState(k2)
    }

    // Test the results.
    it should f"find the same model after some delay: test=$test%d, DKNLMS(d=$refDelay%d), DKNLMS+CT(dd=$dictDelay%d,da=$alphaDelay%d), tol=$tol%.2e." in {
        checkKaf(k2, k1, tol, 0)
    }}

}

