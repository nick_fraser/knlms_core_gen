
import PKAF._
import Chisel._
import org.scalatest._
import util.Random

/** A test suite for the PsspFixed datatype.
  *
  * TODO: Make a todo list for the test suite.
  */
class PsspFixedSuite extends FlatSpec with Matchers {
    class OpModTest[T <: Bits](c: OpMod[T], f: (Double, Double) => Double, toD: BigInt => Double, fromD: Double => T, tol: Double, relTol: Double,
                               numTests: Int = 1, low: Double = -1, high: Double = 1, seed: Int = 0,
                               avoidZero: Boolean = false, verbose: Int = 2, isTrace: Boolean = true) extends Tester(c, isTrace) {
        val rand = new Random(seed)
        var maxErr: Double = 0.0
        var maxRelErr: Double = 0.0
        for (i <- 0 until numTests) {
            val x = nextDouble(rand, toD, fromD, low, high, avoidZero)
            val y = nextDouble(rand, toD, fromD, low, high, avoidZero)
            //val res = f(x, y)
            val res = toD(fromD(f(x, y)).litValue())
            poke(c.io.x, fromD(x).litValue())
            poke(c.io.y, fromD(y).litValue())
            val cycles = c.latency
            if (cycles == 1) {
                step(cycles)
            }
            else if (cycles > 1) {
                step(1)
                poke(c.io.x, 0)
                poke(c.io.y, 0)
                step(cycles - 1)
            }
            val hwres = toD(peek(c.io.res))
            val err = res - hwres
            val relErr = err/res
            maxErr = if (err.abs > maxErr) err.abs else maxErr
            maxRelErr = if (relErr.abs > maxRelErr) relErr.abs else maxRelErr
            if (verbose > 0) println(f"f($x%e, $y%e) S/W: $res%e, H/W: $hwres%e, Error: $err%e, RelErr: $relErr%e")
            hwres should be (res +- tol)
            relErr.abs should be < relTol
        }
        if (verbose == 2) println(f"Maximum error: $maxErr%e, Maximum Relative Error: $maxRelErr%e")
    }
    
    class FunModTest[T <: Bits](c: FunMod[T], f: Double => Double, toD: BigInt => Double, fromD: Double => T, tol: Double, relTol: Double,
                                numTests: Int = 1, low: Double = -1, high: Double = 1, seed: Int = 0,
                                avoidZero: Boolean = false, verbose: Int = 2, isTrace: Boolean = true) extends Tester(c, isTrace) {
        val rand = new Random(seed)
        var maxErr: Double = 0.0
        var maxRelErr: Double = 0.0
        for (i <- 0 until numTests) {
            val x = nextDouble(rand, toD, fromD, low, high, avoidZero)
            //val res = f(x)
            val res = toD(fromD(f(x)).litValue())
            poke(c.io.x, fromD(x).litValue())
            val cycles = c.latency
            if (cycles == 1) {
                step(cycles)
            }
            else if (cycles > 1) {
                step(1)
                poke(c.io.x, 0)
                step(cycles - 1)
            }
            val hwres = toD(peek(c.io.res))
            val err = res - hwres
            val relErr = err/res
            maxErr = if (err.abs > maxErr) err.abs else maxErr
            maxRelErr = if (relErr.abs > maxRelErr) relErr.abs else maxRelErr
            if (verbose > 0) println(f"f($x%e) S/W: $res%e, H/W: $hwres%e, Error: $err%e, RelErr: $relErr%e")
            hwres should be (res +- tol)
            relErr.abs should be < relTol
        }
        if (verbose == 2) println(f"Maximum error: $maxErr%e, Maximum Relative Error: $maxRelErr%e")
    }

    def nextDouble[T <: Bits](rand: Random, toD: BigInt => Double, fromD: Double => T, low: Double, high: Double, avoidZero: Boolean): Double = {
        val range: Double = high - low
        val mean: Double = (high + low)/2
        var res: Double = toD(fromD(range*(rand.nextDouble() - 0.5) + mean).litValue())
        //var res: Double = range*(rand.nextDouble() - 0.5) + mean
        if (avoidZero) {
            while (res == 0.0) {
                //res = range*(rand.nextDouble() - 0.5) + mean
                res = toD(fromD(range*(rand.nextDouble() - 0.5) + mean).litValue())
            }
        }
        res
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    /****** FUNCTION TESTS ********/
    {
    test += 1
    val pdiv: Int = 0
    val pfun = 0
    val w = 32
    val iL = 16
    val hwfun: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64)
    val swfun: Double => Double = math.exp
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-4
    val trials = 10
    val low = -4.0
    val high = 0.0
    "The function test" should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pfun: Int = 0
    val pdiv = 0
    val w = 18
    val iL = 5
    val hwfun: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64)
    val swfun: Double => Double = math.exp
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, 0, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-3
    val trials = 10
    val low = -4.0
    val high = 0.0
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(5){ true }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, doReg)
    val swfun: Double => Double = math.exp
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, 0, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-4
    val trials = 10
    val low = -4.0
    val high = 0.0
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(5){ true }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.expLutLi(-4, 0, 64, doReg)
    val swfun: Double => Double = math.exp
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, 0, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-3
    val trials = 10
    val low = -4.0
    val high = 0.0
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(3){ false }
    val pfun = CountReg.nreg(doReg)
    val low = -3.96875
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 128, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-2
    val relTol = 2e-2
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(3){ true }
    val pfun = CountReg.nreg(doReg)
    val low = -3.96875
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 128, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-2
    val relTol = 2e-2
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(3){ false }
    val pfun = CountReg.nreg(doReg)
    val low = -3.99609375
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 1024, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-3
    val relTol = 2e-3
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(3){ true }
    val pfun = CountReg.nreg(doReg)
    val low = -3.99609375
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 1024, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-3
    val relTol = 2e-3
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(3){ false }
    val pfun = CountReg.nreg(doReg)
    val low = -3.96875
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 128, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-2
    val relTol = 2e-2
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(3){ true }
    val pfun = CountReg.nreg(doReg)
    val low = -3.96875
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 128, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-2
    val relTol = 2e-2
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(3){ false }
    val pfun = CountReg.nreg(doReg)
    val low = -3.99609375
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 1024, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-3
    val relTol = 2e-3
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(3){ true }
    val pfun = CountReg.nreg(doReg)
    val low = -3.99609375
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, 1024, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 2e-3
    val relTol = 2e-3
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 32
    val iL = 16
    val doReg = Array.fill(3){ false }
    val pfun = CountReg.nreg(doReg)
    val lutSize = 4096
    val low = -4 + math.pow(2,-10)
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, lutSize, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-4
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val w = 18
    val iL = 5
    val doReg = Array.fill(3){ true }
    val pfun = CountReg.nreg(doReg)
    val lutSize = 2048
    val low = -4 + math.pow(2,-9)
    val high = 0.0
    val swfun: Double => Double = math.exp
    val hwfun: PsspFixed => PsspFixed = _.funLutPow2(low, high, swfun, lutSize, doReg)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val tol = 5e-4
    val relTol = 5e-3
    val trials = 10
    it should f"calculate exp: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, exp=lutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    /******** POLY TESTS *********/
    def polyEvalHorners(x: Double, coeffs: Array[Double]): Double = {
        val order = coeffs.length-1
        var res = coeffs(order)
        for (i <- 0 until order reverse) {
            res = coeffs(i) + res*x
        }
        res
    }

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 16
    val order = 4
    val doReg = Array.fill(2*(order)){ false }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val coeffs = Array.fill(order+1){ toD(fromD(r.nextGaussian()).litValue()) }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.polyEvalHorners(coeffs, doReg)
    val swfun: Double => Double = polyEvalHorners(_, coeffs)
    val tol = 1e-3
    val relTol = 1e-5
    val trials = 10
    val low = -4.0
    val high = 4.0
    it should f"calculate poly(horners): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 16
    val order = 4
    val doReg = Array.fill(2*(order)){ true }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val coeffs = Array.fill(order+1){ toD(fromD(r.nextGaussian()).litValue()) }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.polyEvalHorners(coeffs, doReg)
    val swfun: Double => Double = polyEvalHorners(_, coeffs)
    val tol = 1e-3
    val relTol = 1e-5
    val trials = 10
    val low = -4.0
    val high = 4.0
    it should f"calculate poly(horners): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 16
    val order = 4
    val doReg = Array.fill(2*log2Up(order+1)){ false }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val coeffs = Array.fill(order+1){ toD(fromD(r.nextGaussian()).litValue()) }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.polyEvalEstrins(coeffs, doReg)
    val swfun: Double => Double = polyEvalHorners(_, coeffs)
    val tol = 1e-3
    val relTol = 1e-4
    val trials = 10
    val low = -4.0
    val high = 4.0
    it should f"calculate poly(estrins): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 16
    val order = 4
    val doReg = Array.fill(2*log2Up(order+1)){ true }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val coeffs = Array.fill(order+1){ toD(fromD(r.nextGaussian()).litValue()) }
    val pfun = CountReg.nreg(doReg)
    val hwfun: PsspFixed => PsspFixed = _.polyEvalEstrins(coeffs, doReg)
    val swfun: Double => Double = polyEvalHorners(_, coeffs)
    val tol = 1e-3
    val relTol = 1e-4
    val trials = 10
    val low = -4.0
    val high = 4.0
    it should f"calculate poly(estrins): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, trials=$trials, range($low%.2e, $high%.2e), latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ false }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-4
    val relTol = 3e-4
    val trials = 10
    val low = -1.0
    val high = -0.0
    val splits = 1
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ true }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-4
    val relTol = 3e-4
    val trials = 10
    val low = -1.0
    val high = -0.0
    val splits = 1
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ false }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-5
    val relTol = 3e-5
    val trials = 10
    val low = -1.0
    val high = -0.0
    val splits = 2
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ true }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-5
    val relTol = 3e-5
    val trials = 10
    val low = -1.0
    val high = -0.0
    val splits = 2
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ false }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-5
    val relTol = 3e-5
    val trials = 10
    val low = -2.0
    val high = -0.0
    val splits = 4
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    {
    test += 1
    val pdiv = 0
    val w = 32
    val iL = 5
    val order = 3
    val doReg = Array.fill(2*log2Up(order+1)+1){ true }
    val seed = 0
    val r = new Random(seed)
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val pfun = CountReg.nreg(doReg)
    val swfun: Double => Double = Math.exp
    val tol = 2e-5
    val relTol = 3e-5
    val trials = 10
    val low = -2.0
    val high = -0.0
    val splits = 4
    val hwfun: PsspFixed => PsspFixed = _.funLutInterpRemez(low, high, swfun, swfun, order, doReg, splits=splits)
    it should f"calculate remez(estrins-multi): test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), f=exp, seed=0, trials=$trials, range($low%.2e, $high%.2e), splits=$splits%d, latency=${pfun+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new FunMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwfun, pfun))) {
            c => new FunModTest[PsspFixed]( c, swfun, toD, fromD, tol, relTol,
                                            numTests=trials, low=low, high=high, verbose=0, isTrace=false)
        }
    }}

    /***** OPERATION TESTS *****/

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _/_
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-4
    val relTol = 1e-4
    val trials = 10
    val low = -1
    val high = 1
    "The operation test" should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=/, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _/_
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-3
    val relTol = 1e-3
    val trials = 10
    val low = -1
    val high = 1
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=/, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 17
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-4
    val relTol = 1e-4
    val trials = 10
    val low = -1
    val high = 1
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=manualDivision, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 17
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivision(_)
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-3
    val relTol = 1e-3
    val trials = 10
    val low = -1
    val high = 1
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=manualDivision, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 17
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(_)
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-4
    val relTol = 1e-4
    val trials = 10
    val low = -1
    val high = 1
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=manualDivisionNew, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 17
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.manualDivisionNew(_)
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-3
    val relTol = 1e-3
    val trials = 10
    val low = -1
    val high = 1
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=manualDivisionNew, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 6
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-3
    val relTol = 1e-3
    val trials = 10
    val low = 0.1
    val high = 15.9
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(low, high, 1024, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 6
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 1e-1
    val relTol = 1e-1
    val trials = 10
    val low = 0.1
    val high = 15.9
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutLi(low, high, 1024, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutLi, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 4
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 5e-2
    val relTol = 5e-2
    val trials = 10
    val lutSize = 128
    val low = math.pow(2,-3)
    val high = low + math.pow(2,-4)*(lutSize-1)
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutPow2(low, high, lutSize, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutPow2, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 4
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 5e-2
    val relTol = 1e-2
    val trials = 10
    val lutSize = 1024
    val low = math.pow(2,-3)
    val high = low + math.pow(2,-6)*(lutSize-1)
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutPow2(low, high, lutSize, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutPow2, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 4
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 5e-2
    val relTol = 5e-2
    val trials = 10
    val lutSize = 128
    val low = math.pow(2,-3)
    val high = low + math.pow(2,-4)*(lutSize-1)
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutPow2(low, high, lutSize, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutPow2, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 4
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _/_
    val tol = 5e-2
    val relTol = 1e-2
    val trials = 10
    val lutSize = 1024
    val low = math.pow(2,-3)
    val high = low + math.pow(2,-7)*(lutSize-1)
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _.divLutPow2(low, high, lutSize, _)
    it should f"calculate /: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=divLutPow2, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=true)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _*_
    val tol = 1e-4
    val relTol = 1e-3
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _*_
    it should f"calculate *: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=*, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _*_
    val tol = 1e-3
    val relTol = 1e-2
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _*_
    it should f"calculate *: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=*, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _+_
    val tol = 1e-8
    val relTol = 1e-8
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _+_
    it should f"calculate +: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=+, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _+_
    val tol = 1e-8
    val relTol = 1e-8
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _+_
    it should f"calculate +: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=+, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 32
    val iL = 16
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _-_
    val tol = 1e-8
    val relTol = 1e-8
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _-_
    it should f"calculate -: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=-, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

    {
    test += 1
    val pdiv: Int = 0
    val pop = pdiv
    val w = 18
    val iL = 5
    val fromD: Double => PsspFixed = PsspFixed(_, w, iL, 0, 0, 0, pdiv, 0)
    val toD: BigInt => Double = PsspFixed.toD(_, PsspFixed(width=w, iL=iL, pdiv=pdiv))
    val swop: (Double, Double) => Double = _-_
    val tol = 1e-8
    val relTol = 1e-8
    val trials = 10
    val low = -1
    val high = 1
    val hwop: (PsspFixed, PsspFixed) => PsspFixed = _-_
    it should f"calculate -: test=$test%d, T=PsspFixed(width=$w%d, iL=$iL%d), seed=0, op=-, trials=$trials, range($low%.2e, $high%.2e), latency=${pop+2}%d, tol=$tol%.2e, relTol=$relTol%.2e" in {
    chiselMainTest(tutArgs, () => Module(
        new OpMod[PsspFixed](PsspFixed(width=w, iL=iL, pdiv=pdiv), 1, 1, hwop, pop))) {
            c => new OpModTest[PsspFixed](c, swop, toD, fromD, tol, relTol,
            numTests=trials, low=low, high=high, verbose=0, isTrace=false, avoidZero=false)
        }
    }}

/*
// "PsspFixed" =>
    //val wl = 18
    val fl = 15
    val il = 6
    val wl = fl + il
    val ops = Array[(PsspFixed, PsspFixed) => PsspFixed](_+_, _-_, _*_, _/_)
    val swops = Array[(Double, Double) => Double](_+_, _-_, _*_, _/_)
    val strops = Array[String]("+", "-", "*", "/")
    //val ops = Array[(PsspFixed, PsspFixed) => PsspFixed](_/_)
    //val swops = Array[(Double, Double) => Double](_/_)
    //val strops = Array[String]("/")
    println(f"Testing PsspFixed using w=$wl%d, iL=$il%d.")
    for (i <- 0 until ops.length) {
        println(f"""Testing operator: \"${strops(i)}%s\"""") //"Vim doesn't understand that this is a COMMENT.
        chiselMainTest(tutArgs, () => Module(
            new OpMod[PsspFixed](PsspFixed(width=wl, iL=il), 1, 1, ops(i)))) {
                c => new OpModTest[PsspFixed](c, swops(i), 
                    PsspFixed.toD(_, PsspFixed(width=wl, iL=il)), PsspFixed(_, wl, il), 1000, low= -0.5, high=0.5, isTrace=false, avoidZero=true)
            }
    }
*/

}
