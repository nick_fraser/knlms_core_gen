
import PKAF._
import Chisel._
import org.scalatest._

/** A test suite for the DotProduct module.
  *
  * TODO: Test using PsspFixed type.
  * TODO: Test more bit widths.
  * TODO: Test more vector lengths.
  */
class ExpKernelSuite extends FlatSpec with Matchers {
    /** A helper class which extends for implementing the tester.
      *
      * TODO: Test random vectors.
      * TODO: Test multiple iterations.
      */
    class ExpKernelTest[T <: Bits](c: ExpKernel[T], toD: BigInt => Double, fromD: Double => T, neg_gamma: Double, tol: Double, isTrace: Boolean = true) extends Tester(c, isTrace) {
        val x = Array.fill(c.n){1.0}
        val y = Array.fill(c.n){0.5}
        var res : Double = 0
        for (i <- 0 until c.n) {
            poke(c.io.x(i), fromD(x(i)).litValue())
            poke(c.io.y(i), fromD(y(i)).litValue())
        }
        val cycles : Int = c.latency
        if (cycles == 1) {
            step(cycles)
        }
        else if (cycles > 1) {
            step(1)
            for (i <- 0 until c.n) {
                poke(c.io.x(i), fromD(0).litValue())
                poke(c.io.y(i), fromD(0).litValue())
            }
            //step(cycles - 1)
            for (i <- 0 until cycles-1) {
                peek(c.io.res)
                step(1)
            }
        }
    
        //Calculate res.
        res = math.exp((((x, y).zipped.map(_ - _).map((f: Double) => f*f).reduce(_ + _)) * neg_gamma))
    
        //expect(c.io.res, fromD(res).litValue())
        val c_io_res = toD(peek(c.io.res).toInt)
        //Output fails.
        //println("ExpHW: " + c_io_res + ", " + "ExpSW: " + res + ", Error: " + (res-c_io_res))
        c_io_res should be (res +- tol)
    }

    val tutArgs = Array(
        "--backend",
        "c",
        "--targetDir",
        "./emulator",
        "--compile",
        "--test",
        "--genHarness"
    )
    var test = 0

    { // Datatype specfications.
    test += 1
    val n = 2
    val doReg = Array.fill(5){ false }
    val entries = 1024
    val low = -4.0
    val hi = 0.0
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUT[Flo](_, low, hi, entries, _*_, _+_, (_).toSInt(), Flo(_))
    val gamma = 0.5
    val tol = 5e-4
    "ExpKernelSuite" should f"pass test $test%d: n=$n%d, T=Flo, latency=0" in {
        chiselMainTest(tutArgs, () => Module(new ExpKernel[Flo](Flo(), -gamma, fromD, doReg, n, _*_, _+_, _-_, exp)))
            { c => new ExpKernelTest(c, toD, fromD, -gamma, tol, isTrace=false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 2
    val doReg = Array.fill(5){ true }
    val entries = 1024
    val low = -4.0
    val hi = 0.0
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUT[Flo](_, low, hi, entries, _*_, _+_, (_).toSInt(), Flo(_))
    val gamma = 0.5
    val tol = 5e-4
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=5" in {
        chiselMainTest(tutArgs, () => Module(new ExpKernel[Flo](Flo(), -gamma, fromD, doReg, n, _*_, _+_, _-_, exp)))
            { c => new ExpKernelTest(c, toD, fromD, -gamma, tol, isTrace=false) }
    }}

    { // Datatype specfications.
    test += 1
    val n = 2
    val doReg = Array(true, false, true, false, true)
    val entries = 1024
    val low = -4.0
    val hi = 0.0
    val toD: BigInt => Double = Lit(_, 32){ Flo() }.floLitValue.toDouble
    val fromD: Double => Flo = Flo(_)
    val exp: Flo => Flo = ExpEval.ExpLUT[Flo](_, low, hi, entries, _*_, _+_, (_).toSInt(), Flo(_))
    val gamma = 0.5
    val tol = 5e-4
    it should f"pass test $test%d: n=$n%d, T=Flo, latency=3" in {
        chiselMainTest(tutArgs, () => Module(new ExpKernel[Flo](Flo(), -gamma, fromD, doReg, n, _*_, _+_, _-_, exp)))
            { c => new ExpKernelTest(c, toD, fromD, -gamma, tol, isTrace=false) }
    }}

}

