# KNLMS Core Generator in Chisel

This repository contains a set of library components to generate architecture to implement the Kernel Normalized Least Mean Squares (KNLMS) algorithm.
Furthermore, this repository also contains information to reproduce various performance numbers from a paper currently under review at ACM TRETS.

## Installation

If you would like to use these library components in your own project, install it as follows:

1. Install [remez_scala](https://github.com/nickfraser/remez-scala)
2. Clone this repository
3. Run `sbt test`, to confirm all of the tests pass
4. Run `sbt publishLocal+` to install

Using these library components in your project can then be done by adding an `.sbt` similar to [this one](https://github.com/nickfraser/rosetta/blob/knlms/knlms_core_gen-dependent.sbt).

## Reproducing FPGA Performance Numbers

Follow the instructions at [this repository](https://github.com/nickfraser/rosetta).

## Reproducing CPU Performance Numbers

Follow the instructions at [this repository](https://bitbucket.org/nick_fraser/libkaf).

## Reproducing FPGA Synthesis Results

You must have `vivado` added to your path for this section.

Reproduce the synthesis results as follows:

1. Edit [src/main/scala/Main.scala](https://bitbucket.org/nick_fraser/knlms_core_gen/src/e7e6561a4e4db2d59a96fe1b12bbd3e08ab1e62d/src/main/scala/Main.scala#lines-253:255) to cycle through the permutations (dictionary size, feature size, pipeline length) that you wish to test.
2. Run `sbt "run KnlmsTesterVivado --backend v --targetDir ./verilog --genHarness"`
3. The results are stored in `output/vivado_designs.csv`

Note, the runtime may take several weeks if you select a large number of permutations.

## Reproducing Accuracy Numbers

Accuracy numbers can be generated as follows:

1. Edit [src/main/scala/Main.scala](https://bitbucket.org/nick_fraser/knlms_core_gen/src/e7e6561a4e4db2d59a96fe1b12bbd3e08ab1e62d/src/main/scala/Main.scala#lines-938:944) to select the DKNLMS type ('dknlms', 'dknlms-md', 'dknlms-md+dg', 'dknlms-ct') either unoptimized ('orig') or optimized hyperparameters ('cv')
2. Run `sbt "run MgSoftTest"` to run the software test, the result will be in store as specified by the [outputFile](https://bitbucket.org/nick_fraser/knlms_core_gen/src/e7e6561a4e4db2d59a96fe1b12bbd3e08ab1e62d/src/main/scala/Main.scala#lines-945) variable.
3. Edit [src/main/scala/Main.scala](https://bitbucket.org/nick_fraser/knlms_core_gen/src/e7e6561a4e4db2d59a96fe1b12bbd3e08ab1e62d/src/main/scala/Main.scala#lines-1040:1056) to select the DKNLMS type ('dknlms', 'dknlms-md', 'dknlms-md+dg')
4. Run `sbt "run MgHwTest"` to run the software test, the result will be in store as specified by the [outputFile](https://bitbucket.org/nick_fraser/knlms_core_gen/src/e7e6561a4e4db2d59a96fe1b12bbd3e08ab1e62d/src/main/scala/Main.scala#lines-1117) variable.

The resultant `.csv` files can then be plotted your preferred plotting tool.
