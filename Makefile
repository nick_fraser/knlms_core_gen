
#all: $(addsuffix .out, $(basename $(wildcard *.scala)))
#all: KNLMSTimeSeriesWrapper.out

#%.out: $(wildcard src/main/scala/*.scala)
#	sbt "run $* --backend c --targetDir ./emulator --compile --test --genHarness" | tee $@

source_files = $(wildcard src/main/scala/*.scala)

# Handle staging.
staging_dir := ~/.sbt/0.13/staging
staging_dirs := $(foreach dir, $(wildcard $(staging_dir)/*), $(wildcard $(dir)/*)) # Get the directory of each staging project.
staging_targets := $(addsuffix /update.stage, $(staging_dirs)) # Add a phoney target to staging dir.

.PHONY: test clean cleanall

%.v: $(source_files)
	sbt "run $* --backend v --targetDir ./verilog --genHarness"

KnlmsTester.v: $(source_files)
	sbt -mem 10000 "run KnlmsTester --backend v --targetDir ./verilog --genHarness"

%.stage:
	cd $(@D) && git pull

test: $(source_files)
	sbt "test"

download: $(staging_targets)

clean:
	rm -rf target project/target emulator verilog *.out

cleanall: clean
	-rm -rf $(staging_dir)/*

