name := "knlms_core_gen"

version := "0.1-SNAPSHOT"

organization := "my.local"

scalaVersion := "2.10.4"

crossScalaVersions := Seq("2.10.4", "2.11.6")

//addSbtPlugin("com.github.scct" % "sbt-scct" % "0.2")
//addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.0.4")

libraryDependencies += "edu.berkeley.cs" %% "chisel" % "latest.release"
//libraryDependencies += "edu.berkeley.cs" %% "chisel" % "2.2.27"

//javaOptions in run += "-Xmx8G"
//javaOptions in run += "-Xms8G"
